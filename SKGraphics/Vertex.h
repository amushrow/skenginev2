#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <cstdint>
#include <SKGraphics/Colour.h>
#include <SKSystem/Math/Types.h>
#include <SKSystem/Debug.h>

namespace SK
{
	namespace Graphics
	{
		//---- Types ----//
		//===============//

		enum class VertexType : uint8_t
		{
			Vertex2D,         // 2D Position Texture Coord and Colour
			Vertex3D,         // 3D Position, Normal and Texture Coord
			Vertex3D_TC,      // 3D Vertex with Tangent and Colour
			InstanceMatCol,   // Instanced transform and colour
			InstanceMat,      // Instanced transform
			COUNT
		};

		enum class InputLayout : uint8_t
		{
			Vertex2D,
			Vertex3D,
			Vertex3D_TC,
			Vertex3D_InstanceMatCol,
			Vertex3D_InstanceMat,
			COUNT
		};

		static const int NUM_VERTEX_TYPES = static_cast<int>(VertexType::COUNT);
		static const int NUM_INPUT_LAYOUTS = static_cast<int>(InputLayout::COUNT);

		struct Vertex2D
		{
			Math::float2 Position;
			Math::float2 TextureCoord;
			ColourFloat  Colour;
		};

		struct Vertex3D
		{
			Math::float3 Position;
			Math::float3 Normal;
			Math::float2 TextureCoord;
		};

		struct Vertex3D_TC
		{
			Math::float3 Position;
			Math::float3 Normal;
			Math::float2 TextureCoord;
			Math::float3 Tangent;
			ColourFloat  Colour;
			float        Reserved;
		};

		struct InstanceMat
		{
			Math::float4x4 Transform;
		};

		struct InstanceMatCol
		{
			Math::float4x4 Transform;
			ColourFloat  Colour;
		};

		//--- Methods ---//
		//===============//

		inline uint32_t VertexSize(VertexType type)
		{
			size_t Result = 0;
			switch (type)
			{
			case VertexType::Vertex2D: Result = sizeof(Vertex2D); break;
			case VertexType::Vertex3D: Result =  sizeof(Vertex3D); break;
			case VertexType::Vertex3D_TC: Result = sizeof(Vertex3D_TC); break;
			case VertexType::InstanceMatCol: Result = sizeof(InstanceMatCol); break;
			case VertexType::InstanceMat: Result = sizeof(InstanceMat); break;
			default: SK::Debug::Halt();
			}

			return static_cast<uint32_t>(Result);
		}
	}
}