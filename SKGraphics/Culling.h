#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <SKSystem/Math/Matrix.h>
#include <SKSystem/Math/TemplateMath.h>
#include <SKSystem/Math/Vector.h>

namespace SK
{
	namespace Graphics
	{
		//---- Types ----//
		//===============//

		struct AABBList
		{
			AABBList(size_t size);
			~AABBList();
			
			float* const CentreX;
			float* const CentreY;
			float* const CentreZ;
			float* const SizeX;
			float* const SizeY;
			float* const SizeZ;
			float* const Radius;
			const size_t Size;
		};

		class Extents
		{
		public:
			//--- Methods ---//
			//===============//

			Extents()
			{
				memset(Corners, 0, 128);
			}

			void* operator new(std::size_t size)   { return SK::AlignedNew(16, size); }
			void* operator new[](std::size_t size) { return SK::AlignedNew(16, size); }
			void  operator delete(void* mem)       { SK::AlignedDelete(mem); }
			void  operator delete[](void* mem)     { SK::AlignedDelete(mem); }

			//-- Variables --//
			//===============//

			Math::Vector Corners[8];
		};

		class AABB
		{
		public:
			//--- Methods ---//
			//===============//

			AABB()
			{
				memset(this, 0, 32);
			}

			void* operator new(std::size_t size)   { return SK::AlignedNew(16, size); }
			void* operator new[](std::size_t size) { return SK::AlignedNew(16, size); }
			void  operator delete(void* mem)       { SK::AlignedDelete(mem); }
			void  operator delete[](void* mem)     { SK::AlignedDelete(mem); }

			static AABB FromExtents(const Extents& extents);

			//-- Variables --//
			//===============//

			Math::Vector Centre;
			Math::Vector Size;
		};

		class Frustum
		{
		public:
			//--- Methods ---//
			//===============//

			void Contains(const AABBList& aabb, uint8_t* results, uint32_t numBoxes) const;
			void Contains(const AABBList& aabb, uint8_t* results, uint32_t start, uint32_t end) const;
			bool Contains(const AABB& aabb) const;
			void SetViewProj(const Math::Matrix& viewProj);

		private:
			//-- Variables --//
			//===============//

			Math::Vector m_Planes[6];
			AABB         m_Box;
		};

		template<int BATCH_SIZE = 8, uint8_t MAX_LEVELS = 3>
		class CulledBoxes
		{
		public:
			//--- Methods ---//
			//===============//
			CulledBoxes(uint32_t maxSize)
				: Box(maxSize)
				, Culled(SK::AlignedNew<uint8_t>(16, maxSize))
				, Size(maxSize)
				, m_HiBoxes(maxSize / BATCH_SIZE)
			{
				memset(Culled, 0, Size);
			}

			~CulledBoxes()
			{
				SK::AlignedDelete(Culled);
			}

			void Cull(const SK::Graphics::Frustum& frustum)
			{
				memset(Culled, 0, Size);
				m_HiBoxes.Cull(frustum);
				for (uint32_t i = 0; i < m_HiBoxes.Size; ++i)
				{
					if (m_HiBoxes.Culled[i])
						memset(Culled + (i * BATCH_SIZE), 1, BATCH_SIZE);
				}

				frustum.Contains(Box, Culled, Size);
			}

			void Cull(const SK::Graphics::Frustum& frustum, uint32_t start, uint32_t end)
			{
				memset(Culled + start, 0, end - start);

				uint32_t HiStart = start / BATCH_SIZE;
				uint32_t HiEnd = std::min(end / BATCH_SIZE, m_HiBoxes.Size);
				m_HiBoxes.Cull(frustum, HiStart, HiEnd);
				for (uint32_t i = HiStart; i < HiEnd; ++i)
				{
					if (m_HiBoxes.Culled[i])
						memset(Culled + (i * BATCH_SIZE), 1, BATCH_SIZE);
				}

				frustum.Contains(Box, Culled, start, end);
			}

			// Initialise AABB
			void SetBox(uint32_t index, const Math::float4& centre, const Math::float4& size)
			{
				Box.CentreX[index] = centre.x;
				Box.CentreY[index] = centre.y;
				Box.CentreZ[index] = centre.z;
				Box.SizeX[index] = size.x;
				Box.SizeY[index] = size.y;
				Box.SizeZ[index] = size.z;
				Box.Radius[index] = size.w;
			}

			// Update an AABB and propagate the changes through the higher levels
			void UpdateBox(uint32_t index, const Math::float4& centre, const Math::float4& size)
			{
				Box.CentreX[index] = centre.x;
				Box.CentreY[index] = centre.y;
				Box.CentreZ[index] = centre.z;
				Box.SizeX[index] = size.x;
				Box.SizeY[index] = size.y;
				Box.SizeZ[index] = size.z;
				Box.Radius[index] = size.w;

				{
					uint32_t startIndex = (index / BATCH_SIZE) * BATCH_SIZE;
					Math::float4 Max(-FLT_MAX);
					Math::float4 Min(FLT_MAX);
					for (uint32_t i = 0; i < BATCH_SIZE; ++i)
					{
						if (Box.SizeX[startIndex + i] != FLT_MAX)
						{
							Max.x = std::max(Max.x, Box.CentreX[startIndex + i] + Box.SizeX[startIndex + i]);
							Max.y = std::max(Max.y, Box.CentreY[startIndex + i] + Box.SizeY[startIndex + i]);
							Max.z = std::max(Max.z, Box.CentreZ[startIndex + i] + Box.SizeZ[startIndex + i]);
							Min.x = std::min(Min.x, Box.CentreX[startIndex + i] - Box.SizeX[startIndex + i]);
							Min.y = std::min(Min.y, Box.CentreY[startIndex + i] - Box.SizeY[startIndex + i]);
							Min.z = std::min(Min.z, Box.CentreZ[startIndex + i] - Box.SizeZ[startIndex + i]);
						}
					}

					uint32_t HiIndex = index / BATCH_SIZE;
					Math::float4 HiCentre = (Max + Min) / 2;
					Math::float4 HiSize = (Max - Min) / 2;

					m_HiBoxes.UpdateBox(HiIndex, HiCentre, HiSize);
				}
			}

			// Initialise all HiBoxes from the current data set
			void InitialiseHiBoxes()
			{
				for (uint32_t k = 0; k < m_HiBoxes.Size; ++k)
				{
					uint32_t StartIndex = k * BATCH_SIZE;
					Math::float4 Max(-FLT_MAX);
					Math::float4 Min(FLT_MAX);
					for (uint32_t i = 0; i < BATCH_SIZE; ++i)
					{
						if (Box.SizeX[StartIndex + i] != FLT_MAX)
						{
							Max.x = std::max(Max.x, Box.CentreX[StartIndex + i] + Box.SizeX[StartIndex + i]);
							Max.y = std::max(Max.y, Box.CentreY[StartIndex + i] + Box.SizeY[StartIndex + i]);
							Max.z = std::max(Max.z, Box.CentreZ[StartIndex + i] + Box.SizeZ[StartIndex + i]);
							Min.x = std::min(Min.x, Box.CentreX[StartIndex + i] - Box.SizeX[StartIndex + i]);
							Min.y = std::min(Min.y, Box.CentreY[StartIndex + i] - Box.SizeY[StartIndex + i]);
							Min.z = std::min(Min.z, Box.CentreZ[StartIndex + i] - Box.SizeZ[StartIndex + i]);
						}
					}

					Math::float4 HiCentre = (Max + Min) / 2;
					Math::float4 HiSize = (Max - Min) / 2;
					m_HiBoxes.SetBox(k, HiCentre, HiSize);
				}

				m_HiBoxes.InitialiseHiBoxes();
			}

			//-- Variables --//
			//===============//
			AABBList              Box;
			uint8_t* const        Culled;
			const uint32_t        Size;
			static const uint32_t HiBlockSize = Math::pow<BATCH_SIZE, MAX_LEVELS>::value;

		private:
			CulledBoxes<BATCH_SIZE, MAX_LEVELS - 1> m_HiBoxes;
		};

		template<int BATCH_SIZE>
		class CulledBoxes<BATCH_SIZE, 0>
		{
		public:
			//--- Methods ---//
			//===============//
			CulledBoxes(uint32_t maxSize)
				: Box(maxSize)
				, Culled(SK::AlignedNew<uint8_t>(16, maxSize))
				, Size(maxSize)
			{
				memset(Culled, 0, Size);
			}

			~CulledBoxes()
			{
				SK::AlignedDelete(Culled);
			}

			void Cull(const SK::Graphics::Frustum& frustum)
			{
				memset(Culled, 0, Size);
				frustum.Contains(Box, Culled, Size);
			}

			void Cull(const SK::Graphics::Frustum& frustum, uint32_t start, uint32_t end)
			{
				memset(Culled + start, 0, end - start);
				frustum.Contains(Box, Culled, start, end);
			}

			void SetBox(uint32_t index, const Math::float4& centre, const Math::float4& size)
			{
				UpdateBox(index, centre, size);
			}

			void UpdateBox(uint32_t index, const Math::float4& centre, const Math::float4& size)
			{
				Box.CentreX[index] = centre.x;
				Box.CentreY[index] = centre.y;
				Box.CentreZ[index] = centre.z;
				Box.SizeX[index] = size.x;
				Box.SizeY[index] = size.y;
				Box.SizeZ[index] = size.z;
				Box.Radius[index] = size.w;
			}

			void InitialiseHiBoxes() {}

			//-- Variables --//
			//===============//
			AABBList              Box;
			uint8_t* const        Culled;
			const uint32_t        Size;
			static const uint32_t BatchSize = BATCH_SIZE;
		};
	}
}
