#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <SKSystem/Singleton.h>
#include <SKGraphics/Texture.h>
#include <SKSystem/Debug.h>
#include <map>
#include <memory>

namespace SK
{
	namespace Graphics
	{
		//---- Types ----//
		//===============//

		template<typename T>
		class ResourceCache : public Singleton<ResourceCache<T>>
		{
		public:

			//---- Types ----//
			//===============//
			friend class Singleton<ResourceCache<T>>;
			typedef std::shared_ptr<T> ResourcePtr;

			//--- Methods ---//
			//===============//

			bool        HasResource(size_t id);
			ResourcePtr GetResource(size_t id);
			int         ResourceUseCount(size_t id);
			void        AddResource(size_t id, ResourcePtr resource);
			void        RemoveResource(size_t id);
			void        Clear();

		private:

			//--- Methods ---//
			//===============//

			ResourceCache() {}

			//-- Variables --//
			//===============//

			std::map<size_t, ResourcePtr> m_Resources;
		};
	}
}

//--- Methods ---//
//===============//

template<typename T>
bool SK::Graphics::ResourceCache<T>::HasResource(size_t id)
{
	return m_Resources.count(id) > 0;
}

template<typename T>
typename SK::Graphics::ResourceCache<T>::ResourcePtr SK::Graphics::ResourceCache<T>::GetResource(size_t id)
{
	auto It = m_Resources.find(id);
	if (It != m_Resources.end())
	{
		return It->second;
	}

	return nullptr;
}

template<typename T>
int SK::Graphics::ResourceCache<T>::ResourceUseCount(size_t id)
{
	auto It = m_Resources.find(id);
	if (It != m_Resources.end())
	{
		return static_cast<int>(It->second.use_count());
	}

	SK::Debug::Assert(false, "ResourceCache::ResourceUseCount - A resource with this id is not in the cache");
	return 0;
}

template<typename T>
void SK::Graphics::ResourceCache<T>::AddResource(size_t id, ResourcePtr resource)
{
	SK::Debug::Assert(HasResource(id) == false, "ResourceCache::AddResource - A resource with this id has already been added.");
	m_Resources[id] = resource;
}

template<typename T>
void SK::Graphics::ResourceCache<T>::RemoveResource(size_t id)
{
	m_Resources.erase(id);
}

template<typename T>
void SK::Graphics::ResourceCache<T>::Clear()
{
	m_Resources.clear();
}