#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <SKSystem/GUI/Win32/Window.h>
#include <SKGraphics/Renderer.h>
#include <SKGraphics/Win32/DX11Shaders.h>
#include <d3d11_1.h>
#include <D3D11Shader.h>
#include <map>

namespace SK
{
	namespace Graphics
	{
		//---- Types ----//
		//===============//

		typedef std::map<size_t, DX11VertexShader> VertexShaderMap;
		typedef std::map<size_t, DX11PixelShader> PixelShaderMap;
		typedef VertexShaderMap::iterator VertexShaderIterator;
		typedef PixelShaderMap::iterator PixelShaderIterator;

		struct Renderer::PlatformData
		{
			//--- Methods ---//
			//===============//

			PlatformData();

			void SetVertexBuffer(PrimType primType, VertexBufferPtr vertexBuffer);
			void SetInstancedVertexBuffer(PrimType primType, VertexBufferPtr vertexBuffer, VertexBufferPtr instanceBuffer);
			void SetIndexBuffer(IndexBufferPtr indexBuffer);
			void ResolveRenderTarget(TexturePtr texture, int face);
			void SetupShaderParams(DX11VertexShader& shader, ID3D11ShaderReflection* reflection);
			void SetupShaderParams(DX11PixelShader& shader, ID3D11ShaderReflection* reflection);

			void GetParams(ID3D11ShaderReflection* reflection, D3D11_SHADER_INPUT_BIND_DESC& bindingDesc, ParamMap& params, ParamBufferList& paramBufferList);

			void Release();

			//-- Variables --//
			//===============//
			bool                     Initialised;
			RenderSettings           CurrentSettings;

			VertexShaderMap          VertexShaders;
			PixelShaderMap           PixelShaders;

			IDXGISwapChain*          SwapChain;
			DXGI_SWAP_CHAIN_DESC     SwapChainDesc;
			ID3D11Device*            Device;
			ID3D11DeviceContext*     DeviceContext;
			ID3D11InputLayout*       InputLayout[NUM_INPUT_LAYOUTS];
			ID3D11Texture2D*         BackBuffer;
			ID3D11RenderTargetView*  BackBufferView;
			ID3D11Texture2D*         DepthBuffer;
			ID3D11DepthStencilView*  DepthBufferView;

			ID3D11DepthStencilState* DepthStencilStates[4];
			ID3D11BlendState*        BlendStates[4];
			ID3D11RasterizerState*   RasterizerStates[3];
			ID3D11SamplerState*      SamplerStates[6];

			VertexShaderIterator     ActiveVertexShader;
			PixelShaderIterator      ActivePixelShader;
			ID3D11Buffer*            ActiveVertexBuffer[2];
			ID3D11Buffer*            ActiveIndexBuffer;
			VertexType               ActiveInputLayout;

			TexturePtr               RenderTargetTexture[D3D11_SIMULTANEOUS_RENDER_TARGET_COUNT];
			TexturePtr               DepthStencilTexture;
			int                      RenderTargetFace[D3D11_SIMULTANEOUS_RENDER_TARGET_COUNT];
			int                      NumRenderTargets;
		};
	}
}