struct Vertex2D
{
	float2 Position : POSITION;
	float2 TexCoord : TEXCOORD0;
	float4 Colour   : TEXCOORD1;
};

struct Vertex3D
{
	float3 Position : POSITION;
	float3 Normal   : NORMAL;
	float2 TexCoord : TEXCOORD0;
};

struct Vertex3D_TC
{
	float3 Position : POSITION;
	float3 Normal   : NORMAL;
	float2 TexCoord : TEXCOORD0;
	float3 Tangent  : TANGENT;
	float4 Colour   : TEXCOORD1;
	float  Reserved : TEXCOORD2;
};

cbuffer Matrices
{
	float4x4 World;
	float4x4 View;
	float4x4 Projection;
	float4x4 ViewProj;
	float4x4 WorldViewProj;
};