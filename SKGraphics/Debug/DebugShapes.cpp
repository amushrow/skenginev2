//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2016 Anthony Mushrow                           //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <SKGraphics\Debug\DebugShapes.h>
using namespace SK;
using namespace SK::Graphics;

//-- Declarations --//
//==================//

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//
DebugShapes::DebugShapes(uint32_t maxLines, uint32_t maxBoxes)
: m_Initialised(false)
, m_NumBoxes(0)
, m_MaxBoxes(maxBoxes)
, m_BoxesBuffer(nullptr)
, m_BoxesInstanceBuffer(nullptr)
{
	// Boxes
	m_BoxInstances = new InstanceMatCol[maxBoxes];
}

void DebugShapes::Initialise(Renderer& renderer)
{
	static Vertex3D BoxVerts[36] =
	{
		//Bottom
		{ { -0.5f, -0.5f, -0.5f }, { 0.0f, -1.0f, 0.0f } },
		{ { 0.5f, -0.5f, 0.5f }, { 0.0f, -1.0f, 0.0f } },
		{ { 0.5f, -0.5f, -0.5f }, { 0.0f, -1.0f, 0.0f } },


		{ { 0.5f, -0.5f, 0.5f }, { 0.0f, -1.0f, 0.0f } },
		{ { -0.5f, -0.5f, -0.5f }, { 0.0f, -1.0f, 0.0f } },
		{ { -0.5f, -0.5f, 0.5f }, { 0.0f, -1.0f, 0.0f } },

		//Top
		{ { -0.5f, 0.5f, -0.5f }, { 0.0f, 1.0f, 0.0f } },
		{ { 0.5f, 0.5f, -0.5f }, { 0.0f, 1.0f, 0.0f } },
		{ { 0.5f, 0.5f, 0.5f }, { 0.0f, 1.0f, 0.0f } },

		{ { 0.5f, 0.5f, 0.5f }, { 0.0f, 1.0f, 0.0f } },
		{ { -0.5f, 0.5f, 0.5f }, { 0.0f, 1.0f, 0.0f } },
		{ { -0.5f, 0.5f, -0.5f }, { 0.0f, 1.0f, 0.0f } },

		//Left
		{ { -0.5f, -0.5f, -0.5f }, { -1.0f, 0.0f, 0.0f } },
		{ { -0.5f, 0.5f, -0.5f }, { -1.0f, 0.0f, 0.0f } },
		{ { -0.5f, 0.5f, 0.5f }, { -1.0f, 0.0f, 0.0f } },

		{ { -0.5f, 0.5f, 0.5f }, { -1.0f, 0.0f, 0.0f } },
		{ { -0.5f, -0.5f, 0.5f }, { -1.0f, 0.0f, 0.0f } },
		{ { -0.5f, -0.5f, -0.5f }, { -1.0f, 0.0f, 0.0f } },

		//Right
		{ { 0.5f, -0.5f, -0.5f }, { 1.0f, 0.0f, 0.0f } },
		{ { 0.5f, 0.5f, 0.5f }, { 1.0f, 0.0f, 0.0f } },
		{ { 0.5f, 0.5f, -0.5f }, { 1.0f, 0.0f, 0.0f } },

		{ { 0.5f, 0.5f, 0.5f }, { 1.0f, 0.0f, 0.0f } },
		{ { 0.5f, -0.5f, -0.5f }, { 1.0f, 0.0f, 0.0f } },
		{ { 0.5f, -0.5f, 0.5f }, { 1.0f, 0.0f, 0.0f } },


		// Front
		{ { 0.5f, 0.5f, 0.5f }, { 0.0f, 0.0f, 1.0f } },
		{ { 0.5f, -0.5f, 0.5f }, { 0.0f, 0.0f, 1.0f } },
		{ { -0.5f, -0.5f, 0.5f }, { 0.0f, 0.0f, 1.0f } },

		{ { -0.5f, -0.5f, 0.5f }, { 0.0f, 0.0f, 1.0f } },
		{ { -0.5f, 0.5f, 0.5f }, { 0.0f, 0.0f, 1.0f } },
		{ { 0.5f, 0.5f, 0.5f }, { 0.0f, 0.0f, 1.0f } },

		// Back
		{ { 0.5f, 0.5f, -0.5f }, { 0.0f, 0.0f, -1.0f } },
		{ { -0.5f, -0.5f, -0.5f }, { 0.0f, 0.0f, -1.0f } },
		{ { 0.5f, -0.5f, -0.5f }, { 0.0f, 0.0f, -1.0f } },


		{ { -0.5f, -0.5f, -0.5f }, { 0.0f, 0.0f, -1.0f } },
		{ { 0.5f, 0.5f, -0.5f }, { 0.0f, 0.0f, -1.0f } },
		{ { -0.5f, 0.5f, -0.5f }, { 0.0f, 0.0f, -1.0f } },
	};

	m_BoxesBuffer = renderer.CreateVertexBuffer(VertexType::Vertex3D, 36, false, BoxVerts, sizeof(BoxVerts));
	m_BoxesInstanceBuffer = renderer.CreateVertexBuffer(VertexType::InstanceMatCol, m_MaxBoxes, true);
	m_Initialised = true;
}

void DebugShapes::Clear()
{
	m_NumBoxes = 0;
}

void DebugShapes::Render(Renderer& renderer)
{
	if (m_NumBoxes)
	{
		auto InstanceBuffer = renderer.LockBuffer(m_BoxesInstanceBuffer);
		memcpy(InstanceBuffer.Data, m_BoxInstances, VertexSize(VertexType::InstanceMatCol) * m_NumBoxes);
		renderer.UnlockBuffer(m_BoxesInstanceBuffer);

		renderer.DrawBufferInstanced(PrimType::TriangleList, m_BoxesBuffer, 36, 0, m_BoxesInstanceBuffer, m_NumBoxes, 0);
	}
}

bool DebugShapes::AddLine(const Math::float3& a, const Math::float3& b, const ColourFloat& colour)
{
	return false;
}

bool DebugShapes::AddBox(const Math::Matrix& matrix, const Math::float3& extents, const ColourFloat& colour)
{
	if (m_NumBoxes >= m_MaxBoxes)
		return false;

	InstanceMatCol& Instance = m_BoxInstances[m_NumBoxes++];
	Instance.Transform = (Math::Matrix::Scale(extents.x, extents.y, extents.z) * matrix).Get();
	Instance.Colour = colour;

	return true;
}

//-- Private Methods --//
//=====================//
