#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//
//SKTD: add BGRA formats

//--- Include ---//
//===============//
#include <SKSystem/Flags.h>
#include <SKSystem/Math/Types.h>
#include <memory>
#include <cstdint>

namespace SK
{
	namespace Graphics
	{
		//---- Types ----//
		//===============//

		enum class TextureFormat
		{
			Unknown,
			RGBA_32,         // 8-bit unsigned
			RGBA_64,         // 16-bit float
			RGBA_128,        // 32-bit float
			RG_32,           // 16-bit float
			RG_64,           // 32-bit float
			Red_8,           // 8-bit unsigned
			Red_16,          // 16-bit float
			Red_32,          // 32-bit float
			Depth_32,        // 32-bit depth
			DepthStencil_32, // 24-bit depth, 8-bit stencil
			RGBA_UINT32, // 8-bit uint
			RGBA_UINT64, // 16-bit uint
			RGBA_UINT128,// 32-bit uint
			RG_UINT32,   // 16-bit uint
			RG_UINT64,   // 32-bit uint
		};

		enum class TextureFlag
		{
			Lockable = 0x01,
			RenderTarget = 0x02,
			MipMapped = 0x04,
			Cube = 0x08,
			MSAA = 0x10,
			Staging = 0x20
		};
		typedef Flags<TextureFlag> TextureFlags;

		class Texture
		{
		public:

			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			Texture(const Math::int3& size, TextureFormat format, TextureFlags flags)
			: m_Size(size), m_Format(format), m_Flags(flags)
			{

			}

			virtual ~Texture() { }

			TextureFlags      Flags() const  { return m_Flags; }
			const Math::int3& Size() const   { return m_Size; }
			TextureFormat     Format() const { return m_Format; }

			//-- Variables --//
			//===============//

		private:

			//-- Variables --//
			//===============//

			TextureFlags   m_Flags;
			Math::int3     m_Size;
			TextureFormat  m_Format;
		};

		typedef std::shared_ptr<Texture> TexturePtr;

		//--- Methods ---//
		//===============//

		//--- Methods ---//
		//===============//

		inline uint32_t TextureSampleSize(TextureFormat format)
		{
			switch (format)
			{
			case TextureFormat::RGBA_32: return 4;
			case TextureFormat::RGBA_64: return 8;
			case TextureFormat::RGBA_128: return 16;
			case TextureFormat::RG_32: return 4;
			case TextureFormat::RG_64: return 8;
			case TextureFormat::Red_8: return 1;
			case TextureFormat::Red_16: return 2;
			case TextureFormat::Red_32: return 4;
			case TextureFormat::Depth_32: return 4;
			case TextureFormat::DepthStencil_32: return 4;
			case TextureFormat::RGBA_UINT32: return 4;
			case TextureFormat::RGBA_UINT64: return 8;
			case TextureFormat::RGBA_UINT128: return 16;
			case TextureFormat::RG_UINT32: return 4;
			case TextureFormat::RG_UINT64: return 8;
			}

			return 0;
		}
	}
}