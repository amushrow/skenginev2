//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <SKSystem/Debug.h>
#include <SKSystem/IO/MemoryStream.h>
#include <SKSystem/Math/Types.h>
#include <cmath>
#include "MipmapImage.h"

using namespace SK::Graphics;

//-- Declarations --//
//==================//

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

MipmapImage::MipmapImage(const Image& image, int levels)
{
	SK::Debug::Assert(levels > 0, "At least 1 level is required.");

	unsigned int Stride = 0;
	switch (image.Format())
	{
	case InternalFormat::RGB:
	case InternalFormat::BGR:
		Stride = 3;
		break;
	case InternalFormat::RGBA:
	case InternalFormat::BGRA:
		Stride = 4;
		break;
	}

	SK::Math::int2 BaseSize = image.Size();
	if (levels > 1)
	{
		BaseSize.x = static_cast<int>(std::ceil((BaseSize.x * 2.0f) / 3.0f));
	}

	SK::Math::int2 MipSize = BaseSize;
	int RowOffset = 0;
	for (int level = 0; level < levels; ++level)
	{
		Image NewImage(MipSize.x, MipSize.y, image.Format());
		SK::IO::MemoryStream NewImageData(NewImage.Data(), MipSize.x * MipSize.y * Stride, false);
		std::size_t RowLength = MipSize.x * Stride;

		if (level == 0)
		{
			for (int Row = 0; Row < MipSize.y; ++Row)
			{
				std::size_t Offset = Row * image.Size().x * Stride;			
				NewImageData.WriteData(image.Data(), Offset, RowLength);
			}
		}
		else
		{
			for (int Row = 0; Row < MipSize.y; ++Row)
			{
				std::size_t Offset = ((Row + RowOffset) * image.Size().x * Stride) + (BaseSize.x * Stride);
				NewImageData.WriteData(image.Data(), Offset, RowLength);
			}
			RowOffset += MipSize.y;
		}

		m_Images.push_back(std::move(NewImage));
		MipSize.x = static_cast<int>(std::floor(MipSize.x / 2.0f));
		MipSize.y = static_cast<int>(std::floor(MipSize.y / 2.0f));
	}
}

MipmapImage::MipmapImage(MipmapImage&& other)
: m_Images(std::move(other.m_Images))
{
	other.m_Images.clear();
}

MipmapImage& MipmapImage::operator = (MipmapImage&& other)
{
	m_Images = std::move(other.m_Images);
	other.m_Images.clear();

	return *this;
}

//-- Private Methods --//
//=====================//