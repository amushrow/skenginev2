#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <cstdint>

namespace SK
{
	namespace Graphics
	{
		//---- Types ----//
		//===============//

#pragma pack(push, 1)
		struct BitmapFileHeader
		{
			uint16_t FileType;
			uint32_t Length;
			uint16_t Reserved0;
			uint16_t Reserved1;
			uint32_t ImageDataOffset;
		};

		struct BitmapInfoHeader
		{
			uint32_t HeaderLength;
			int32_t  Width;
			int32_t  Height;
			uint16_t Planes;
			uint16_t Bpp;
			uint32_t Compression;
			uint32_t BufferSize;
			int32_t  XPelsPerMeter;
			int32_t  YPelsPerMeter;
			uint32_t NbColoursUsed;
			uint32_t NbColoursRequired;
		};
#pragma pack(pop)

		//--- Methods ---//
		//===============//
	}
}