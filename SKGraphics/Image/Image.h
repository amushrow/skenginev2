#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <SKSystem/IO/Stream.h>
#include <SKSystem/Math/Types.h>

namespace SK
{
	namespace Graphics
	{
		//---- Types ----//
		//===============//

		enum class FileFormat
		{
			BMP24,
			PNG
		};

		enum class InternalFormat
		{
			RGB,
			BGR,
			RGBA,
			BGRA
		};

		class Image
		{
		public:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			Image(int width, int height, InternalFormat format);
			~Image();

			Image(const Image& other);
			Image& operator=(const Image& other);

			Image(Image&& other);
			Image& operator=(Image&& other);

			Math::int2     Size() const { return Math::int2(m_Width, m_Height); }
			InternalFormat Format() const { return m_Format; }
			uint8_t        BitDepth() const { return m_BitDepth; }
			uint8_t*       Data() { return m_Data; }
			const uint8_t* Data() const { return m_Data; }
			void           Save(IO::Stream& outputStream, FileFormat format) const;
			static Image   Load(IO::Stream& inputStream, uint64_t length);

			//-- Variables --//
			//===============//

		private:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			Image() : m_Width(0), m_Height(0), m_Format(InternalFormat::RGBA), m_BitDepth(8), m_Data(nullptr) {}
			static void    GetImageData(IO::Stream& input, uint64_t length, Image& image);
			void           WriteBitmap24(IO::Stream& outputStream) const;
			void           WritePng(IO::Stream& outputStream) const;

			//-- Variables --//
			//===============//

			uint32_t       m_Width;
			uint32_t       m_Height;
			InternalFormat m_Format;
			uint8_t        m_BitDepth;
			uint8_t*       m_Data;
		};


		// Wrapper around image that allows modification of data, but prevents changing
		// the dimensions or format of the data through use of copy / move.
		// A const ImmutableImage is no different than a const Image
		class ImmutableImage
		{
		public:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			ImmutableImage(Image& source) : m_Source(source) {}

			operator const Image&() const { return m_Source; }

			Math::int2     Size() const { return m_Source.Size(); }
			InternalFormat Format() const { return m_Source.Format(); }
			uint8_t*       Data() { return m_Source.Data(); }
			const uint8_t* Data() const { return m_Source.Data(); }
			void           Save(IO::Stream& outputStream, FileFormat format) const { m_Source.Save(outputStream, format); }

			//-- Variables --//
			//===============//

		private:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			//-- Variables --//
			//===============//

			Image& m_Source;
		};

		
		//--- Methods ---//
		//===============//
	}
}