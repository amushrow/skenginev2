#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <SKGraphics/Image/Image.h>

namespace SK
{
	namespace Graphics
	{
		//---- Types ----//
		//===============//

		class MipmapImage
		{
		public:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			MipmapImage(const Image& image, int levels);
			MipmapImage(MipmapImage&& other);
			MipmapImage& operator=(MipmapImage&& other);

			int            NbLevels() const   { return static_cast<int>(m_Images.size()); }
			const Image&   Level(int i) const { return m_Images[i]; }
			ImmutableImage Level(int i)       { return ImmutableImage(m_Images[i]); }

			std::vector<Image>::const_iterator begin() const { return m_Images.begin(); }
			std::vector<Image>::const_iterator end() const   { return m_Images.end(); }

			//-- Variables --//
			//===============//

		private:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			//-- Variables --//
			//===============//
			std::vector<Image> m_Images;
		};

		//--- Methods ---//
		//===============//
	}
}