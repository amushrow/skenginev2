/*

LoadPicture.txt
by Edward "CGameProgrammer" Resnick (cgp@gdnmail.net)
This file was finished at 7:11 am eastern time, Febuary 4th, 2002.

You may use it however you wish and modify it as you wish, though
credit would be appreciated... or even better, e-mailing me if you
use it. But it is just the code for loading file formats other
people developed, nothing special really, so no big deal.


The JPEG-loading code is based on the CxImageJPG implementation.
Credits pasted from ximajpg.h:

PASTE:  CxImageJPG (c) 07/Aug/2001 <ing.davide.pizzolato@libero.it>
PASTE:  Special thanks to Troels Knakkergaard for new features, enhancements and bugfixes

PASTE:  original CImageJPG  and CImageIterator implementation are:
PASTE:  Copyright:  (c) 1995, Alejandro Aguilar Sierra <asierra@servidor.unam.mx>

PASTE:  This software is based in part on the work of the Independent JPEG Group.
PASTE:  Copyright (C) 1991-1998, Thomas G. Lane.


---------------- BEGIN SOURCE CODE -------------------

*/

#include "Image.h"
#include "BitmapFileFormat.h"

///////////////////////////////////////
// HEADERS YOU WOULDN'T NORMALLY USE //
///////////////////////////////////////

#ifdef USE_JPEG
#include "jpeglib.h"    // The JPEG library
#include <setjmp.h>     // We need this to use the JPEG library.
#endif

///////////
// FLAGS //
///////////

#define PICTURE_NONE                0x0000  // No picture loaded

// File Format Flags
#define PICTURE_BITMAP              0x1000  // Bitmap (*.bmp)
#define PICTURE_JPEG                0x2000  // JPEG (*.jpg)
#define PICTURE_TARGA               0x4000  // Targa (*.tga)

// Compression Flags
#define PICTURE_UNCOMPRESSED        0x0100  // Always true with bitmaps, valid for targas
#define PICTURE_COMPRESSED          0x0200  // Always true with jpegs, valid with targas

// Bits-Per-Pixel Flags
#define PICTURE_1BIT                0x0001  // Valid with bitmaps
#define PICTURE_MONOCHROME          0x0001
#define PICTURE_4BIT                0x0002  // Valid with bitmaps
#define PICTURE_16COLORS            0x0002
#define PICTURE_8BIT                0x0004  // Valid with bitmaps, jpegs, and targas
#define PICTURE_256COLORS           0x0004
#define PICTURE_16BIT               0x0008  // Valid with bitmaps
#define PICTURE_HIGHCOLOR           0x0008
#define PICTURE_24BIT               0x0010  // Most common bit depth, valid with bitmaps, jpegs, and targas
#define PICTURE_TRUECOLOR           0x0010
#define PICTURE_32BIT               0x0020  // Valid with bitmaps

// Gray-Scale Flag
#define PICTURE_GRAYSCALE           0x0040  // Valid with jpegs and targas

/////////////////////////
// CLASSES AND STRUCTS //
/////////////////////////

#ifdef USE_JPEG
// We need these structs to use the JPEG library.
struct JPGError
{
	jpeg_error_mgr  Public;
	jmp_buf         SetJmp;
};

static void JPEG_OnError(j_common_ptr JPGInfo)
{
	JPGError*   Error;
	char        Buffer[JMSG_LENGTH_MAX];

	Error = (JPGError*) JPGInfo->err;
	Error->Public.format_message(JPGInfo, Buffer);

	longjmp(Error->SetJmp, 1);
}
#endif

struct TGAHeader
{
	int8_t  idlength;
	int8_t  colourmaptype;
	int8_t  datatypecode;
	int16_t colourmaporigin;
	int16_t colourmaplength;
	int8_t  colourmapdepth;
	int16_t x_origin;
	int16_t y_origin;
	int16_t width;
	int16_t height;
	int8_t  bitsperpixel;
	int8_t  imagedescriptor;
};

/////////////////
// THE BIG ONE //
/////////////////

/* Note that this function requires as its only parameters
a pointer to a Stream and, optionally, the size of the picture's
part of that stream. Stream is a class I wrote, and
you can modify the function to use either your file
class, or a FILE*, or a filename, or whatever.

Picture::Load() uses only four of its functions:

bool Stream::Input( LPVOID Pointer, int Size )
This reads Size bytes and puts it in Pointer.

bool Stream::Seek( int Position, int Flag )
::Seek(0,STREAM_BEGINNING) is like fseek( File, 0, SEEK_SET )
::Seek(0,STREAM_END) is like fseek( File, 0, SEEK_END )

int Stream::Position()
This works like ftell. It returns the caret position.

int Stream::Size()
This returns the file size, and is only called if
you don't specify the file size to Picture::Load().

So it's pretty easy to integrate this code into your own
project, I hope.

You would use this function like this:

File.Open( STREAM_FILE, STREAM_READ, "Test.jpg" );
Pic.Load( &File, 0 );
File.Close( );
*/

void SK::Graphics::Image::GetImageData(SK::IO::Stream& input, uint64_t length, Image& image)
{
	//  General-purpose variables
	uint8_t TryToGetType[2];
	uint8_t Palette[1024];
	int     NumColors;
	uint32_t CurrPixel;
	int     Dest;
	int     Src;
	uint64_t     Front;
	int     HStart;
	int     HEnd;
	int     HDir;

	//  Bitmap variables
	uint8_t          BMRed;
	uint8_t          BMGreen;
	uint8_t          BMBlue;
	uint8_t          BMAlpha;
	BitmapFileHeader BMFile;
	BitmapInfoHeader BMInfo;
	int              BMPadding;
	int              BMPad;
	uint8_t          BMGroup;
	uint8_t          BMPixel8;
	uint16_t         BMPixel16;
	int              BMScanWidth;

#ifdef USE_JPEG
	//  JPEG variables
	jpeg_decompress_struct  JPGInfo;
	JPGError                JPGErr;
	JSAMPARRAY              JPGBuffer;
	int                     JPGStride;
	uint8_t*                JPGRed;
	uint8_t*                JPGGreen;
	uint8_t*                JPGBlue;
	uint8_t*                JPGSource;
	FILE*                   JPGTempFile;
	int                     JPGImageSize;
	uint8_t*                JPGImage;
	uint8_t*                JPGPixel;
	uint8_t*                JPGContents;
#endif

	//  Targa variables
	uint8_t  TGAID[256];
	int      TGANumColors;
	uint8_t  TGAPacket;
	int      TGANumPixels;
	uint8_t  TGAScanline[384];
	uint8_t* TGAImage;

	//  Find out where in the file we are. Do this in case it's an archive
	//  and the picture might start in the middle somewhere:
	Front = input.Position();
	SK::IO::Endian OriginalEndianess = input.Endianness();
	input.Endianness(SK::IO::Endian::Little);

	if (input.ReadData(TryToGetType, 0, 2) != 2)
	{
		input.Endianness(OriginalEndianess);
		return;
	}
	
	//  Go back to where we were
	input.Position(Front);

	if (TryToGetType[0] == 'B' &&
		TryToGetType[1] == 'M')
	{
		//      The file is a bitmap
		input.Read(BMFile.FileType);
		input.Read(BMFile.Length);
		input.Read(BMFile.Reserved0);
		input.Read(BMFile.Reserved1);
		input.Read(BMFile.ImageDataOffset);

		input.Read(BMInfo.HeaderLength);
		input.Read(BMInfo.Width);
		input.Read(BMInfo.Height);
		input.Read(BMInfo.Planes);
		input.Read(BMInfo.Bpp);
		input.Read(BMInfo.Compression);
		input.Read(BMInfo.BufferSize);
		input.Read(BMInfo.XPelsPerMeter);
		input.Read(BMInfo.YPelsPerMeter);
		input.Read(BMInfo.NbColoursUsed);
		input.Read(BMInfo.NbColoursRequired);
		
		//      Only uncompressed bitmaps are supported by this code
		if (BMInfo.Compression != 0)
		{
			input.Position(Front);
			input.Endianness(OriginalEndianess);
			return;
		}

		//      Create the palette now, if we must
		if (BMInfo.Bpp <= 8)
		{
			NumColors = 1 << BMInfo.Bpp;

			for (int C = 0; C < NumColors * 4; C += 4)
			{
				/*              Palette information in bitmaps is stored in BGR_ format.
				That means it's blue-green-red-blank, for each entry.
				*/
				input.Read(BMBlue);
				input.Read(BMGreen);
				input.Read(BMRed);
				input.Read(BMAlpha);
				
				Palette[C] = BMRed;
				Palette[C + 1] = BMGreen;
				Palette[C + 2] = BMBlue;
				Palette[C + 3] = BMAlpha;
			}
		}

		image.m_Width = BMInfo.Width;

		if (BMInfo.Height < 0)
		{
			image.m_Height = -BMInfo.Height;
			HStart = 0;
			HEnd = image.m_Height;
			HDir = 1;
		}
		else
		{
			image.m_Height = BMInfo.Height;
			HStart = image.m_Height - 1;
			HEnd = -1;
			HDir = -1;
		}

		if (BMInfo.Bpp == 1)
		{
			image.m_Data = new uint8_t[image.m_Width * image.m_Height * 4];
			image.m_Format = InternalFormat::RGBA;

			//          Note that this file is not necessarily grayscale, since it's possible
			//          the palette is blue-and-white, or whatever. But of course most image
			//          programs only write 1-bit images if they're black-and-white.

			//          For bitmaps, each scanline is dword-aligned.
			BMScanWidth = (image.m_Width + 7) >> 3;
			if (BMScanWidth & 3)
				BMScanWidth += 4 - (BMScanWidth & 3);

			for (int H = HStart; H != HEnd; H += HDir)
			{
				CurrPixel = 0;

				for (int W = 0; W < BMScanWidth; W++)
				{
					input.Read(BMGroup);
					
					/*                  Now we read the pixels. Usually there are
					eight pixels per byte, since each pixel
					is represented by one bit, but if the width
					is not a multiple of eight, the last byte
					will have some bits set, with the others just
					being extra. Plus there's the dword-alignment
					padding. So we keep checking to see if we've
					already read "Width" number of pixels.
					*/
					if (CurrPixel < image.m_Width)
					{
						Dest = 4 * ((H * image.m_Width) + CurrPixel);
						Src = 4 * ((BMGroup & 0x80) >> 7);
						image.m_Data[Dest] = Palette[Src];
						image.m_Data[Dest + 1] = Palette[Src + 1];
						image.m_Data[Dest + 2] = Palette[Src + 2];
						image.m_Data[Dest + 3] = Palette[Src + 3];
						CurrPixel++;
					}

					if (CurrPixel < image.m_Width)
					{
						Dest = 4 * ((H * image.m_Width) + CurrPixel);
						Src = 4 * ((BMGroup & 0x40) >> 6);
						image.m_Data[Dest] = Palette[Src];
						image.m_Data[Dest + 1] = Palette[Src + 1];
						image.m_Data[Dest + 2] = Palette[Src + 2];
						image.m_Data[Dest + 3] = Palette[Src + 3];
						CurrPixel++;
					}

					if (CurrPixel < image.m_Width)
					{
						Dest = 4 * ((H * image.m_Width) + CurrPixel);
						Src = 4 * ((BMGroup & 0x20) >> 5);
						image.m_Data[Dest] = Palette[Src];
						image.m_Data[Dest + 1] = Palette[Src + 1];
						image.m_Data[Dest + 2] = Palette[Src + 2];
						image.m_Data[Dest + 3] = Palette[Src + 3];
						CurrPixel++;
					}

					if (CurrPixel < image.m_Width)
					{
						Dest = 4 * ((H * image.m_Width) + CurrPixel);
						Src = 4 * ((BMGroup & 0x10) >> 4);
						image.m_Data[Dest] = Palette[Src];
						image.m_Data[Dest + 1] = Palette[Src + 1];
						image.m_Data[Dest + 2] = Palette[Src + 2];
						image.m_Data[Dest + 3] = Palette[Src + 3];
						CurrPixel++;
					}

					if (CurrPixel < image.m_Width)
					{
						Dest = 4 * ((H * image.m_Width) + CurrPixel);
						Src = 4 * ((BMGroup & 0x08) >> 3);
						image.m_Data[Dest] = Palette[Src];
						image.m_Data[Dest + 1] = Palette[Src + 1];
						image.m_Data[Dest + 2] = Palette[Src + 2];
						image.m_Data[Dest + 3] = Palette[Src + 3];
						CurrPixel++;
					}

					if (CurrPixel < image.m_Width)
					{
						Dest = 4 * ((H * image.m_Width) + CurrPixel);
						Src = 4 * ((BMGroup & 0x04) >> 2);
						image.m_Data[Dest] = Palette[Src];
						image.m_Data[Dest + 1] = Palette[Src + 1];
						image.m_Data[Dest + 2] = Palette[Src + 2];
						image.m_Data[Dest + 3] = Palette[Src + 3];
						CurrPixel++;
					}

					if (CurrPixel < image.m_Width)
					{
						Dest = 4 * ((H * image.m_Width) + CurrPixel);
						Src = 4 * ((BMGroup & 0x02) >> 1);
						image.m_Data[Dest] = Palette[Src];
						image.m_Data[Dest + 1] = Palette[Src + 1];
						image.m_Data[Dest + 2] = Palette[Src + 2];
						image.m_Data[Dest + 3] = Palette[Src + 3];
						CurrPixel++;
					}

					if (CurrPixel < image.m_Width)
					{
						Dest = 4 * ((H * image.m_Width) + CurrPixel);
						Src = 4 * (BMGroup & 0x01);
						image.m_Data[Dest] = Palette[Src];
						image.m_Data[Dest + 1] = Palette[Src + 1];
						image.m_Data[Dest + 2] = Palette[Src + 2];
						image.m_Data[Dest + 3] = Palette[Src + 3];
						CurrPixel++;
					}
				}
			}
		}
		else if (BMInfo.Bpp == 4)
		{
			image.m_Data = new uint8_t[image.m_Width * image.m_Height * 4];
			image.m_Format = InternalFormat::RGBA;

			//          For bitmaps, each scanline is dword-aligned.
			BMScanWidth = (image.m_Width + 1) >> 1;
			if (BMScanWidth & 3)
				BMScanWidth += 4 - (BMScanWidth & 3);

			for (int H = HStart; H != HEnd; H += HDir)
			{
				CurrPixel = 0;

				for (int W = 0; W < BMScanWidth; W++)
				{
					input.Read(BMGroup);

					/*                  Now we read the pixels. Usually there are
					two pixels per byte, since each pixel
					is represented by four bits, but if the width
					is not a multiple of two, the last byte
					will have only four bits set, with the others just
					being extra. Plus there's the dword-alignment
					padding. So we keep checking to see if we've
					already read "Width" number of pixels.
					*/
					if (CurrPixel < image.m_Width)
					{
						Dest = 4 * ((H * image.m_Width) + CurrPixel);
						Src = 4 * ((BMGroup & 0xF0) >> 4);
						image.m_Data[Dest] = Palette[Src];
						image.m_Data[Dest + 1] = Palette[Src + 1];
						image.m_Data[Dest + 2] = Palette[Src + 2];
						image.m_Data[Dest + 3] = Palette[Src + 3];
						CurrPixel++;
					}

					if (CurrPixel < image.m_Width)
					{
						Dest = 4 * ((H * image.m_Width) + CurrPixel);
						Src = 4 * (BMGroup & 0x0F);
						image.m_Data[Dest] = Palette[Src];
						image.m_Data[Dest + 1] = Palette[Src + 1];
						image.m_Data[Dest + 2] = Palette[Src + 2];
						image.m_Data[Dest + 3] = Palette[Src + 3];
						CurrPixel++;
					}
				}
			}
		}
		else if (BMInfo.Bpp == 8)
		{
			image.m_Data = new uint8_t[image.m_Width * image.m_Height * 4];
			image.m_Format = InternalFormat::RGBA;

			//          For bitmaps, each scanline is dword-aligned.
			BMScanWidth = image.m_Width;
			if (BMScanWidth & 3)
				BMScanWidth += 4 - (BMScanWidth & 3);

			for (int H = HStart; H != HEnd; H += HDir)
			{
				CurrPixel = 0;

				for (int W = 0; W < BMScanWidth; W++)
				{
					input.Read(BMPixel8);
					
					if (CurrPixel < image.m_Width)
					{
						Dest = 4 * ((H * image.m_Width) + CurrPixel);
						Src = 4 * BMPixel8;
						image.m_Data[Dest] = Palette[Src];
						image.m_Data[Dest + 1] = Palette[Src + 1];
						image.m_Data[Dest + 2] = Palette[Src + 2];
						image.m_Data[Dest + 3] = Palette[Src + 3];
						CurrPixel++;
					}
				}
			}
		}
		else if (BMInfo.Bpp == 16)
		{
			image.m_Data = new uint8_t[image.m_Width * image.m_Height * 3];
			image.m_Format = InternalFormat::RGB;

			//          For bitmaps, each scanline is dword-aligned.
			BMScanWidth = image.m_Width << 1;

			if (BMScanWidth & 3)
				BMPadding = 4 - (BMScanWidth & 3);
			else
				BMPadding = 0;

			for (int H = HStart; H != HEnd; H += HDir)
			{
				for (uint32_t W = 0; W < image.m_Width; W++)
				{
					input.Read(BMPixel16);

					/*                  Now we seperate each WORD into the correct RGB values.
					The highest bit is zero, the next 5 are red, the next
					5 are green, and the lowest 5 are blue.
					*/
					BMRed = ((BMPixel16 >> 10) & 0x1F) << 3;
					BMGreen = ((BMPixel16 >> 5) & 0x1F) << 3;
					BMBlue = (BMPixel16 & 0x1F) << 3;

					Dest = ((H * image.m_Width) + W) << 1;
					image.m_Data[Dest] = BMRed;
					image.m_Data[Dest + 1] = BMGreen;
					image.m_Data[Dest + 2] = BMBlue;
				}

				if (BMPadding)
				{
					input.ReadData(reinterpret_cast<uint8_t*>(&BMPad), 0, BMPadding);
				}
			}
		}
		else if (BMInfo.Bpp == 24)
		{
			image.m_Data = new uint8_t[image.m_Width * image.m_Height * 3];
			image.m_Format = InternalFormat::RGB;

			//          For bitmaps, each scanline is dword-aligned.
			BMScanWidth = image.m_Width * 3;

			if (BMScanWidth & 3)
				BMPadding = 4 - (BMScanWidth & 3);
			else
				BMPadding = 0;

			for (int H = HStart; H != HEnd; H += HDir)
			{
				for (uint32_t W = 0; W < image.m_Width; W++)
				{
					input.Read(BMBlue);
					input.Read(BMGreen);
					input.Read(BMRed);

					Dest = 3 * ((H * image.m_Width) + W);
					image.m_Data[Dest] = BMRed;
					image.m_Data[Dest + 1] = BMGreen;
					image.m_Data[Dest + 2] = BMBlue;
				}

				if (BMPadding)
				{
					input.ReadData(reinterpret_cast<uint8_t*>(&BMPad), 0, BMPadding);
				}
			}
		}
		else if (BMInfo.Bpp == 32)
		{
			image.m_Data = new uint8_t[image.m_Width * image.m_Height * 4];
			image.m_Format = InternalFormat::RGBA;

			//          Since each pixel is a dword, it is obviously dword-aligned
			for (int H = HStart; H != HEnd; H += HDir)
			{
				for (uint32_t W = 0; W < image.m_Width; W++)
				{
					input.Read(BMAlpha);
					input.Read(BMBlue);
					input.Read(BMGreen);
					input.Read(BMRed);
					
					Dest = 4 * ((H * image.m_Width) + W);
					image.m_Data[Dest] = BMRed;
					image.m_Data[Dest + 1] = BMGreen;
					image.m_Data[Dest + 2] = BMBlue;
					image.m_Data[Dest + 3] = BMAlpha;
				}
			}
		}
		else // We support all possible bit depths, so if the
		{//     code gets here, it's not even a real bitmap.
			delete[] image.m_Data;
			image.m_Data = nullptr;
			input.Position(Front);
			input.Endianness(OriginalEndianess);
			return;
		}
	}
#ifdef USE_JPEG
	else if (TryToGetType[0] == 0xFF && // These unlikely-looking characters are
		TryToGetType[1] == 0xD8)  // used to identify JPEGs
	{
		/*      Unfortunately, the JPEG library seems to require a FILE*
		passed as a parameter, as far as I can tell. I may be wrong,
		but for now I save the JPEG to a temporary file and load
		from that. This is necessary if it's loaded from a resource,
		for example.
		*/
		JPGContents = new BYTE[FileSize];

		if (!File->Input(JPGContents, FileSize))
		{
			delete[] JPGContents;
			File->Seek(Front + FileSize, STREAM_BEGINNING);
			input.Endianness(OriginalEndianess);
			return false;
		}

		File->Seek(Front + FileSize, STREAM_BEGINNING);

		//      JPGContents now holds the contents of the JPEG.

		JPGInfo.err = jpeg_std_error(&JPGErr.Public);
		JPGErr.Public.error_exit = JPEG_OnError;

		if (setjmp(JPGErr.SetJmp))
		{
			//          This part of the code is only called when a JPEG
			//          library function fails. When it does, it will jump
			//          to this point.

			jpeg_destroy_decompress(&JPGInfo);
			File->Seek(Front + FileSize, STREAM_BEGINNING);
			fclose(JPGTempFile);
			input.Endianness(OriginalEndianess);
			return false;
		}

		JPGTempFile = fopen("__PicLoad.jpg", "wb"); // Create the temporary File.
		fwrite(JPGContents, 1, FileSize, JPGTempFile);
		fclose(JPGTempFile);

		delete[] JPGContents; // No longer needed
		JPGTempFile = fopen("__PicLoad.jpg", "rb");

		jpeg_create_decompress(&JPGInfo);
		jpeg_stdio_src(&JPGInfo, JPGTempFile);
		jpeg_read_header(&JPGInfo, TRUE);
		jpeg_start_decompress(&JPGInfo);

		Width = JPGInfo.image_width;
		Height = JPGInfo.image_height;

		if (JPGInfo.jpeg_color_space == JCS_GRAYSCALE)
		{
			Flags = PICTURE_JPEG | PICTURE_COMPRESSED | PICTURE_8BIT | PICTURE_GRAYSCALE;

			for (int N = 0; N < 256; N++)
			{
				Palette[N * 3] = N;
				Palette[(N * 3) + 1] = N;
				Palette[(N * 3) + 2] = N;
			}
		}
		else
		{
			if (JPGInfo.num_components == 1)
				Flags = PICTURE_JPEG | PICTURE_COMPRESSED | PICTURE_8BIT;
			else
				Flags = PICTURE_JPEG | PICTURE_COMPRESSED | PICTURE_24BIT;

			if (JPGInfo.quantize_colors && JPGInfo.num_components == 1)
			{
				JPGRed = JPGInfo.colormap[0];
				JPGGreen = JPGInfo.colormap[1];
				JPGBlue = JPGInfo.colormap[2];

				if (!JPGRed)
				{
					fclose(JPGTempFile);
					jpeg_destroy_decompress(&JPGInfo);
					input.Endianness(OriginalEndianess);
					return false;
				}

				if (!JPGGreen)
					JPGGreen = JPGRed;
				if (!JPGBlue)
					JPGBlue = JPGGreen;

				for (int N = 0; N < 256; N++)
				{
					Palette[N * 3] = JPGRed[N];
					Palette[(N * 3) + 1] = JPGGreen[N];
					Palette[(N * 3) + 2] = JPGBlue[N];
				}
			}
		}

		JPGStride = JPGInfo.output_width * JPGInfo.num_components;
		JPGBuffer = (*JPGInfo.mem->alloc_sarray)((j_common_ptr) &JPGInfo, JPOOL_IMAGE, JPGStride, 1);

		JPGImageSize = JPGInfo.num_components * Width * Height;
		JPGImage = new BYTE[JPGImageSize];
		JPGPixel = JPGImage;

		while (JPGInfo.output_scanline < JPGInfo.output_height)
		{
			jpeg_read_scanlines(&JPGInfo, JPGBuffer, 1);

			if (JPGInfo.num_components == 4 &&
				!JPGInfo.quantize_colors)
			{
				JPGSource = JPGBuffer[0];

				for (int X1 = 0, X2 = 0; X1 < JPGInfo.num_components * Width && X2 < JPGStride; X1 += 3, X2 += 4)
				{
					JPGPixel[X1] = (JPGSource[X2 + 3] * JPGSource[X2 + 2]) / 255;
					JPGPixel[X1 + 1] = (JPGSource[X2 + 3] * JPGSource[X2 + 1]) / 255;
					JPGPixel[X1 + 2] = (JPGSource[X2 + 3] * JPGSource[X2]) / 255;
				}
			}
			else
			{
				if (JPGStride < JPGInfo.num_components * Width)
					memcpy(JPGPixel, JPGBuffer[0], JPGStride);
				else
					memcpy(JPGPixel, JPGBuffer[0], JPGInfo.num_components * Width);
			}

			JPGPixel += JPGInfo.num_components * Width;
		}

		jpeg_finish_decompress(&JPGInfo);
		jpeg_destroy_decompress(&JPGInfo);

		if (JPGInfo.num_components == 1)
		{
			RGB = new BYTE[Width * Height * 3];

			for (int N = 0; N < JPGImageSize; N++)
			{
				RGB[N * 3] = Palette[JPGImage[N] * 3];
				RGB[(N * 3) + 1] = Palette[(JPGImage[N] * 3) + 1];
				RGB[(N * 3) + 2] = Palette[(JPGImage[N] * 3) + 2];
			}

			delete[] JPGImage;
		}
		else
			RGB = JPGImage;

		fclose(JPGTempFile);
		remove("__PicLoad.jpg");
	}
#endif
	else
	{
		//      The image may be a Targa
		TGAHeader Header;
		input.Read(Header.idlength);
		input.Read(Header.colourmaptype);
		input.Read(Header.datatypecode);
		input.Read(Header.colourmaporigin);
		input.Read(Header.colourmaplength);
		input.Read(Header.colourmapdepth);
		input.Read(Header.x_origin);
		input.Read(Header.y_origin);
		input.Read(Header.width);
		input.Read(Header.height);
		input.Read(Header.bitsperpixel);
		input.Read(Header.imagedescriptor);

		// Check to see if this is right
		if (Header.colourmaptype == 1)
		{
			if (Header.colourmapdepth != 24)
			{
				input.Endianness(OriginalEndianess);
				return;
			}
		}
		else if (Header.colourmaptype == 0)
		{
			if (Header.bitsperpixel != 24)
			{
				input.Endianness(OriginalEndianess);
				return;
			}
		}
		else
		{
			input.Endianness(OriginalEndianess);
			return;
		}

		if (Header.imagedescriptor & 0xF0)
		{
			input.Endianness(OriginalEndianess);
			return;
		}
		
		/*      The ID field is an optional string that you can use to give
		the image a name or description or whatever.
		*/
		if (Header.idlength)
		{
			input.ReadData(TGAID, 0, Header.idlength);
		}

		if (Header.datatypecode == 1)
		{
			TGANumColors = Header.colourmaplength;

			/*          Unlike bitmaps, Targas support palettes that are in other
			bit depths besides 24-bit. But I haven't seen any and I am
			lazy so this code ignores them.
			*/
			if (Header.colourmapdepth != 24)
			{
				input.Endianness(OriginalEndianess);
				return;
			}

			input.ReadData(Palette, 0, 3 * TGANumColors);
			
			TGAImage = new uint8_t[Header.width * Header.height];
			input.ReadData(TGAImage, 0, Header.width * Header.height);
			
			image.m_Data = new uint8_t[Header.width * Header.height * 3];
			image.m_Width = Header.width;
			image.m_Height = Header.height;
			image.m_Format = InternalFormat::RGB;

			for (int Row = 0; Row < Header.height; ++Row)
			{
				for (int Col = 0; Col < Header.width; ++Col)
				{
					int TGAIndex = ((Header.width * Row) + Col);
					int Index = ((Header.width * (Header.height - Row - 1)) + Col) * 3;
					Src = 3 * TGAImage[TGAIndex];

					image.m_Data[Index] = Palette[Src + 2];
					image.m_Data[Index + 1] = Palette[Src + 1];
					image.m_Data[Index + 2] = Palette[Src];
				}
			}

			delete[] TGAImage;
		}
		else if (Header.datatypecode == 2)
		{
			TGAImage = new uint8_t[Header.width * Header.height * 3];
			input.ReadData(TGAImage, 0, Header.width * Header.height * 3);
			image.m_Data = new uint8_t[Header.width * Header.height * 3];
			image.m_Width = Header.width;
			image.m_Height = Header.height;
			image.m_Format = InternalFormat::RGB;

			for (int Row = 0; Row < Header.height; ++Row)
			{
				for (int Col = 0; Col < Header.width; ++Col)
				{
					int TGAIndex = ((Header.width * Row) + Col) * 3;
					int Index = ((Header.width * (Header.height - Row-1)) + Col) * 3;

					image.m_Data[Index] = TGAImage[TGAIndex + 2];
					image.m_Data[Index + 1] = TGAImage[TGAIndex + 1];
					image.m_Data[Index + 2] = TGAImage[TGAIndex];
				}
			}

			delete[] TGAImage;
		}
		else if (Header.datatypecode == 3)
		{
			TGAImage = new uint8_t[Header.width * Header.height];
			input.ReadData(TGAImage, 0, Header.width * Header.height);
			
			image.m_Data = new uint8_t[Header.width * Header.height * 3];
			image.m_Width = Header.width;
			image.m_Height = Header.height;
			image.m_Format = InternalFormat::RGB;

			for (int Row = 0; Row < Header.height; ++Row)
			{
				for (int Col = 0; Col < Header.width; ++Col)
				{
					int TGAIndex = ((Header.width * Row) + Col);
					int Index = ((Header.width * (Header.height - Row - 1)) + Col) * 3;

					image.m_Data[Index] = TGAImage[TGAIndex];
					image.m_Data[Index + 1] = TGAImage[TGAIndex];
					image.m_Data[Index + 2] = TGAImage[TGAIndex];
				}
			}

			delete[] TGAImage;
		}

		/*      The following TGA formats are RLE encoded. This means the image
		data comes in two types of packets: RLE packets and raw packets.

		RLE Packets: If there is a row of 12 red pixels in a 24-bit
		image, the RLE packet consists of one byte whose
		value is 12, and three more bytes whose values are
		255,0,0. Red=255, Green=0, Blue=0.

		Raw Packets: For parts of the image that don't have nice long rows
		of the same color, rather than use RLE packets for
		each individual pixel, it's fastest to do it with raw
		packets. If, after the last packet read, there is a
		red pixel, a green pixel, and a yellow pixel, and then
		a row of blue pixels, the first byte of the raw packet
		will be 3, and it will be followed by the pixel data
		for the red, green, and yellow pixels. Then a RLE
		packet will be written for the blue pixels.

		That's basically the theory behind RLE encoding. The way TGAs use it,
		the highest bit of the first byte of the packet is used to tell whether
		it's a RLE or raw packet. Then the other 7 bits are used for the length
		of the packet pixels. Also packets cannot span several scanlines. So if
		you have a 8x8 image that's pure white, then theoretically you'd be able
		to use one RLE packet for the entire thing. But TGAs don't allow that.
		You'd need 8 RLE packets, one for each scanline.
		*/
		else if (Header.datatypecode == 9)
		{
			TGANumColors = Header.colourmaplength;

			/*          Unlike bitmaps, Targas support palettes that are in other
			bit depths besides 24-bit. But I haven't seen any and I am
			lazy so this code ignores them.
			*/
			if (Header.colourmapdepth != 24)
			{
				return;
			}

			input.ReadData(Palette, 0, 3 * TGANumColors);
			TGAImage = new uint8_t[Header.width * Header.height];

			for (int H = 0; H < Header.height; H++)
			{
				CurrPixel = 0;

				while (CurrPixel < static_cast<uint32_t>(Header.width))
				{
					input.Read(TGAPacket);
					TGANumPixels = 1 + (TGAPacket & 0x7F);

					if (TGAPacket & 0x80) // Run-length packet
					{
						input.Read(TGAScanline[0]);
						for (int N = 0; N < TGANumPixels; N++)
						{
							TGAImage[(H*Header.width) + CurrPixel] = TGAScanline[0];
							CurrPixel++;
						}
					}
					else // Raw packet
					{
						input.ReadData(TGAScanline, 0, TGANumPixels);
						for (int N = 0; N < TGANumPixels; N++)
						{
							TGAImage[(H*Header.width) + CurrPixel] = TGAScanline[N];
							CurrPixel++;
						}
					}
				}
			}

			image.m_Data = new uint8_t[Header.width * Header.height * 3];
			image.m_Width = Header.width;
			image.m_Height = Header.height;
			image.m_Format = InternalFormat::RGB;

			for (int Row = 0; Row < Header.height; ++Row)
			{
				for (int Col = 0; Col < Header.width; ++Col)
				{
					int TGAIndex = ((Header.width * Row) + Col);
					int Index = ((Header.width * (Header.height - Row - 1)) + Col) * 3;
					Src = 3 * TGAImage[TGAIndex];

					image.m_Data[Index] = Palette[Src + 2];
					image.m_Data[Index + 1] = Palette[Src + 1];
					image.m_Data[Index + 2] = Palette[Src];
				}
			}

			delete[] TGAImage;
		}
		else if (Header.datatypecode == 10)
		{
			TGAImage = new uint8_t[Header.width * Header.height * 3];

			for (int H = 0; H < Header.height; H++)
			{
				CurrPixel = 0;

				while (CurrPixel < static_cast<uint32_t>(Header.width * 3))
				{
					input.Read(TGAPacket);
					TGANumPixels = 1 + (TGAPacket & 0x7F);

					if (TGAPacket & 0x80) // Run-length packet
					{
						input.ReadData(TGAScanline, 0, 3);
						for (int N = 0; N < TGANumPixels; N++)
						{
							TGAImage[(H*Header.width * 3) + CurrPixel] = TGAScanline[0];
							TGAImage[(H*Header.width * 3) + CurrPixel + 1] = TGAScanline[1];
							TGAImage[(H*Header.width * 3) + CurrPixel + 2] = TGAScanline[2];

							CurrPixel += 3;
						}
					}
					else // Raw packet
					{
						input.ReadData(TGAScanline, 0, TGANumPixels * 3);
						
						for (int N = 0; N < TGANumPixels * 3; N += 3)
						{
							TGAImage[(H*Header.width * 3) + CurrPixel] = TGAScanline[N];
							TGAImage[(H*Header.width * 3) + CurrPixel + 1] = TGAScanline[N + 1];
							TGAImage[(H*Header.width * 3) + CurrPixel + 2] = TGAScanline[N + 2];

							CurrPixel += 3;
						}
					}
				}
			}

			image.m_Data = new uint8_t[Header.width * Header.height * 3];
			image.m_Width = Header.width;
			image.m_Height = Header.height;
			image.m_Format = InternalFormat::RGB;

			for (int Row = 0; Row < Header.height; ++Row)
			{
				for (int Col = 0; Col < Header.width; ++Col)
				{
					int TGAIndex = ((Header.width * Row) + Col) * 3;
					int Index = ((Header.width * (Header.height - Row - 1)) + Col) * 3;

					image.m_Data[Index] = TGAImage[TGAIndex + 2];
					image.m_Data[Index + 1] = TGAImage[TGAIndex + 1];
					image.m_Data[Index + 2] = TGAImage[TGAIndex];
				}
			}

			delete[] TGAImage;
		}
		else if (Header.datatypecode == 11)
		{
			TGAImage = new uint8_t[Header.width * Header.height];

			for (int H = 0; H < Header.height; H++)
			{
				CurrPixel = 0;

				while (CurrPixel < static_cast<uint32_t>(Header.width))
				{
					input.Read(TGAPacket);
					TGANumPixels = 1 + (TGAPacket & 0x7F);

					if (TGAPacket & 0x80) // Run-length packet
					{
						input.Read(TGAScanline[0]);
						for (int N = 0; N < TGANumPixels; N++)
						{
							TGAImage[(H*Header.width) + CurrPixel] = TGAScanline[0];
							CurrPixel++;
						}
					}
					else // Raw packet
					{
						input.ReadData(TGAScanline, 0, TGANumPixels);
						
						for (int N = 0; N < TGANumPixels; N++)
						{
							TGAImage[(H*Header.width) + CurrPixel] = TGAScanline[N];
							CurrPixel++;
						}
					}
				}
			}

			image.m_Data = new uint8_t[Header.width * Header.height * 3];
			image.m_Width = Header.width;
			image.m_Height = Header.height;
			image.m_Format = InternalFormat::RGB;
			
			for (int Row = 0; Row < Header.height; ++Row)
			{
				for (int Col = 0; Col < Header.width; ++Col)
				{
					int TGAIndex = ((Header.width * Row) + Col);
					int Index = ((Header.width * (Header.height - Row - 1)) + Col) * 3;

					image.m_Data[Index] = TGAImage[TGAIndex];
					image.m_Data[Index + 1] = TGAImage[TGAIndex];
					image.m_Data[Index + 2] = TGAImage[TGAIndex];
				}
			}

			delete[] TGAImage;
		}
		else
		{
			//          Unsupported image type, but we supported all six,
			//          so this should never happen.
			input.Position(Front);
			input.Endianness(OriginalEndianess);
			return;
		}
	}

	// Success! Yay!
	input.Endianness(OriginalEndianess);
}