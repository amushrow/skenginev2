//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <SKGraphics/Camera.h>
using namespace SK::Graphics;
using namespace SK::Math;
//-- Declarations --//
//==================//

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

Camera::Camera()
: m_Orientation(0,0,0,1)
, m_Position(0,0,0,1)
, m_FoV(1.0f)
, m_Near(0.1f)
, m_Far(100.0f)
, m_Aspect(1.0f)
, m_RecalcProjection(true)
, m_RecalcView(true)
, m_RecalcViewProj(true)
, m_RecalcFrustum(true)
{

}

void Camera::Position(const Vector& position)
{
	m_Position = position;
	m_RecalcView = true;
	m_RecalcViewProj = true;
	m_RecalcFrustum = true;
}

void Camera::Orientation(const Quaternion& orientation)
{
	m_Orientation = orientation;
	m_RecalcView = true;
	m_RecalcViewProj = true;
	m_RecalcFrustum = true;
}

void Camera::FoV(float fov)
{
	m_FoV = fov;
	m_RecalcProjection = true;
	m_RecalcViewProj = true;
	m_RecalcFrustum = true;
}

void Camera::Aspect(float aspect)
{
	m_Aspect = aspect;
	m_RecalcProjection = true;
	m_RecalcViewProj = true;
	m_RecalcFrustum = true;
}

void Camera::ClippingPlanes(float near, float far)
{
	m_Near = near;
	m_Far = far;
	m_RecalcProjection = true;
	m_RecalcViewProj = true;
	m_RecalcFrustum = true;
}

void Camera::LookAt(const Vector& target, const Vector& up)
{
	Vector Forward = Vector::Normalise(target - m_Position);
	m_Orientation = Quaternion::Look(Forward, up);
	
	m_RecalcView = true;
	m_RecalcViewProj = true;
	m_RecalcFrustum = true;
}

Matrix Camera::ProjectionMatrix()
{
	if (m_RecalcProjection)
	{
		float SinFoV = sin(m_FoV / 2.0f);
		float CosFoV = cos(m_FoV / 2.0f);
		float Q = m_Far / (m_Far - m_Near);

		m_ProjectionMatrix.Row0 = _mm_setr_ps((1.0f / m_Aspect) * (CosFoV / SinFoV), 0.0f, 0.0f, 0.0f);
		m_ProjectionMatrix.Row1 = _mm_setr_ps(0.0f, CosFoV / SinFoV, 0.0f, 0.0f);
		m_ProjectionMatrix.Row2 = _mm_setr_ps(0.0f, 0.0f, Q, 1.0f);
		m_ProjectionMatrix.Row3 = _mm_setr_ps(0.0f, 0.0f, -Q * m_Near, 0.0f);

		m_RecalcProjection = false;
	}

	return m_ProjectionMatrix;
}

Matrix Camera::BackwardsProjectionMatrix()
{
	float SinFoV = sin(m_FoV / 2.0f);
	float CosFoV = cos(m_FoV / 2.0f);
	float Q = m_Near / (m_Near - m_Far);

	Matrix Result;
	Result.Row0 = _mm_setr_ps((1.0f / m_Aspect) * (CosFoV / SinFoV), 0.0f, 0.0f, 0.0f);
	Result.Row1 = _mm_setr_ps(0.0f, CosFoV / SinFoV, 0.0f, 0.0f);
	Result.Row2 = _mm_setr_ps(0.0f, 0.0f, Q, 1.0f);
	Result.Row3 = _mm_setr_ps(0.0f, 0.0f, -Q * m_Far, 0.0f);

	return Result;
}

Matrix Camera::ViewMatrix()
{
	if (m_RecalcView)
	{
		float4 Position = (-m_Position).Get();
		Position.w = 1.0f;
		m_ViewMatrix = Matrix::Translation(Position.x, Position.y, Position.z) * Matrix::FromQuaternion(m_Orientation);

		m_RecalcView = false;
	}

	return m_ViewMatrix;
}

Matrix Camera::ViewProjMatrix()
{
	if (m_RecalcViewProj)
	{
		m_ViewProjMatrix = ViewMatrix() * ProjectionMatrix();
	}
	return m_ViewProjMatrix;
}

const Frustum& Camera::Frustum()
{
	if (m_RecalcFrustum)
	{
		m_Frustum.SetViewProj(ViewProjMatrix());
		m_RecalcFrustum = false;
	}

	return m_Frustum;
}

//-- Private Methods --//
//=====================//
