#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <SKGraphics/Renderer.h>
#include <SKGraphics/Renderable.h>
#include <SKSystem/Text/String.h>
#include <SKSystem/Math/Matrix.h>
#include <SKSystem/Array.h>
#include <SKSystem/Memory.h>
#include <vector>
#include <memory>
#include <map>
#include <functional>

namespace SK
{
	namespace Graphics
	{
		//---- Types ----//
		//===============//
		
		class Mesh;
		class MeshInstance;
		class MeshLoader;
		typedef std::shared_ptr<Mesh> MeshPtr;

		class MeshInstanceManager : public Renderable
		{
		public:
			struct Bin
			{
				uint32_t InstanceTypeId;
				SK::Graphics::InstanceMat* Instances;
				uint8_t renderTest;
				uint32_t NumInstances;
				uint32_t MaxInstances;
				VertexBufferPtr InstanceBuffer;
				
				Mesh*      Mesh;
				uint32_t   Offset;
				uint32_t   Count;
				uint32_t   MaterialIndex;
				uint32_t   TextureCount;
				TexturePtr Textures[MAX_TEXTURE_UNITS];

				void AddInstance(Renderable* r)
				{
					if (r && NumInstances < MaxInstances)
						Instances[NumInstances++].Transform = r->WorldTransform().Get();
				}
			};

			Bin* FindInstanceBin(uint32_t instanceTypeId);
			Bin* CreateInstanceBin(uint32_t instanceTypeId, Renderer& renderer);

			void               Render(Renderer& renderer) override;
			SK::String         Name() const override       { return "InstanceBin"; }

		private:
			std::map<uint32_t, Bin> m_InstanceBins;
		};

		class Mesh
		{
		public:

			//---- Types ----//
			//===============//

			friend class MeshLoader;

			struct Material
			{
				Material() : Offset(0)
				, Count(0)
				, Cullable(false)
				, SortOrder(RenderSortOrder::Normal)
				, VertexShaderId(INVALID_ID)
				, PixelShaderId(INVALID_ID)
				, ExtraDataId(INVALID_ID) {}

				// Index buffer location
				uint32_t Offset;
				uint32_t Count;

				bool                 Cullable;
				RenderSortOrder      SortOrder;
				size_t               VertexShaderId;
				size_t               PixelShaderId;
				SK::Array<uint8_t>   ExtraData;
				size_t               ExtraDataId;
				SK::String           TextureNames[MAX_TEXTURE_UNITS];
			};

			//--- Methods ---//
			//===============//

			Mesh();

			void* operator new(std::size_t size)   { return SK::AlignedNew(16, size); }
			void* operator new[](std::size_t size) { return SK::AlignedNew(16, size); }
			void  operator delete(void* mem)       { SK::AlignedDelete(mem); }
			void  operator delete[](void* mem)     { SK::AlignedDelete(mem); }

			const SK::String&            Name() const         { return m_Name; }
			VertexBufferPtr              VertexBuffer() const { return m_VertexBuffer; }
			IndexBufferPtr               IndexBuffer() const  { return m_IndexBuffer; }
			PrimType                     PrimType() const     { return m_PrimType; }
			const Graphics::Extents&     Extents() const      { return m_Extents; }
			const std::vector<Material>& Materials() const    { return m_Materials; }
			const std::vector<MeshPtr>&  Children() const     { return m_Children; }

			MeshInstance*                CreateInstance(MeshInstanceManager& manager, Renderer& renderer);
			MeshInstance*                CreateInstance();

			OcclusionMesh                GetOcclusionMesh() const { return m_OcclusionMesh; }

			//-- Variables --//
			//===============//

		private:

			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			//-- Variables --//
			//===============//

			SK::Math::Matrix      m_Transform;
			SK::String            m_Name;
			Graphics::Extents     m_Extents;

			// Material data
			std::vector<Material> m_Materials;

			// Vertex data
			VertexBufferPtr       m_VertexBuffer;
			IndexBufferPtr        m_IndexBuffer;
			Graphics::PrimType    m_PrimType;
			OcclusionMesh         m_OcclusionMesh;

			// Hierarchy
			Mesh*                 m_Parent;
			std::vector<MeshPtr>  m_Children;
		};

		class MeshInstance : public Renderable
		{
		public:

			//---- Types ----//
			//===============//

			friend class Mesh;
			MeshInstance() {}

			//--- Methods ---//
			//===============//

			void* operator new(std::size_t size)   { return SK::AlignedNew(16, size); }
			void* operator new[](std::size_t size) { return SK::AlignedNew(16, size); }
			void  operator delete(void* mem)       { SK::AlignedDelete(mem); }
			void  operator delete[](void* mem)     { SK::AlignedDelete(mem); }

			void          Render(Renderer& renderer) override;
			SK::String    Name() const override { return m_Mesh->Name(); }
			OcclusionMesh GetOcclusionMesh() const { return m_Mesh->GetOcclusionMesh(); };

			//-- Variables --//
			//===============//

		private:

			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			//-- Variables --//
			//===============//

			Mesh* m_Mesh;
			MeshInstanceManager::Bin* m_Bin;
			uint32_t   m_Offset;
			uint32_t   m_Count;
			uint32_t   m_MaterialIndex;
			uint32_t   m_TextureCount;
			TexturePtr m_Textures[MAX_TEXTURE_UNITS];
		};

		

		//--- Methods ---//
		//===============//
	}
}