//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <SKGraphics/RenderTree.h>
#include <algorithm>
using namespace SK::Graphics;

//-- Declarations --//
//==================//

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

RenderTree::RenderTree(uint32_t maxRenderables)
: ChildAdded(m_ChildAdded)
, ChildRemoved(m_ChildRemoved)
, m_MaxRenderables(maxRenderables)
, m_NumRenderables(0)
{

}

void RenderTree::Add(Renderable* child)
{
	if (child->Parent() != nullptr || child->m_Root != this)
	{
		child->Detach();

		m_Tree.push_back(child);
		child->SetRoot(this);
		OnChildAdded(child);
	}
}

void RenderTree::Remove(Renderable* child)
{
	if (child->Parent() == nullptr && child->m_Root == this)
	{
		auto It = std::find(m_Tree.begin(), m_Tree.end(), child);
		m_Tree.erase(It);
		OnChildRemoved(child);
		child->SetRoot(nullptr);
	}
}

void RenderTree::Update()
{
	//SKTD: Check for dirty children
}

//-- Private Methods --//
//=====================//

void RenderTree::OnChildAdded(Renderable* newChild)
{
	AddChildToList(newChild);

	TreeUpdatedArgs args(newChild);
	m_ChildAdded.OnEvent(this, args);
}

void RenderTree::OnChildRemoved(Renderable* oldChild)
{
	RemoveChildFromList(oldChild);

	TreeUpdatedArgs args(oldChild);
	m_ChildRemoved.OnEvent(this, args);
}

void RenderTree::AddChildToList(Renderable* child)
{
	SK::Debug::Assert(m_NumRenderables < m_MaxRenderables, "Ran out of space for renderables");

	++m_NumRenderables;

	for (auto grandChild = child->Child(); grandChild; grandChild = grandChild->Next())
		AddChildToList(grandChild);
}

void RenderTree::RemoveChildFromList(Renderable* child)
{
	--m_NumRenderables;
	for (auto grandChild = child->Child(); grandChild; grandChild = grandChild->Next())
		RemoveChildFromList(grandChild);
}