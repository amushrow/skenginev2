//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <SKNetwork/MessageProtocol.h>
#include <SKSystem/Cryptography/CRC.h>
#include <SKNetwork/Peer.h>
using namespace SK::Network;

//-- Declarations --//
//==================//

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

MessageProtocol::MessageProtocol(SK::Network::Peer& peer)
: m_Peer(peer)
{
	
}

void MessageProtocol::ProtocolSend(const uint8_t* packet, uint32_t packetLength, const EndPoint& ep)
{
	m_Peer.Send(packet, packetLength, ep, m_ProtocolId);
}

void MessageProtocol::ProtocolSend(const uint8_t* packet, uint32_t packetLength, RemotePeer& peer, MessageType type)
{
	m_Peer.Send(packet, packetLength, peer, type, m_ProtocolId);
}


//-- Private Methods --//
//=====================//

void MessageProtocol::Init()
{
	String Name = ProtocolName();
	m_ProtocolId = SK::Crypto::CRC32::Compute(Name.Buffer(), static_cast<uint32_t>(Name.Size()));
}
