#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <SKNetwork/Lobby.h>
#include <SKNetwork/EndPoint.h>
#include <SKSystem/Debug.h>
#include <SKSystem/Array.h>

namespace SK
{
	namespace Network
	{
		//---- Types ----//
		//===============//

		enum class PeerUpdateType : uint8_t;
		class RemotePeer;

		struct PeerEP
		{
			EndPoint   EndPoint;
			SK::String Name;
			bool       LocalPlayer;
		};

		class ConnectionRequestArgs
		{
		public:

			//--- Methods ---//
			//===============//

			ConnectionRequestArgs(const EndPoint& from) : m_EndPoint(from), m_Accepted(false) {}
			
			EndPoint EndPoint() const { return m_EndPoint; }
			bool     Accepted() const { return m_Accepted; }
			void     Accept()         { m_Accepted = true; }
			void     Decline()        { m_Accepted = false; }

		private:

			//-- Variables --//
			//===============//

			SK::Network::EndPoint m_EndPoint;
			bool                  m_Accepted;
		};

		class ConnectionFailedArgs
		{
		public:

			//--- Methods ---//
			//===============//

			ConnectionFailedArgs(const EndPoint& from) : m_EndPoint(from) {}

			EndPoint EndPoint() const { return m_EndPoint; }

		private:

			//-- Variables --//
			//===============//

			SK::Network::EndPoint m_EndPoint;
		};

		class ConnectionSuccessArgs
		{
		public:

			//--- Methods ---//
			//===============//

			ConnectionSuccessArgs(RemotePeer* remotePeer) : m_RemotePeer(remotePeer) {}

			RemotePeer* RemotePeer() const { return m_RemotePeer; }

		private:

			//-- Variables --//
			//===============//

			SK::Network::RemotePeer* m_RemotePeer;
		};

		class MessageReceivedArgs
		{
		public:

			//--- Methods ---//
			//===============//

			MessageReceivedArgs(RemotePeer* remotePeer, SK::Array<uint8_t> data, uint32_t protocol);
			MessageReceivedArgs(EndPoint ep, SK::Array<uint8_t> data, uint32_t protocol);

			RemotePeer*        RemotePeer() const { return m_RemotePeer; }
			EndPoint           EndPoint() const   { return m_EndPoint; }
			SK::Array<uint8_t> Data() const       { return m_Data; }
			uint32_t           Protocol() const   { return m_Protocol; }

		private:

			//-- Variables --//
			//===============//

			SK::Network::RemotePeer* m_RemotePeer;
			SK::Network::EndPoint    m_EndPoint;
			SK::Array<uint8_t>       m_Data;
			uint32_t                 m_Protocol;
		};

		class LobbyCreatedArgs
		{
		public:

			//--- Methods ---//
			//===============//

			LobbyCreatedArgs(const Lobby& lobby) : m_Lobby(lobby) { }

			const Lobby& Lobby() const { return m_Lobby; }

		private:

			//-- Variables --//
			//===============//

			const SK::Network::Lobby& m_Lobby;
		};

		class LobbyFoundArgs
		{
		public:

			//--- Methods ---//
			//===============//

			LobbyFoundArgs(const std::vector<Lobby>& lobbies) : m_Lobbies(lobbies) { }

			const std::vector<Lobby>& Lobbies() const { return m_Lobbies; }

		private:

			//-- Variables --//
			//===============//

			const std::vector<Lobby>& m_Lobbies;
		};

		class LobbyArgs
		{
		public:
			
			//--- Methods ---//
			//===============//

			LobbyArgs(uint64_t lobbyId) : m_LobbyId(lobbyId) { }

			uint64_t                         LobbyId() const { return m_LobbyId; }

		private:

			//-- Variables --//
			//===============//

			uint64_t                         m_LobbyId;
		};

		class ReceivedPeersArgs
		{
		public:

			//--- Methods ---//
			//===============//

			ReceivedPeersArgs(uint64_t lobbyId, const std::vector<PeerEP>& peers) : m_LobbyId(lobbyId), m_Peers(peers) { }

			uint64_t                   LobbyId() const { return m_LobbyId; }
			const std::vector<PeerEP>& Peers() const   { return m_Peers; }

		private:

			//-- Variables --//
			//===============//

			uint64_t                   m_LobbyId;
			const std::vector<PeerEP>& m_Peers;
		};

		class PeerUpdateArgs
		{
		public:

			//--- Methods ---//
			//===============//

			PeerUpdateArgs(uint64_t lobbyId, const EndPoint& ep, PeerUpdateType update)
				: m_LobbyId(lobbyId)
				, m_EndPoint(ep)
				, m_UpdateType(update)
			{ }

			uint64_t       LobbyId() const { return m_LobbyId; }
			EndPoint       EndPoint() const { return m_EndPoint; }
			PeerUpdateType UpdateType() const { return m_UpdateType; }

		private:

			//-- Variables --//
			//===============//

			uint64_t              m_LobbyId;
			SK::Network::EndPoint m_EndPoint;
			PeerUpdateType        m_UpdateType;
		};
	}
}