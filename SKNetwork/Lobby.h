#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <cstddef>
#include <unordered_map>
#include <SKSystem/Text/String.h>
#include <SKSystem/IO/Serialize.h>

namespace SK
{
	namespace Network
	{
		//---- Types ----//
		//===============//

		typedef std::unordered_map<SK::String, SK::String> LobbyFilters;

		class Lobby
		{
		public:

			//---- Types ----//
			//===============//

			friend struct SK::IO::Serialize<Lobby>;
			friend class LobbyServer;

			struct PlatformData;
			static const uint64_t INVALID_ID = -1;

			//--- Methods ---//
			//===============//

			Lobby(const SK::String& name, uint16_t maxNbPlayers);
			Lobby();

			const SK::String&   Name() const;
			uint64_t            LobbyId() const;
			uint16_t            NbPlayers() const;
			uint16_t            MaxNbPlayers() const;
			const LobbyFilters& Filters() const;
			void                AddFilter(const SK::String& key, const SK::String& value);
			void                RemoveFilter(const SK::String& key);

		private:

			//--- Methods ---//
			//===============//

			void                LobbyId(uint64_t id);
			void                NbPlayers(uint16_t nbPlayers);

			//-- Variables --//
			//===============//

			PlatformData* m_Platform;
		};

	}
}