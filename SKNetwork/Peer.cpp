//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <SKNetwork/Peer.h>
#include <SKSystem/Cryptography/CRC.h>
using namespace SK::Network;

//-- Declarations --//
//==================//

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

void Peer::Send(const uint8_t* packet, uint32_t packetLength, const EndPoint& ep)
{
	Send(packet, packetLength, ep, 0);
}

void Peer::Send(const uint8_t* packet, uint32_t packetLength, RemotePeer& peer, MessageType type)
{
	Send(packet, packetLength, peer, type, 0);
}

//-- Private Methods --//
//=====================//

void Peer::OnMessageReceived(MessageReceivedArgs& args)
{
	for (MessageProtocol* Protocol : m_Protocols)
	{
		if (Protocol->ProtocolId() == args.Protocol())
		{
			Protocol->HandleMessage(args);
			return;
		}
	}

	if (args.RemotePeer() != nullptr || m_AllowUnsolicited)
	{
		m_MessageReceived.OnEvent(this, args);
	}
}
