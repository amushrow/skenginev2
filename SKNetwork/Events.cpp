//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <SKNetwork/Events.h>
#include <SKNetwork/Peer.h>
#include <SKNetwork/RemotePeer.h>
using namespace SK::Network;

//-- Declarations --//
//==================//

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

MessageReceivedArgs::MessageReceivedArgs(SK::Network::RemotePeer* remotePeer, SK::Array<uint8_t> data, uint32_t protocol)
: m_RemotePeer(remotePeer)
, m_Data(data)
, m_Protocol(protocol)
{
	m_EndPoint = remotePeer->EndPoint();
}

MessageReceivedArgs::MessageReceivedArgs(SK::Network::EndPoint ep, SK::Array<uint8_t> data, uint32_t protocol)
: m_RemotePeer(nullptr)
, m_EndPoint(ep)
, m_Data(data)
, m_Protocol(protocol)
{

}

//-- Private Methods --//
//=====================//

