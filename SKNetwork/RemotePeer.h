#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <SKSystem/Text/String.h>
#include <SKSystem/Time/Time.h>
#include <SKNetwork/EndPoint.h>

namespace SK
{
	namespace Network
	{
		//---- Types ----//
		//===============//

		class RemotePeer
		{
		public:

			//---- Types ----//
			//===============//

			struct Implementation;

			//--- Methods ---//
			//===============//

			RemotePeer(const EndPoint& ep);
			RemotePeer(const RemotePeer& other) = delete;
			RemotePeer& operator=(const RemotePeer& other) = delete;

			SK::Time::Duration    PingTime() const;
			const SK::String&     Name() const;
			const EndPoint&       EndPoint() const { return m_EndPoint; }

			const Implementation* Data() const { return m_Implementation; }
			Implementation*       Data()       { return m_Implementation; }

		private:

			//-- Variables --//
			//===============//

			Implementation*       m_Implementation;
			SK::Network::EndPoint m_EndPoint;
		};
	}
}