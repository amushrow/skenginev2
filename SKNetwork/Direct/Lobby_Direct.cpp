//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <SKNetwork/Lobby.h>
#include <SKNetwork/Direct/Implementations.h>
using namespace SK::Network;
using namespace SK::IO;

//-- Declarations --//
//==================//

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

Lobby::Lobby()
: m_Platform(new PlatformData("", 0))
{
	
}

Lobby::Lobby(const SK::String& name, uint16_t maxPlayers)
: m_Platform(new PlatformData(name, maxPlayers))
{

}

const SK::String& Lobby::Name() const
{
	return m_Platform->Name;
}

uint64_t Lobby::LobbyId() const
{
	return m_Platform->Id;
}

uint16_t Lobby::NbPlayers() const
{
	return m_Platform->NbPlayers;
}

uint16_t Lobby::MaxNbPlayers() const
{
	return m_Platform->MaxNbPlayers;
}

const LobbyFilters& Lobby::Filters() const
{
	return m_Platform->Filters;
}

void Lobby::AddFilter(const SK::String& key, const SK::String& value)
{
	m_Platform->Filters[key] = value;
}

void Lobby::RemoveFilter(const SK::String& key)
{
	m_Platform->Filters.erase(key);
}

//-- Private Methods --//
//=====================//

void Lobby::LobbyId(uint64_t id)
{
	m_Platform->Id = id;
}

void Lobby::NbPlayers(uint16_t nbPlayers)
{
	m_Platform->NbPlayers = nbPlayers;
}