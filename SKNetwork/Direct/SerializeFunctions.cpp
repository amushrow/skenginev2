//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <SKNetwork/Direct/Implementations.h>
#include <SKNetwork/Lobby.h>
#include <SKSystem/IO/Serialize.h>
using namespace SK::IO;
using namespace SK::Network;

//-- Declarations --//
//==================//

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

template<>
template<SerializeMode Mode>
bool SK::IO::Serialize<Packet>::operator()(Serializer<Mode>& serializer, Packet& packet)
{
	serializer(packet.Ack);
	serializer(packet.GlobalId);
	serializer(packet.OrderedId);
	serializer(packet.Message);
	serializer(packet.Protocol);

	return true;
}

template<>
template<SerializeMode Mode>
bool SK::IO::Serialize<MultipartPacket>::operator()(Serializer<Mode>& serializer, MultipartPacket& packet)
{
	serializer(packet.Packet);
	serializer(packet.DataOffset);
	serializer(packet.TotalDataLength);
	serializer(packet.SharedId);

	return true;
}

template<>
template<>
bool Serialize<Lobby>::operator()(Serializer<SerializeMode::Write>& serializer, Lobby& lobby)
{
	serializer(lobby.m_Platform->Id, lobby.m_Platform->NbPlayers, lobby.m_Platform->MaxNbPlayers, lobby.m_Platform->Name);
	serializer(static_cast<uint32_t>(lobby.m_Platform->Filters.size()));
	for (auto& key : lobby.m_Platform->Filters)
	{
		serializer(key.first, key.second);
	}

	return true;
}

template<>
template<>
bool Serialize<Lobby>::operator()(Serializer<SerializeMode::Read>& serializer, Lobby& lobby)
{
	serializer(lobby.m_Platform->Id, lobby.m_Platform->NbPlayers, lobby.m_Platform->MaxNbPlayers, lobby.m_Platform->Name);

	uint32_t NbKeys;
	if (serializer(NbKeys))
	{
		for (std::size_t i = 0; i < NbKeys; ++i)
		{
			SK::String Key, Value;
			serializer(Key, Value);
			lobby.m_Platform->Filters[Key] = Value;
		}
	}

	return true;
}

SerializerInstantiate(MultipartPacket);
SerializerInstantiate(Packet);

//-- Private Methods --//
//=====================//
