#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <SKSystem/Win32/Win32.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <SKNetwork/Direct/Implementations.h>

namespace SK
{
	namespace Network
	{
		//---- Types ----//
		//===============//

		struct EndPoint::PlatformData
		{
			sockaddr_in Address;
		};

		struct Peer::Implementation::PlatformData
		{
			SOCKET Socket;
		};
	}
}