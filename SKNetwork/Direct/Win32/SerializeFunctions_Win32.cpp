//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <SKNetwork/Direct/Win32/Network.h>
#include <SKNetwork/EndPoint.h>
#include <SKSystem/IO/Serialize.h>
using namespace SK::IO;
using namespace SK::Network;

//-- Declarations --//
//==================//

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

template<>
template<SerializeMode Mode>
bool SK::IO::Serialize<EndPoint>::operator()(Serializer<Mode>& serializer, EndPoint& endPoint)
{
	serializer(endPoint.Data()->Address.sin_family);
	serializer(endPoint.Data()->Address.sin_port);
	serializer(endPoint.Data()->Address.sin_addr.S_un.S_addr);

	return true;
}

SerializerInstantiate(EndPoint);

//-- Private Methods --//
//=====================//
