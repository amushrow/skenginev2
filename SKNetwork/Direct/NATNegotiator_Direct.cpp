//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <SKNetwork/Peer.h>
#include <SKNetwork/NATNegotator.h>
#include <SKSystem/IO/MemoryStream.h>
using namespace SK::Network;

//-- Declarations --//
//==================//

namespace
{
	enum class NATNegMessage : uint8_t
	{
		Open,
		RelayPunchthrough,
		Punchthrough,
	};
}

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

void NATNegotiator::Connect(const EndPoint& negotiator, const EndPoint& remote)
{
	uint8_t Message[64];
	SK::IO::MemoryStream MessageStream(Message, sizeof(Message), false);
	MessageStream.Write(NATNegMessage::RelayPunchthrough);
	MessageStream.Write(remote);
	ProtocolSend(Message, static_cast<uint32_t>(MessageStream.Position()), negotiator);

	NATNegMessage Open = NATNegMessage::Open;
	ProtocolSend(reinterpret_cast<uint8_t*>(&Open), 1, remote);
}

//-- Private Methods --//
//=====================//

void NATNegotiator::HandleMessage(MessageReceivedArgs& args)
{
	SK::IO::MemoryStream MessageStream(args.Data().Get(), args.Data().Length());
	NATNegMessage Id = MessageStream.Read<NATNegMessage>();

	switch (Id)
	{
	case NATNegMessage::Open:
		{
			// The door is open, let's go
			Peer().Connect(args.EndPoint());
		}
		break;

	case NATNegMessage::RelayPunchthrough:
		{
			EndPoint Target = MessageStream.Read<EndPoint>();
			uint8_t Message[64];
			SK::IO::MemoryStream ResponseStream(Message, sizeof(Message), false);
			ResponseStream.Write(NATNegMessage::Punchthrough);
			ResponseStream.Write(args.EndPoint());
			ProtocolSend(Message, static_cast<uint32_t>(ResponseStream.Position()), Target);
		}
		break;

	case NATNegMessage::Punchthrough:
		{
			EndPoint Target = MessageStream.Read<EndPoint>();
			NATNegMessage Open = NATNegMessage::Open;
			ProtocolSend(reinterpret_cast<uint8_t*>(&Open), 1, Target);
		}
		break;
	}
}


