#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright © 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <netinet/in.h>
#include <sys/socket.h>
#include <netdb.h>
#include <fcntl.h>
#include <unistd.h>
#include <SKNetwork/Direct/Implementations.h>

namespace SK
{
	namespace Network
	{
		//---- Types ----//
		//===============//

		struct EndPoint::PlatformData
		{
			sockaddr_in Address;
		};

		struct Peer::Implementation::PlatformData
		{
			int Socket;
		};
	}
}