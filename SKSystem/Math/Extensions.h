#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <xmmintrin.h>

//--- Methods ---//
//===============//
__m128 _mm_sin_ps(__m128 _A);
__m128 _mm_cos_ps(__m128 _A);
void _mm_sincos_ps(__m128 _A, __m128& out_Sin, __m128& out_Cos);
