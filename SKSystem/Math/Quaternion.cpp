//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <cmath>
#include <smmintrin.h>
#include "Extensions.h"
#include "Vector.h"
#include "Matrix.h"
#include "Quaternion.h"

using namespace SK::Math;

//-- Declarations --//
//==================//

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

namespace
{
	static const __m128 ZERO = _mm_set_ps1(0);
	static const __m128 ONE = _mm_set_ps1(1);
	static const __m128 TWO = _mm_set_ps1(2);
	static const __m128 CLEARMASK = _mm_castsi128_ps(_mm_set_epi32(-1, 0, 0, 0));
	static const __m128 SIGNMASK = _mm_castsi128_ps(_mm_set1_epi32(0x80000000));
	static const __m128 CONJUGATE_MASK = _mm_castsi128_ps(_mm_set_epi32(0, 0x80000000, 0x80000000, 0x80000000));

	static const __m128 MAT2QUAT_FLIP0 = _mm_castsi128_ps(_mm_set_epi32(0, 0x80000000, 0x80000000, 0));
	static const __m128 MAT2QUAT_FLIP1 = _mm_castsi128_ps(_mm_set_epi32(0, 0x80000000, 0, 0x80000000));
	static const __m128 MAT2QUAT_FLIP2 = _mm_castsi128_ps(_mm_set_epi32(0, 0, 0x80000000, 0x80000000));
	static const __m128 MAT2QUAT_FLIP3 = _mm_castsi128_ps(_mm_set_epi32(0x80000000, 0x80000000, 0, 0x80000000));
	static const __m128 MAT2QUAT_FLIP4 = _mm_castsi128_ps(_mm_set_epi32(0, 0, 0x80000000, 0));
}

//-- Public Methods --//
//====================//

Quaternion::Quaternion(float x, float y, float z, float w)
{
	Data = _mm_setr_ps(x, y, z, w);
}

Quaternion::Quaternion(const float4& data)
{
	Data = _mm_loadu_ps(data);
}

float Quaternion::Length() const
{
	return _mm_cvtss_f32(_mm_sqrt_ss(_mm_dp_ps(Data, Data, 0xFF)));
}

void Quaternion::Normalise()
{
	Data = _mm_div_ps(Data, _mm_sqrt_ps(_mm_dp_ps(Data, Data, 0xFF)));
}

Euler Quaternion::GetEuler() const
{
	Euler Result;
	ALIGN(16) float4 Parts;
	ALIGN(16) float4 SquaredParts;
	_mm_store_ps(SquaredParts, _mm_mul_ps(Data, Data));
	_mm_store_ps(Parts, Data);

	Result.Roll = atan2(2.f * (Parts.x * Parts.y + Parts.z * Parts.w), SquaredParts.x - SquaredParts.y - SquaredParts.z + SquaredParts.w);
	Result.Yaw = asin(-2.f * (Parts.x * Parts.z - Parts.y * Parts.w));
	Result.Pitch = atan2(2.f * (Parts.y * Parts.z + Parts.x * Parts.w), -SquaredParts.x - SquaredParts.y + SquaredParts.z + SquaredParts.w);
	
	return Result;
}

const Quaternion& Quaternion::Conjugate()
{
	Data = _mm_xor_ps(Data, CONJUGATE_MASK);
	return *this;
}

Quaternion Quaternion::Look(const Vector& forward, const Vector& up)
{
	Vector OrthoUp = Vector::MakeOrthogonal(forward, up);
	Vector RightVec = Vector::Cross(OrthoUp, forward);

	float4 Up = OrthoUp.Get();
	float4 Forward = forward.Get();
	float4 Right = RightVec.Get();
	float4 Orientation;

	Orientation.w = sqrt(1.0f + Right.x + Up.y + Forward.z) / 2.0f;
	float R = 1.0f / (4.0f * Orientation.w);
	Orientation.x = (Forward.y - Up.z) * R;
	Orientation.y = (Right.z - Forward.x) * R;
	Orientation.z = (Up.x - Right.y) * R;

	return Quaternion(Orientation);
}

bool Quaternion::operator == (const Quaternion& rhs) const
{
	return _mm_movemask_ps(_mm_cmpneq_ps(Data, rhs.Data)) == 0;
}

bool Quaternion::operator != (const Quaternion& rhs) const
{
	return _mm_movemask_ps(_mm_cmpneq_ps(Data, rhs.Data)) != 0;
}

const Quaternion& Quaternion::operator /= (float rhs)
{
	__m128 Div = _mm_set_ps1(rhs);
	Data = _mm_div_ps(Data, Div);
	return *this;
}

const Quaternion& Quaternion::operator *= (float rhs)
{
	__m128 Mul = _mm_set_ps1(rhs);
	Data = _mm_div_ps(Data, Mul);
	return *this;
}

const Quaternion& Quaternion::operator *= (const Quaternion& rhs)
{
	__m128 wzyx = _mm_shuffle_ps(Data, Data, _MM_SHUFFLE(0, 1, 2, 3));
	__m128 baba = _mm_shuffle_ps(rhs.Data, rhs.Data, _MM_SHUFFLE(0, 1, 0, 1));
	__m128 dcdc = _mm_shuffle_ps(rhs.Data, rhs.Data, _MM_SHUFFLE(2, 3, 2, 3));

	__m128 ZnXWY = _mm_hsub_ps(_mm_mul_ps(Data, baba), _mm_mul_ps(wzyx, dcdc));
	__m128 XZYnW = _mm_hadd_ps(_mm_mul_ps(Data, dcdc), _mm_mul_ps(wzyx, baba));

	__m128 XZWY = _mm_addsub_ps(_mm_shuffle_ps(XZYnW, ZnXWY, _MM_SHUFFLE(3, 2, 1, 0)),
		_mm_shuffle_ps(ZnXWY, XZYnW, _MM_SHUFFLE(2, 3, 0, 1)));

	Data = _mm_shuffle_ps(XZWY, XZWY, _MM_SHUFFLE(2, 1, 3, 0));
	return *this;
}

const Quaternion& Quaternion::operator += (const Quaternion& rhs)
{
	Data = _mm_add_ps(Data, rhs.Data);
	return *this;
}

const Quaternion& Quaternion::operator -= (const Quaternion& rhs)
{
	Data = _mm_sub_ps(Data, rhs.Data);
	return *this;
}

const Quaternion Quaternion::operator * (const Quaternion& rhs) const
{
	Quaternion Result;
	__m128 wzyx = _mm_shuffle_ps(Data, Data, _MM_SHUFFLE(0, 1, 2, 3));
	__m128 baba = _mm_shuffle_ps(rhs.Data, rhs.Data, _MM_SHUFFLE(0, 1, 0, 1));
	__m128 dcdc = _mm_shuffle_ps(rhs.Data, rhs.Data, _MM_SHUFFLE(2, 3, 2, 3));

	__m128 ZnXWY = _mm_hsub_ps(_mm_mul_ps(Data, baba), _mm_mul_ps(wzyx, dcdc));
	__m128 XZYnW = _mm_hadd_ps(_mm_mul_ps(Data, dcdc), _mm_mul_ps(wzyx, baba));

	__m128 XZWY = _mm_addsub_ps(_mm_shuffle_ps(XZYnW, ZnXWY, _MM_SHUFFLE(3, 2, 1, 0)),
		_mm_shuffle_ps(ZnXWY, XZYnW, _MM_SHUFFLE(2, 3, 0, 1)));

	Result.Data = _mm_shuffle_ps(XZWY, XZWY, _MM_SHUFFLE(2, 1, 3, 0));
	return Result;
}

const Quaternion Quaternion::operator - (const Quaternion& rhs) const
{
	Quaternion Result;
	Result.Data = _mm_sub_ps(Data, rhs.Data);
	return Result;
}

const Quaternion Quaternion::operator + (const Quaternion& rhs) const
{
	Quaternion Result;
	Result.Data = _mm_add_ps(Data, rhs.Data);
	return Result;
}

const Quaternion Quaternion::operator / (float rhs)
{
	Quaternion Result;
	Result.Data = _mm_div_ps(Data, _mm_set_ps1(rhs));
	return Result;
}

const Quaternion Quaternion::operator - () const
{
	Quaternion Result;
	Result.Data = _mm_xor_ps(Data, SIGNMASK);
	return Result;
}


Quaternion Quaternion::FromEuler(const Euler& rhs)
{
	Quaternion Result;
	ALIGN(16) float4 SinData;
	ALIGN(16) float4 CosData;
	ALIGN(16) float4 ResultData;

	__m128 Angle = _mm_setr_ps(0.5f*rhs.Pitch, 0.5f*rhs.Yaw, 0.5f*rhs.Roll, 0.0f);
	__m128 Cos, Sin;
	_mm_sincos_ps(Angle, Sin, Cos);
	_mm_store_ps(SinData, Sin);
	_mm_store_ps(CosData, Cos);

	// and now compute quaternion
	ResultData.x = CosData.z*CosData.y*SinData.x - SinData.z*SinData.y*CosData.x;
	ResultData.y = CosData.z*SinData.y*CosData.x + SinData.z*CosData.y*SinData.x;
	ResultData.z = SinData.z*CosData.y*CosData.x - CosData.z*SinData.y*SinData.x;
	ResultData.w = CosData.z*CosData.y*CosData.x + SinData.z*SinData.y*SinData.x;

	Result.Data = _mm_load_ps(ResultData);
	return Result;
}

Quaternion Quaternion::FromAxis(const Vector& axis, float a)
{
	Quaternion Result;
	ALIGN(16) float4 ResultData = axis.Get() * sin(0.5f * a);
	ResultData.w = cos(0.5f * a);
	Result.Data = _mm_load_ps(ResultData);
	return Result;
}

Quaternion Quaternion::FromAxis(const float4& axis, float a)
{
	Quaternion Result;
	ALIGN(16) float4 ResultData = axis * sin(0.5f * a);
	ResultData.w = cos(0.5f * a);
	Result.Data = _mm_load_ps(ResultData);
	return Result;
}

Quaternion Quaternion::FromMatrix(const Matrix& rhs)
{
	Quaternion Result;

	__m128 X = _mm_xor_ps(_mm_shuffle_ps(rhs.Row0, rhs.Row0, 0x00), MAT2QUAT_FLIP0);
	__m128 Y = _mm_xor_ps(_mm_shuffle_ps(rhs.Row1, rhs.Row1, 0x55), MAT2QUAT_FLIP1);
	__m128 Z = _mm_xor_ps(_mm_shuffle_ps(rhs.Row2, rhs.Row2, 0xAA), MAT2QUAT_FLIP2);
	__m128 Left = _mm_shuffle_ps(rhs.Row1, rhs.Row0, _MM_SHUFFLE(1, 2, 2, 0));
	__m128 Right = _mm_xor_ps(_mm_shuffle_ps(rhs.Row2, rhs.Row1, _MM_SHUFFLE(0, 0, 0, 1)), MAT2QUAT_FLIP3);
	Left = _mm_xor_ps(_mm_shuffle_ps(Left, Left, 0x39), MAT2QUAT_FLIP4);
	__m128 Signs = _mm_add_ps(Left, Right);
	__m128 ClearComponents = _mm_or_ps(CLEARMASK, _mm_cmpneq_ps(Signs, ZERO));
	Signs = _mm_and_ps(SIGNMASK, Signs);
	Result.Data = _mm_and_ps(ClearComponents, _mm_or_ps(Signs, _mm_div_ps(_mm_max_ps(ZERO, _mm_sqrt_ps(_mm_add_ps(ONE, _mm_add_ps(X, _mm_add_ps(Y, Z))))), TWO)));

	return Result;
}

Quaternion Quaternion::Conjugate(const Quaternion& rhs)
{
	Quaternion Result;
	Result.Data = _mm_xor_ps(rhs.Data, CONJUGATE_MASK);
	return Result;
}

float DotProduct(const Quaternion& lhs, const Quaternion& rhs)
{
	return _mm_cvtss_f32(_mm_dp_ps(lhs.Data, rhs.Data, 0xFF));
}

//-- Private Methods --//
//=====================//