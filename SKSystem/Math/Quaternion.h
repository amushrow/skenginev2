#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <xmmintrin.h>
#include <SKSystem/Memory.h>
#include <SKSystem/Math/Euler.h>
#include <SKSystem/Math/Types.h>

namespace SK
{
	namespace Math
	{
		//---- Types ----//
		//===============//

		class Matrix;
		class Vector;
		class Quaternion
		{
		public:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			Quaternion() {}
			Quaternion(float x, float y, float z, float w);
			Quaternion(const float4& data);
			Quaternion(__m128 data) : Data(data) {}

			void*             operator new(std::size_t size){ return SK::AlignedNew(16, size); }
			void*             operator new[](std::size_t size) { return SK::AlignedNew(16, size); }
			void              operator delete(void* mem)       { SK::AlignedDelete(mem); }
			void              operator delete[](void* mem)     { SK::AlignedDelete(mem); }

			void              Normalise();
			float             Length() const;
			Euler             GetEuler() const;
			const Quaternion& Conjugate();

			bool              operator == (const Quaternion& rhs) const;
			bool              operator != (const Quaternion& rhs) const;
			const Quaternion& operator /= (float rhs);
			const Quaternion& operator *= (float rhs);
			const Quaternion& operator *= (const Quaternion& rhs);
			const Quaternion& operator += (const Quaternion& rhs);
			const Quaternion& operator -= (const Quaternion& rhs);
			const Quaternion  operator * (const Quaternion& rhs) const;
			const Quaternion  operator - (const Quaternion& rhs) const;
			const Quaternion  operator + (const Quaternion& rhs) const;
			const Quaternion  operator / (float rhs);
			const Quaternion  operator - () const;

			static Quaternion FromEuler(const Euler& rhs);
			static Quaternion FromAxis(const Vector& axis, float a);
			static Quaternion FromAxis(const float4& axis, float a);
			static Quaternion FromMatrix(const Matrix& rhs);
			static Quaternion Conjugate(const Quaternion& rhs);
			static Quaternion Look(const Vector& forward, const Vector& up);
			static float      DotProduct(const Quaternion& lhs, const Quaternion& rhs);

			//-- Variables --//
			//===============//

			__m128 Data;
		};

		//--- Methods ---//
		//===============//

		const Quaternion operator * (float lhs, const Quaternion& rhs);
		const Quaternion operator * (const Quaternion& lhs, float rhs);
	}
}