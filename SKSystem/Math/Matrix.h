#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <cstddef>
#include <xmmintrin.h>
#include <SKSystem/Memory.h>
#include <SKSystem/Math/Euler.h>
#include <SKSystem/Math/Types.h>

namespace SK
{
	namespace Math
	{
		//---- Types ----//
		//===============//


		class Vector;
		class Quaternion;
		class Matrix
		{
		public:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			static const Matrix& Identity();

			Matrix() {}
			Matrix(const float* data);
			Matrix(const float4& row0, const float4& row1, const float4& row2, const float4& row3);
			Matrix(float xx, float xy, float xz, float xw,
				float yx, float yy, float yz, float yw,
				float zx, float zy, float zz, float zw,
				float wx, float wy, float wz, float ww);

			void*         operator new(std::size_t size){ return SK::AlignedNew(16, size); }
			void*         operator new[](std::size_t size) { return SK::AlignedNew(16, size); }
			void          operator delete(void* mem)       { SK::AlignedDelete(mem); }
			void          operator delete[](void* mem)     { SK::AlignedDelete(mem); }

			const Matrix  operator * (const Matrix& rhs) const;
			void          operator *= (const Matrix& rhs);
			bool          operator == (const Matrix& rhs) const;
			bool          operator != (const Matrix& rhs) const;

			float4x4      Get() const;
			float4        Row(int row) const;
			void          Row(int row, float4 data);

			Matrix        Transpose() const;
			Matrix        Inverse() const;

			static Matrix RotationX(float a);
			static Matrix RotationY(float a);
			static Matrix RotationZ(float a);
			static Matrix RotationArbi(const Vector& axis, float a);
			static Matrix Rotation(const Euler& rhs);
			static Matrix Translation(float dx, float dy, float dz);
			static Matrix Translation(float4 translation);
			static Matrix Scale(float x, float y, float z);
			static Matrix Scale(float4 scale);
			static Matrix FromQuaternion(const Quaternion& rhs);

			//-- Variables --//
			//===============//

			__m128 Row0;
			__m128 Row1;
			__m128 Row2;
			__m128 Row3;
		};

		//--- Methods ---//
		//===============//
	}
}