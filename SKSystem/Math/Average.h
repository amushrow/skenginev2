#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <algorithm>

namespace SK
{
	namespace Math
	{
		//---- Types ----//
		//===============//

		template<typename T, int Size>
		class Average
		{
		public:

			//--- Methods ---//
			//===============//

			Average() : m_ValueIndex(0), m_NumValues(0)
			{

			}

			void Clear()
			{
				m_NumValues = m_ValueIndex = 0;
			}

			void Push(T value)
			{
				m_Values[m_ValueIndex] = value;
				m_NumValues = std::min(m_NumValues + 1, Size);
				if (++m_ValueIndex == Size)
				{
					m_ValueIndex = 0;
				}
			}

			T Mean()
			{
				T Result = T(0);
				
				if (m_NumValues > 0)
				{
					for (int i = 0; i < m_NumValues; ++i)
					{
						Result += m_Values[i];
					}

					Result /= m_NumValues;
				}

				return Result;
			}

			T Median()
			{
				memcpy(m_SortedValues, m_Values, sizeof(m_Values));
				std::sort(m_SortedValues, m_SortedValues + m_NumValues);

				return m_SortedValues[m_NumValues / 2];
			}

		private:

			//-- Variables --//
			//===============//

			int m_ValueIndex;
			int m_NumValues;
			T   m_Values[Size];
			T   m_SortedValues[Size];

		};
	}
}