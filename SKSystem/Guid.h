#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright © 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <cstdint>
#include <SKSystem/Text/String.h>

namespace SK
{
	//---- Types ----//
	//===============//

	class Guid
	{
	public:
		//---- Types ----//
		//===============//
		
		struct _data
		{
			uint32_t Data1;
			uint16_t Data2;
			uint16_t Data3;
			uint8_t  Data4[8];
		};

		//--- Methods ---//
		//===============//

		Guid();
		Guid(uint32_t data1, int16_t data2, uint16_t data3, const uint8_t data4[8]);
		Guid(const Guid& rhs);
		explicit Guid(const String& string);
		bool operator!=(const Guid& rhs) const;
		bool operator==(const Guid& rhs) const;

		static Guid Generate();

		//-- Variables --//
		//===============//

		union
		{
			_data Parts;
			uint8_t Data[16];
		};
	};

	//--- Methods ---//
	//===============//
}