//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <SKSystem/Utils.h>
#include <SKSystem/IO/MemoryStream.h>
#include "Encoding.h"

using namespace SK;
using namespace SK::Encoding;

//-- Declarations --//
//==================//

namespace
{
	void WriteCodePoint(CodePoint codePoint, IO::Stream& stream);
}

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

Array<uint8_t> UTF16::GetBytes(const String& string, bool nullTerminated)
{
	const uint8_t* StringData = string.Buffer();

	// Calculate the number of bytes required
	std::size_t Position = 0;
	std::size_t BufferSize = 0;
	CodePoint CP = 0;
	while ((CP = UTF8::GetNextCodePoint(StringData, Position, string.Size())))
	{
		BufferSize += 2;
		if (CP > 0xFFFF)
		{
			BufferSize += 2;
		}
	}

	if (nullTerminated) BufferSize += 2;

	uint8_t* Buffer = new uint8_t[BufferSize];
	IO::MemoryStream UTF16Buffer(Buffer, BufferSize, false);
	Position = 0;
	while ((CP = UTF8::GetNextCodePoint(StringData, Position, string.Size())))
	{
		if (CP <= 0xFFFF)
		{
			UTF16Buffer.Write(uint16_t(CP));
		}
		else
		{
			uint32_t CPShort = CP - 0x10000;
			uint32_t HighBits = CPShort >> 10;
			uint32_t LowBits = CPShort & 0x3FF;
			UTF16Buffer.Write(uint16_t(0xD800 + HighBits));
			UTF16Buffer.Write(uint16_t(0xDC00 + LowBits));
		}
	}

	if (nullTerminated)
	{
		UTF16Buffer.Write(uint16_t(0));
	}

	return Array<uint8_t>(Buffer, BufferSize);
}

String UTF16::FromBytes(const uint8_t* data, std::size_t length)
{
	std::size_t StringLength = 0;
	std::size_t Position = 0;
	CodePoint CP = 0;
	std::size_t BufferSize = 0;
	while ((CP = GetNextCodePoint(data, Position, length)))
	{
		StringLength++;

		if (CP < 0x80)         { BufferSize++; }
		else if (CP < 0x800)   { BufferSize += 2; }
		else if (CP < 0x10000) { BufferSize += 3; }
		else                   { BufferSize += 4; }
	}

	uint8_t* Buffer = new uint8_t[BufferSize];
	IO::MemoryStream UTF8Buffer(Buffer, BufferSize, false, true);
	Position = 0;
	while ((CP = GetNextCodePoint(data, Position, length)))
	{
		WriteCodePoint(CP, UTF8Buffer);
	}

	return String(Buffer, BufferSize, StringLength);
}

String UTF16::FromBytes(const uint8_t* data)
{
	std::size_t StringLength = 0;
	std::size_t Position = 0;
	CodePoint CP = 0;
	std::size_t BufferSize = 0;
	while ((CP = GetNextCodePoint(data, Position)))
	{
		StringLength++;

		if (CP < 0x80)         { BufferSize++; }
		else if (CP < 0x800)   { BufferSize += 2; }
		else if (CP < 0x10000) { BufferSize += 3; }
		else                   { BufferSize += 4; }
	}

	uint8_t* Buffer = new uint8_t[BufferSize];
	IO::MemoryStream UTF8Buffer(Buffer, BufferSize, false);
	Position = 0;
	while ((CP = GetNextCodePoint(data, Position)))
	{
		WriteCodePoint(CP, UTF8Buffer);
	}

	return String(Buffer, BufferSize, StringLength);
}

String UTF16::FromBytes(const Array<uint8_t> data)
{
	return FromBytes(data.Get(), data.Length());
}

CodePoint UTF16::GetNextCodePoint(const uint8_t* data, std::size_t& startPosition, std::size_t bufferLength)
{
	if (bufferLength != -1 && startPosition >= bufferLength)
	{
		return 0;
	}

	uint16_t First = *reinterpret_cast<const uint16_t*>(&data[startPosition]);
	if (First != 0) startPosition += 2;

	if (CheckFlags(First, 0xD800))
	{
		uint16_t Second = *reinterpret_cast<const uint16_t*>(&data[startPosition]);
		startPosition += 2;

		return ((CodePoint(First - 0xD800) << 10) | CodePoint(Second - 0xDC00)) + 0x10000;
	}
	else
	{
		return First;
	}
}


//-- Private Methods --//
//=====================//

namespace
{
	void WriteCodePoint(CodePoint codePoint, IO::Stream& stream)
	{
		if (codePoint < 0x80)
		{
			stream.Write(uint8_t(codePoint));
		}
		else if (codePoint < 0x800)
		{
			uint8_t First = 0xC0 | (codePoint >> 6);
			uint8_t Second = 0x80 | (codePoint & 0x3F);
			stream.Write(First);
			stream.Write(Second);
		}
		else if (codePoint < 0x10000)
		{
			uint8_t First = 0xE0 | (codePoint >> 12);
			uint8_t Second = 0x80 | ((codePoint >> 6) & 0x3F);
			uint8_t Third = 0x80 | (codePoint & 0x3F);
			stream.Write(First);
			stream.Write(Second);
			stream.Write(Third);
		}
		else
		{
			uint8_t First = 0xF0 | (codePoint >> 18);
			uint8_t Second = 0x80 | ((codePoint >> 12) & 0x3F);
			uint8_t Third = 0x80 | ((codePoint >> 6) & 0x3F);
			uint8_t Fourth = 0x80 | (codePoint & 0x3F);
			stream.Write(First);
			stream.Write(Second);
			stream.Write(Third);
			stream.Write(Fourth);
		}
	}
}