//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <SKSystem/Utils.h>
#include "Encoding.h"

using namespace SK;
using namespace SK::Encoding;

//-- Declarations --//
//==================//

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

Array<uint8_t> UTF8::GetBytes(const String& string, bool nullTerminated)
{
	std::size_t Size = string.Size();
	if (nullTerminated) Size++;
	uint8_t* Copy = new uint8_t[Size];
	const uint8_t* StringData = string.Buffer();
	memcpy(Copy, StringData, Size);

	if (nullTerminated)
	{
		Copy[Size - 1] = 0x00;
	}
	return Array<uint8_t>(Copy, Size);
}

String UTF8::FromBytes(const uint8_t* data, std::size_t length)
{
	std::size_t StringLength = 0;
	std::size_t Position = 0;
	while (GetNextCodePoint(data, Position, length) > 0)
	{
		StringLength++;
	}

	return String(data, length, StringLength);
}

String UTF8::FromBytes(const uint8_t* data)
{
	std::size_t StringLength = 0;
	std::size_t Position = 0;
	while (GetNextCodePoint(data, Position) > 0)
	{
		StringLength++;
	}
	return String(data, Position, StringLength);
}

String UTF8::FromBytes(const Array<uint8_t> data)
{
	return FromBytes(data.Get(), data.Length());
}

CodePoint UTF8::GetNextCodePoint(const uint8_t* data, std::size_t& startPosition, std::size_t bufferLength)
{
	static uint8_t ByteMask[] = { 192, 224, 240 };
	if (bufferLength != -1 && startPosition >= bufferLength)
	{
		return 0;
	}

	CodePoint CP = 0;
	if (CheckFlags(data[startPosition], ByteMask[2]))
	{
		CP = (int(data[startPosition++]) & 0x07) << 18;
		CP |= (int(data[startPosition++]) & 0x3F) << 12;
		CP |= (int(data[startPosition++]) & 0x3F) << 6;
		CP |= int(data[startPosition++]) & 0x3F;
	}
	else if (CheckFlags(data[startPosition], ByteMask[1]))
	{
		CP = (int(data[startPosition++]) & 0x0F) << 12;
		CP |= (int(data[startPosition++]) & 0x3F) << 6;
		CP |= int(data[startPosition++]) & 0x3F;
	}
	else if (CheckFlags(data[startPosition], ByteMask[0]))
	{
		CP = (int(data[startPosition++]) & 0x1F) << 6;
		CP |= int(data[startPosition++]) & 0x3F;
	}
	else
	{
		CP = data[startPosition];
		if (CP != 0) startPosition++;
	}

	if (bufferLength > 0 && startPosition > bufferLength)
	{
		throw ArgumentException("UTF8::GetNextCodePoint - Invalid data. Read past the end of the buffer.");
	}

	return CP;
}

//-- Private Methods --//
//=====================//