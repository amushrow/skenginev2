#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright © 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <vector>
#include <memory>
#include <string>
#include <cstdint>

namespace SK
{
	//---- Types ----//
	//===============//

	namespace Encoding
	{
		class UTF8;
		class UTF16;
		class UTF32;
	}

	typedef uint32_t CodePoint;

	class String
	{
	public:
		//---- Types ----//
		//===============//

		friend class Encoding::UTF8;
		friend class Encoding::UTF16;
		friend class Encoding::UTF32;

		class Iterator : public std::iterator<std::bidirectional_iterator_tag, CodePoint>
		{
		public:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			Iterator(const char* startChar, const char* buffer) : m_Char(startChar), m_Buffer(buffer) {}
			Iterator(const Iterator& other) : m_Char(other.m_Char), m_Buffer(other.m_Buffer) {}
			Iterator& operator=(const Iterator& other)             { m_Char = other.m_Char; m_Buffer = other.m_Buffer; return *this; }

			bool operator==(const Iterator& other) const           { return m_Char == other.m_Char; }
			bool operator!=(const Iterator& other) const           { return m_Char != other.m_Char; }

			Iterator operator++(int)
			{
				Iterator Tmp(*this);
				this->operator++();
				return Tmp;
			}

			Iterator operator--(int)
			{
				Iterator Tmp(*this);
				this->operator--();
				return Tmp;
			}

			Iterator& operator++();
			Iterator& operator--();
			CodePoint operator*();

			//-- Variables --//
			//===============//

		private:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			//-- Variables --//
			//===============//

			const char* m_Char;
			const char* m_Buffer;
		};

		//--- Methods ---//
		//===============//

		String();
		~String();

		String(const char* string);
		String(const std::string& string);

		String(const String& other);
		String& operator=(const String& other);

		String(String&& other);
		String& operator=(String&& other);

		operator const char*() const { return m_Buffer; }

		bool                     operator==(const String& other) const { return Equals(other); }
		bool                     operator!=(const String& other) const { return !Equals(other); }

		bool                     IsEmpty() const { return m_StringLength == 0; }
		void                     Concat(const String& other);
		void                     Concat(const std::initializer_list<String>& others);
		int                      CompareTo(const String& other) const;
		int                      CompareToCI(const String& other) const;
		bool                     Equals(const String& other) const { return CompareTo(other) == 0; }
		bool                     EqualsCI(const String& other) const { return CompareToCI(other) == 0; }
		bool                     Contains(const String& other) const;
		bool                     StartsWith(const String& other) const;
		bool                     EndsWith(const String& other) const;
		std::size_t              IndexOf(const String& other, std::size_t startIndex = 0) const;
		std::size_t              LastIndexOf(const String& other, std::size_t startIndex = -1) const;
		String&                  Replace(const String& other, const String& replacement);
		String                   Substring(std::size_t index) const;
		String                   Substring(std::size_t index, std::size_t count) const;
		std::vector<String>      Split(const String& delimiter) const;
		std::size_t              Length() const { return m_StringLength; }
		std::size_t              Size() const   { return m_DataSize; }
		const uint8_t*           Buffer() const { return reinterpret_cast<uint8_t*>(m_Buffer); }

		static String            Join(const std::vector<String>& strings, const String& delimiter);
		static String            Join(const std::vector<String>& strings);
		static String            Format(const char* string, ...);
		
		const Iterator           begin() const { return Iterator(m_Buffer, m_Buffer); }
		const Iterator           end() const   { return Iterator(m_Buffer + m_DataSize, m_Buffer); }

		//-- Variables --//
		//===============//

	private:
		//---- Types ----//
		//===============//
		
		//--- Methods ---//
		//===============//

		String(const uint8_t* bytes, std::size_t bufferLength, std::size_t stringLength);
		std::size_t CharIndexToBuffer(std::size_t charIndex) const;

		//-- Variables --//
		//===============//

		char*       m_Buffer;
		std::size_t m_BufferSize;
		std::size_t m_DataSize;
		std::size_t m_StringLength;
	};

	//--- Methods ---//
	//===============//
}


// The following hash was pinched from xstddef
template<>
struct std::hash<SK::String>
{
	std::size_t operator()(const SK::String& val) const
	{
#if defined(_M_X64) || defined(_LP64) || defined(__x86_64) || defined(_WIN64)
		static_assert(sizeof(std::size_t) == 8, "This code is for 64-bit size_t.");
		const std::size_t _FNV_offset_basis = 14695981039346656037ULL;
		const std::size_t _FNV_prime = 1099511628211ULL;

#else /* defined(_M_X64), etc. */
		static_assert(sizeof(std::size_t) == 4, "This code is for 32-bit size_t.");
		const std::size_t _FNV_offset_basis = 2166136261U;
		const std::size_t _FNV_prime = 16777619U;
#endif /* defined(_M_X64), etc. */

		std::size_t _Count = val.Size();
		const uint8_t* _First = val.Buffer();

		std::size_t _Val = _FNV_offset_basis;
		for (std::size_t _Next = 0; _Next < _Count; ++_Next)
		{	// fold in another byte
			_Val ^= (std::size_t) _First[_Next];
			_Val *= _FNV_prime;
		}

#if defined(_M_X64) || defined(_LP64) || defined(__x86_64) || defined(_WIN64)
		static_assert(sizeof(std::size_t) == 8, "This code is for 64-bit size_t.");
		_Val ^= _Val >> 32;

#else /* defined(_M_X64), etc. */
		static_assert(sizeof(std::size_t) == 4, "This code is for 32-bit size_t.");
#endif /* defined(_M_X64), etc. */

		return (_Val);
	}
};