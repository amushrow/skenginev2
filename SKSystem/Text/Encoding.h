#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <SKSystem/Array.h>
#include <SKSystem/Text/String.h>

namespace SK
{
	namespace Encoding
	{
		//---- Types ----//
		//===============//

		class CaseFolding
		{
		public:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			static CodePoint Fold(CodePoint in);

			//-- Variables --//
			//===============//
		};

		class UTF8
		{
		public:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			static Array<uint8_t> GetBytes(const String& string, bool nullTerminated = false);
			static String         FromBytes(const uint8_t* data, std::size_t length);
			static String         FromBytes(const uint8_t* data);
			static String         FromBytes(const Array<uint8_t> data);
			static CodePoint      GetNextCodePoint(const uint8_t* data, std::size_t& startPosition, std::size_t bufferLength = -1);

			//-- Variables --//
			//===============//
		};

		class UTF16
		{
		public:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			static Array<uint8_t> GetBytes(const String& string, bool nullTerminated = false);
			static String         FromBytes(const uint8_t* data, std::size_t length);
			static String         FromBytes(const uint8_t* data);
			static String         FromBytes(const Array<uint8_t> data);
			static CodePoint      GetNextCodePoint(const uint8_t* data, std::size_t& startPosition, std::size_t bufferLength = -1);

			//-- Variables --//
			//===============//
		};

		class UTF32
		{
		public:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			static Array<uint8_t> GetBytes(const String& string, bool nullTerminated = false);
			static String         FromBytes(const uint8_t* data, std::size_t length);
			static String         FromBytes(const uint8_t* data);
			static String         FromBytes(const Array<uint8_t> data);
			static CodePoint      GetNextCodePoint(const uint8_t* data, std::size_t& startPosition, std::size_t bufferLength = -1);

			//-- Variables --//
			//===============//
		};

		//--- Methods ---//
		//===============//
	}
}