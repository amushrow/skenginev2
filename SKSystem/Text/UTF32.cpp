//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <SKSystem/Utils.h>
#include <SKSystem/IO/MemoryStream.h>
#include "Encoding.h"

using namespace SK;
using namespace SK::Encoding;

//-- Declarations --//
//==================//

namespace
{
	void WriteCodePoint(CodePoint codePoint, IO::Stream& stream);
}

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

Array<uint8_t> UTF32::GetBytes(const String& string, bool nullTerminated)
{
	const uint8_t* StringData = string.Buffer();

	std::size_t BufferSize = string.Length() * 4;
	if (nullTerminated) BufferSize += 4;
	uint8_t* Buffer = new uint8_t[BufferSize];
	IO::MemoryStream UTF32Buffer(Buffer, BufferSize, false);
	std::size_t Position = 0;
	CodePoint CP = 0;
	while ((CP = UTF8::GetNextCodePoint(StringData, Position, string.Size())))
	{
		UTF32Buffer.Write(CP);
	}

	if (nullTerminated)
	{
		UTF32Buffer.Write(CodePoint(0));
	}

	return Array<uint8_t>(Buffer, BufferSize);
}

String UTF32::FromBytes(const uint8_t* data, std::size_t length)
{
	IO::MemoryStream UTF32Buffer(data, length);
	CodePoint CP = 0;
	std::size_t StringLength = 0;
	std::size_t BufferSize = 0;

	while (UTF32Buffer.Read(CP))
	{
		StringLength++;

		if (CP < 0x80)         { BufferSize++; }
		else if (CP < 0x800)   { BufferSize += 2; }
		else if (CP < 0x10000) { BufferSize += 3; }
		else                   { BufferSize += 4; }
	}

	uint8_t* Buffer = new uint8_t[BufferSize];
	IO::MemoryStream UTF8Buffer(Buffer, BufferSize, false, true);
	UTF32Buffer.Position(0);
	while (UTF32Buffer.Read(CP))
	{
		WriteCodePoint(CP, UTF8Buffer);
	}

	return String(Buffer, BufferSize, StringLength);
}

String UTF32::FromBytes(const uint8_t* data)
{
	std::size_t StringLength = 0;
	std::size_t Position = 0;
	CodePoint CP = 0;
	std::size_t BufferSize = 0;
	while ((CP = GetNextCodePoint(data, Position)))
	{
		StringLength++;

		if (CP < 0x80)         { BufferSize++; }
		else if (CP < 0x800)   { BufferSize += 2; }
		else if (CP < 0x10000) { BufferSize += 3; }
		else                   { BufferSize += 4; }
	}

	uint8_t* Buffer = new uint8_t[BufferSize];
	IO::MemoryStream UTF8Buffer(Buffer, BufferSize, false);
	Position = 0;
	while ((CP = GetNextCodePoint(data, Position)))
	{
		WriteCodePoint(CP, UTF8Buffer);
	}

	return String(Buffer, BufferSize, StringLength);
}

String UTF32::FromBytes(const Array<uint8_t> data)
{
	return FromBytes(data.Get(), data.Length());
}

uint32_t UTF32::GetNextCodePoint(const uint8_t* data, std::size_t& startPosition, std::size_t bufferLength)
{
	if (bufferLength != -1 && startPosition >= bufferLength)
	{
		return 0;
	}

	CodePoint CP = *reinterpret_cast<const uint32_t*>(&data[startPosition]);
	if (CP != 0) startPosition += 4;
	return CP;
}

//-- Private Methods --//
//=====================//

namespace
{
	void WriteCodePoint(CodePoint codePoint, IO::Stream& stream)
	{
		if (codePoint < 0x80)
		{
			stream.Write(uint8_t(codePoint));
		}
		else if (codePoint < 0x800)
		{
			uint8_t First = 0xC0 | (codePoint >> 6);
			uint8_t Second = 0x80 | (codePoint & 0x3F);
			stream.Write(First);
			stream.Write(Second);
		}
		else if (codePoint < 0x10000)
		{
			uint8_t First = 0xE0 | (codePoint >> 12);
			uint8_t Second = 0x80 | ((codePoint >> 6) & 0x3F);
			uint8_t Third = 0x80 | (codePoint & 0x3F);
			stream.Write(First);
			stream.Write(Second);
			stream.Write(Third);
		}
		else
		{
			uint8_t First = 0xF0 | (codePoint >> 18);
			uint8_t Second = 0x80 | ((codePoint >> 12) & 0x3F);
			uint8_t Third = 0x80 | ((codePoint >> 6) & 0x3F);
			uint8_t Fourth = 0x80 | (codePoint & 0x3F);
			stream.Write(First);
			stream.Write(Second);
			stream.Write(Third);
			stream.Write(Fourth);
		}
	}
}