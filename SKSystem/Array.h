#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//
// Fixed size array class. Implicity casts to T* and deletes it's memory when no more references are held.

//--- Include ---//
//===============//
#include <memory>
#include <cstddef>

namespace SK
{
	//---- Types ----//
	//===============//

	template< typename T >
	struct ArrayDeleter
	{
		//---- Types ----//
		//===============//

		//--- Methods ---//
		//===============//

		void operator ()(T const* data)
		{
			delete[] data;
		}

		//-- Variables --//
		//===============//
	};

	template<typename T>
	class Array
	{
	public:
		//---- Types ----//
		//===============//

		//--- Methods ---//
		//===============//

		Array() : m_Array(nullptr), m_Length(0) {}
		Array(std::size_t size) : m_Array(new T[size], ArrayDeleter<T>()), m_Length(size) {}
		Array(T* data, std::size_t size) : m_Array(data, ArrayDeleter<T>()), m_Length(size) {}

		operator T*()              { return m_Array.get(); }
		operator const T*()  const { return m_Array.get(); }
		operator bool()      const { return m_Array; }
		T*          Get()          { return m_Array.get(); }
		const T*    Get()    const { return m_Array.get(); }
		std::size_t Length() const { return m_Length; }
		void        Delete()       { m_Array.reset(); }

		//-- Variables --//
		//===============//

	protected:
		//---- Types ----//
		//===============//

		//--- Methods ---//
		//===============//

		//-- Variables --//
		//===============//

		std::shared_ptr<T> m_Array;
		std::size_t        m_Length;
	};

	//--- Methods ---//
	//===============//
}
