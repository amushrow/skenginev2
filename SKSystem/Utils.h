#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

// Random helper functions that don't really deserve their own file

//--- Include ---//
//===============//
#include <stdexcept>
#include <memory>
#include <SKSystem/Text/String.h>

namespace SK
{
	//---- Types ----//
	//===============//

	class TimeoutException : public std::runtime_error
	{
	public:
		explicit TimeoutException(const String& msg) : std::runtime_error(std::string(msg)) {}
	};

	class NotEnoughMemoryException : public std::runtime_error
	{
	public:
		explicit NotEnoughMemoryException(const String& msg) : std::runtime_error(std::string(msg)) {}
	};

	class ArgumentException : public std::runtime_error
	{
	public:
		explicit ArgumentException(const String& msg) : std::runtime_error(std::string(msg)) {}
	};

	class IOException : public std::runtime_error
	{
	public:
		explicit IOException(const String& msg) : std::runtime_error(std::string(msg)) {}
	};

	class FileNotFoundException : public IOException
	{
	public:
		explicit FileNotFoundException(const String& msg) : IOException(std::string(msg)) {}
	};

	//--- Methods ---//
	//===============//

	template<typename T>
	int HighestOneBitPosition(T a)
	{
		int bits = 0;
		while (a != 0)
		{
			++bits;
			a >>= 1;
		};
		return bits;
	}

	template<typename T>
	bool MultiplicationIsSafe(T a, T b)
	{
		return (HighestOneBitPosition(a) + HighestOneBitPosition(b)) <= 32;
	}

	template<typename T>
	bool AdditionIsSafe(T a, T b)
	{
		return HighestOneBitPosition(a)<32 && HighestOneBitPosition(b)<32;
	}

	inline bool CheckFlags(int value, int flags)
	{
		return (value & flags) == flags;
	}

	inline size_t ByteCount(size_t numBits)
	{
		return (numBits + 7) / 8;
	}

	inline bool BitIsSet(uint8_t* value, size_t bit)
	{
		return (value[bit / 8] & (1 << (bit % 8))) != 0;
	}

	inline void SetBit(uint8_t* value, size_t bit)
	{
		value[bit / 8] |= (1 << (bit % 8));
	}

	inline void ClearBit(uint8_t* value, size_t bit)
	{
		value[bit / 8] &= ~(1 << (bit % 8));
	}
}