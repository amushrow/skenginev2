#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <functional>
#include <cstdint>
#include <cmath>

namespace SK
{
	namespace Time
	{
		//---- Types ----//
		//===============//

		class Duration
		{
		public:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			Duration() : m_Count(0) {}
			explicit Duration(int64_t count) : m_Count(count) {}
			Duration(int64_t days, int64_t hours, int64_t minutes, int64_t seconds, int64_t milliseconds, int64_t microseconds);

			static Duration                        Max()                { return Duration(INT64_MAX); }

			template<typename type = int64_t> type Days()         const { return static_cast<type>(m_Count) / static_cast<type>(86400000000); }
			template<typename type = int64_t> type Hours()        const { return static_cast<type>(m_Count) / static_cast<type>(3600000000); }
			template<typename type = int64_t> type Minutes()      const { return static_cast<type>(m_Count) / static_cast<type>(60000000); }
			template<typename type = int64_t> type Seconds()      const { return static_cast<type>(m_Count) / static_cast<type>(1000000); }
			template<typename type = int64_t> type Milliseconds() const { return static_cast<type>(m_Count) / static_cast<type>(1000); }
			int64_t                                Microseconds() const { return m_Count; }

			const Duration  operator+ (const Duration& other) const { return Duration(m_Count + other.m_Count); }
			const Duration& operator+=(const Duration& other)       { m_Count += other.m_Count; return *this; }
			const Duration  operator- (const Duration& other) const { return Duration(m_Count - other.m_Count); }
			const Duration& operator-=(const Duration& other)       { m_Count -= other.m_Count; return (*this); }
			const Duration& operator*=(const   double& other)       { m_Count = static_cast<int64_t>(m_Count * other); return *this; }
			const Duration  operator/ (const   double& other) const { return Duration(static_cast<int64_t>(m_Count / other)); }
			const Duration& operator/=(const   double& other)       { m_Count = static_cast<int64_t>(m_Count / other); return *this; }
			bool            operator==(const Duration& other) const { return m_Count == other.m_Count; }
			bool            operator!=(const Duration& other) const { return m_Count != other.m_Count; }
			bool            operator> (const Duration& other) const { return m_Count >  other.m_Count; }
			bool            operator< (const Duration& other) const { return m_Count <  other.m_Count; }
			bool            operator>=(const Duration& other) const { return m_Count >= other.m_Count; }
			bool            operator<=(const Duration& other) const { return m_Count <= other.m_Count; }
			const Duration  operator- () const                      { return Duration(-m_Count); }

			//-- Variables --//
			//===============//

		private:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			//-- Variables --//
			//===============//

			int64_t m_Count;
		};

		typedef Duration TimePoint;

		class Timeline;
		struct TimeEventArgs
		{
			const Time::Timeline* Timeline;
			Time::Duration        Delta;
			Time::TimePoint       Now;
		};

		typedef std::function<void(const TimeEventArgs&)> TimeEvent;
		
		//--- Methods ---//
		//===============//

		using std::abs;
		inline Duration abs(const Duration& duration) { return duration.Microseconds() > 0 ? duration : -duration; }

		Duration        Days(int64_t days);
		Duration        Hours(int64_t hours);
		Duration        Minutes(int64_t minutes);
		Duration        Seconds(int64_t seconds);
		Duration        Milliseconds(int64_t milliseconds);
		Duration        Microseconds(int64_t microseconds);

		const TimePoint Now();
		const Duration  Zero();
	}
}

//--- Methods ---//
//===============//

inline const SK::Time::Duration operator*(double scale, const SK::Time::Duration& duration) { return SK::Time::Duration(static_cast<int64_t>(duration.Microseconds() * scale)); }
inline const SK::Time::Duration operator*(const SK::Time::Duration& duration, double scale) { return SK::Time::Duration(static_cast<int64_t>(duration.Microseconds() * scale)); }
