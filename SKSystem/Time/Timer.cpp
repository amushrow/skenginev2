//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <SKSystem/Debug.h>
#include "Timer.h"

using namespace SK;
using namespace SK::Time;

//-- Declarations --//
//==================//

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

Timer::Timer(Duration interval, TimeEvent callback)
: m_Interval(interval)
, m_IntermittentFunction(callback)
{
}

Timer::Timer(Timer&& other)
{
	*this = std::move(other);
}

Timer& Timer::operator = (Timer&& other)
{
	m_Interval = other.m_Interval;
	m_IntermittentFunction = std::move(other.m_IntermittentFunction);
	other.m_Interval = SK::Time::Zero();
	return *this;
}

void Timer::Update(const TimeEventArgs& e)
{
	Debug::Assert(static_cast<bool>(m_IntermittentFunction), "Timer::Update - Invalid timer");

	if (e.Delta != Time::Zero())
	{
		m_Elapsed += e.Delta;
		if (m_Elapsed >= m_Interval)
		{
			TimeEventArgs args;
			args.Timeline = e.Timeline;
			args.Now = e.Now;
			args.Delta = m_Interval;
			m_IntermittentFunction(args);

			do
			{
				m_Elapsed -= m_Interval;
			} while (m_Elapsed > m_Interval);
		}

		if (m_Elapsed <= Time::Zero())
		{
			TimeEventArgs args;
			args.Timeline = e.Timeline;
			args.Now = e.Now;
			args.Delta = -m_Interval;
			m_IntermittentFunction(args);

			do
			{
				m_Elapsed += m_Interval;
			} while (m_Elapsed < Time::Zero());
		}
	}
}

AutoUpdateTimer::AutoUpdateTimer(Duration interval, TimeEvent callback, ClockPtr clock, bool start)
: m_Interval(interval)
, m_Stopped(true)
, m_Clock(clock)
, m_IntermittentFunction(callback)
{
	if (!m_Clock)
	{
		m_Clock = std::make_shared<HighPerformanceClock>();
	}

	if (start)
	{
		Start();
	}
}

AutoUpdateTimer::AutoUpdateTimer(AutoUpdateTimer&& other)
{
	*this = std::move(other);
}

AutoUpdateTimer& AutoUpdateTimer::operator = (AutoUpdateTimer&& other)
{
	if (m_UpdateThread.IsValid())
	{
		m_Stopped = true;
		m_UpdateThread.Join();
	}

	m_Interval = other.m_Interval;
	m_IntermittentFunction = std::move(other.m_IntermittentFunction);
	m_Clock = std::move(other.m_Clock);
	other.m_Interval = SK::Time::Zero();

	if (!other.m_Stopped)
	{
		if (other.m_UpdateThread.IsValid())
		{
			other.m_Stopped = true;
			other.m_UpdateThread.Join();
		}

		Start();
	}

	return *this;
}

AutoUpdateTimer::~AutoUpdateTimer()
{
	if (m_UpdateThread.IsValid())
	{
		m_Stopped = true;
		m_UpdateThread.Join();
	}
}

void AutoUpdateTimer::Start()
{
	Debug::Assert(static_cast<bool>(m_IntermittentFunction), "AutoUpdateTimer::Update - Invalid timer");
	if (m_Stopped)
	{
		m_Stopped = false;
		m_UpdateTime = m_Clock->Time();
		m_UpdateThread = Threading::Thread(&AutoUpdateTimer::UpdateTime, this);
	}
}

void AutoUpdateTimer::Stop()
{
	m_Stopped = true;
}

//-- Private Methods --//
//=====================//

void AutoUpdateTimer::UpdateTime()
{
	while (!m_Stopped)
	{
		// Update
		TimePoint Now = m_Clock->Time();
		Duration Elapsed = Now - m_UpdateTime;
		if (Elapsed > m_Interval)
		{
			TimeEventArgs args;
			args.Timeline = nullptr;
			args.Now = Now;
			args.Delta = m_Interval;
			m_IntermittentFunction(args);

			m_UpdateTime = Now - (Elapsed - m_Interval);
		}

		// Sleep until we're nearer the next tick
		SK::Time::Duration SleepTime = (m_Interval - Elapsed) - SK::Time::Milliseconds(5);
		SleepTime = std::min(Time::Milliseconds(30), std::max(SK::Time::Zero(), SleepTime));
		Threading::Sleep(SleepTime);
	}
}