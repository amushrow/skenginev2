#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <memory>
#include <SKSystem/Time/Time.h>

namespace SK
{
	namespace Time
	{
		//---- Types ----//
		//===============//
		
		class Clock
		{
		public:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			virtual ~Clock() {};
			virtual TimePoint Time() = 0;

			//-- Variables --//
			//===============//
		};

		class HighPerformanceClock : public Clock
		{
		public:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			TimePoint Time() override;

			//-- Variables --//
			//===============//
		};

		typedef std::shared_ptr<Clock> ClockPtr;

		//--- Methods ---//
		//===============//
	}
}