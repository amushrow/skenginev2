//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <ctime>
#include <SKSystem/Time/Clocks.h>

using namespace SK;

//-- Declarations --//
//==================//

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

Time::TimePoint Time::HighPerformanceClock::Time()
{
	timespec Time = { 0 };
	clock_gettime(CLOCK_REALTIME, &Time);

	return TimePoint((Time.tv_sec * 1000000) + (Time.tv_nsec / 1000));
}

//-- Private Methods --//
//=====================//
