//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include "Timeline.h"

using namespace SK::Time;

//-- Declarations --//
//==================//

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

Timeline::Timeline(ClockPtr clock, Duration fixedUpdateInterval)
: m_Clock(clock)
, m_UpdateInterval(fixedUpdateInterval)
, m_Elapsed(0)
, m_Now(0)
, m_Scale(1.0f)
{
	m_LastUpdate = m_Clock->Time();
}

void Timeline::Update()
{
	if (IsPaused())
	{
		return;
	}

	TimePoint RealtimeNow = m_Clock->Time();
	Duration Delta = (RealtimeNow - m_LastUpdate) * m_Scale;

	if (m_UpdateInterval > Time::Zero())
	{
		if (abs(Delta) > m_UpdateInterval)
		{
			// Check if we are going forwards or backwards in time
			Duration FixedDelta = m_Scale > 0 ? m_UpdateInterval : -m_UpdateInterval;

			// Update the time on this timeline
			m_Now += FixedDelta;
			m_LastUpdate = RealtimeNow;

			OnUpdate(FixedDelta, m_Now);
		}
	}
	else
	{
		// Update the time on this timeline
		m_Now += Delta;
		m_LastUpdate = RealtimeNow;

		OnUpdate(Delta, m_Now);
	}
}

//-- Private Methods --//
//=====================//

void Timeline::OnUpdate(Duration delta, TimePoint now)
{
	TimeEventArgs args;
	args.Timeline = this;
	args.Delta = delta;
	args.Now = now;

	for (auto& kvp : m_Objects)
	{
		kvp.second(args);
	}
}