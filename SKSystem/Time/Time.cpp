//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include "Time.h"
#include "Clocks.h"

using namespace SK;

//-- Declarations --//
//==================//

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

namespace
{
	Time::HighPerformanceClock _HighPerformanceClock;
}

//-- Public Methods --//
//====================//

const SK::Time::TimePoint SK::Time::Now()
{
	return _HighPerformanceClock.Time();
}

const SK::Time::Duration SK::Time::Zero()
{
	static Duration _Zero(0);
	return _Zero;
}

Time::Duration Time::Days(int64_t days)
{
	return Duration(days * 86400000000);
}

Time::Duration Time::Hours(int64_t hours)
{
	return Duration(hours * 3600000000);
}

Time::Duration Time::Minutes(int64_t minutes)
{
	return Duration(minutes * 60000000);
}

Time::Duration Time::Seconds(int64_t seconds)
{
	return Duration(seconds * 1000000);
}

Time::Duration Time::Milliseconds(int64_t milliseconds)
{
	return Duration(milliseconds * 1000);
}

Time::Duration Time::Microseconds(int64_t microseconds)
{
	return Duration(microseconds);
}

Time::Duration::Duration(int64_t days, int64_t hours, int64_t minutes, int64_t seconds, int64_t milliseconds, int64_t microseconds)
{
	m_Count = (days * 86400000000) + (hours * 3600000000) + (minutes * 60000000) + (seconds * 1000000) + (milliseconds * 1000) + microseconds;
}

//-- Private Methods --//
//=====================//
