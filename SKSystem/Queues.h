#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

// Thread safe queues and the like

//--- Include ---//
//===============//
#include <atomic>

namespace SK
{
	//--- Types ---//
	//===============//

	template<class T>
	class RingBuffer
	{
	public:
		//---- Types ----//
		//===============//

		//--- Methods ---//
		//===============//

		RingBuffer(unsigned int maxSize) : m_MaxSize(maxSize)
		{
			m_StartIndex = 0;
			m_EndIndex = 0;
			m_List = new T[m_MaxSize];
		}

		~RingBuffer()
		{
			delete[] m_List;
		}

		// Only one thread may add items!
		bool Enqueue(const T& item)
		{
			bool Result = false;
			if ((m_EndIndex + 1) % m_MaxSize != m_StartIndex)
			{
				m_List[m_EndIndex] = item;
				m_EndIndex = (m_EndIndex + 1) % m_MaxSize;
				Result = true;
			}

			return Result;
		}

		// Multiple threads may remove items
		bool Dequeue(T& dest)
		{
			do
			{
				// Check current index
				unsigned int Old = m_StartIndex;
				if (Old == m_EndIndex)
					break;

				// Calc the new index
				unsigned int NewStart = (Old + 1) % m_MaxSize;
				
				// Copy data
				dest = m_List[Old];

				// Advance
				if (m_StartIndex.compare_exchange_weak(Old, NewStart))
				{
					return true;
				}

			} while (true);

			return false;
		}

		bool IsEmpty()
		{
			return m_StartIndex == m_EndIndex;
		}

		bool IsFull()
		{
			unsigned int NextIndex = (m_EndIndex + 1) % m_MaxSize;
			return NextIndex == m_StartIndex;
		}

		//-- Variables --//
		//===============//

	private:
		//---- Types ----//
		//===============//

		//--- Methods ---//
		//===============//

		//-- Variables --//
		//===============//

		T*                        m_List;
		std::atomic<unsigned int> m_StartIndex;
		unsigned int              m_EndIndex;
		const unsigned int        m_MaxSize;
	};

	template<class T>
	class Queue
	{
	public:
		//---- Types ----//
		//===============//

		//--- Methods ---//
		//===============//

		Queue()
		{
			m_First = m_Last = m_Divider = new Node(T());
		}

		~Queue()
		{
			while (m_First != nullptr)
			{
				Node* Temp = m_First;
				m_First = Temp->Next;
				delete Temp;
			}
		}

		// Only one thread may add items!
		void Enqueue(const T& item)
		{
			(*m_Last).Next = new Node(item);
			m_Last = (*m_Last).Next;

			while (m_First != m_Divider)
			{
				Node* Temp = m_First;
				m_First = Temp->Next;
				delete Temp;
			}
		}

		// Only one thread may remove items!
		bool Dequeue(T& dest)
		{
			if (m_Divider.load() != m_Last.load())
			{
				dest = (*m_Divider).Next->Value;
				m_Divider = (*m_Divider).Next;
				return true;
			}

			return false;
		}

		bool IsEmpty()
		{
			return m_Divider.load() == m_Last.load();
		}

		//-- Variables --//
		//===============//

	private:
		//---- Types ----//
		//===============//

		struct Node
		{
			Node(T val) : Value(val), Next(nullptr) {}
			T     Value;
			Node* Next;
		};

		//--- Methods ---//
		//===============//

		//-- Variables --//
		//===============//

		Node*              m_First;
		std::atomic<Node*> m_Last;
		std::atomic<Node*> m_Divider;	
	};
}