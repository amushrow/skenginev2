#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2016 Anthony Mushrow                           //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <SKSystem/Math/Matrix.h>
#include <SKSystem/Flags.h>

namespace SK
{
	//---- Types ----//
	//===============//

	enum class ObjectDirty
	{
		Transform = 0x01,
	};

	template<class T>
	class Object
	{
	public:
		//--- Methods ---//
		//===============//

		Object(GenericFlags matrixDirtyFlags);
		virtual ~Object() {}

		T*                  Child() const  { return m_Child; }
		T*                  Next() const   { return m_Next; }
		T*                  Parent() const { return m_Parent; }

		void                Attach(T* parent);
		void                Detach();

		const Math::Matrix& WorldTransformDirect() const { return m_WorldTransform; }
		const Math::Matrix& WorldTransform();
		void                WorldTransform(const Math::Matrix& matrix);
		const Math::Matrix& LocalTransform() const                       { return m_LocalTransform; }
		void                LocalTransform(const Math::Matrix& matrix);
		
		GenericFlags&       Flags()                                 { return m_DirtyFlags; }

	private:

		//---- Types ----//
		//===============//

		//--- Methods ---//
		//===============//

		void UpdateWorldTransform();
		void MarkDirty();

		virtual void Detached() = 0;
		virtual void Attached(T* parent) = 0;

		//-- Variables --//
		//===============//

		Math::Matrix m_LocalTransform;
		Math::Matrix m_WorldTransform;

		T*           m_Parent;
		T*           m_Child;
		T*           m_Next;
		GenericFlags m_TransformDirtyFlags;

	protected:
		//-- Variables --//
		//===============//
		
		GenericFlags m_DirtyFlags;
	};

	//--- Methods ---//
	//===============//

	template<class T>
	Object<T>::Object(GenericFlags transformDirtyFlags = ObjectDirty::Transform)
		: m_LocalTransform(Math::Matrix::Identity())
		, m_WorldTransform(Math::Matrix::Identity())
		, m_Parent(nullptr)
		, m_Child(nullptr)
		, m_Next(nullptr)
		, m_TransformDirtyFlags(transformDirtyFlags)
		, m_DirtyFlags(0)
	{

	}

	template<class T>
	void Object<T>::Attach(T* parent)
	{
		if (m_Parent != parent)
		{
			if (m_Parent != nullptr)
				Detach();

			m_Next = parent->m_Child;
			m_Parent = parent;
			parent->m_Child = static_cast<T*>(this);

			m_LocalTransform = WorldTransform() * parent->WorldTransform().Inverse();
			MarkDirty();
		}
	}

	template<class T>
	void Object<T>::Detach()
	{
		if (m_Parent != nullptr)
		{
			if (m_DirtyFlags.HasFlags(ObjectDirty::Transform))
				UpdateWorldTransform();

			if (m_Parent->m_Child == this)
			{
				m_Parent->m_Child = m_Next;
			}
			else
			{
				Object* Prev = m_Parent->m_Child;
				Object* child = m_Parent->m_Child;
				while (child = child->m_Next)
				{
					if (child == this)
					{
						Prev->m_Next = m_Next;
						break;
					}
					Prev = child;
				}
			}

			m_Parent = nullptr;
		}
	}

	template<class T>
	const Math::Matrix& Object<T>::WorldTransform()
	{
		if (m_DirtyFlags.HasFlags(ObjectDirty::Transform))
			UpdateWorldTransform();
		return m_WorldTransform;
	}

	template<class T>
	void Object<T>::WorldTransform(const Math::Matrix& matrix)
	{
		if (m_Parent)
			m_LocalTransform = matrix * m_Parent->WorldTransform().Inverse();
		else
			m_LocalTransform = matrix;
		MarkDirty();
	}

	template<class T>
	void Object<T>::LocalTransform(const Math::Matrix& matrix)
	{
		m_LocalTransform = matrix;
		MarkDirty();
	}

	template<class T>
	void Object<T>::UpdateWorldTransform()
	{
		if (m_DirtyFlags.HasFlags(ObjectDirty::Transform))
		{
			if (m_Parent)
				m_WorldTransform = m_LocalTransform * m_Parent->WorldTransform();
			else
				m_WorldTransform = m_LocalTransform;

			m_DirtyFlags &= ~static_cast<int>(ObjectDirty::Transform);
		}
	}

	template<class T>
	void Object<T>::MarkDirty()
	{
		m_DirtyFlags |= m_TransformDirtyFlags;
		for (Object* child = m_Child; child; child = child->m_Next)
		{
			if (!child->m_DirtyFlags.HasFlags(ObjectDirty::Transform))
				child->MarkDirty();
		}
	}
}