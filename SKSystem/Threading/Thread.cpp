//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright © 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <thread>
#include <mutex>
#include <unordered_map>
#include <queue>
#include <SKSystem/Utils.h>
#include <SKSystem/Threading/ThreadProc.h>
#include <SKSystem/Threading/Thread.h>

using namespace SK::Threading;

//-- Declarations --//
//==================//

namespace std
{
	template <>
	struct hash<SK::Threading::ThreadId>
	{
		size_t operator()(const SK::Threading::ThreadId& t) const
		{
			return t.hash();
		}
	};
}

//-- Public Variables --//
//======================//

namespace
{
	typedef std::queue<std::shared_ptr<ThreadProc>> ThreadProcQueue;

	std::unordered_map<ThreadId, ThreadProcQueue> ThreadTaskQueues;
	std::mutex                                    ThreadHashLock;
}

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

namespace
{
	const std::shared_ptr<const ThreadProc> QueueThreadProc(const ThreadId& thread, std::function<void()> func)
	{
		ThreadHashLock.lock();

		auto Element = ThreadTaskQueues.emplace(thread, ThreadProcQueue()).first;
		Element->second.emplace(std::make_shared<ThreadProc>(func));
		std::shared_ptr<const ThreadProc> NewThreadProc = Element->second.back();
		ThreadHashLock.unlock();

		return NewThreadProc;
	}

	void ExecuteThreadProcs()
	{
		ThreadHashLock.lock();

		auto Element = ThreadTaskQueues.find(SK::Threading::GetThreadId());
		if (Element != ThreadTaskQueues.end())
		{
			// We may have some functions to call
			while(Element->second.size() > 0)
			{
				std::shared_ptr<ThreadProc> Proc = Element->second.front();
				Element->second.pop();
				Proc->ExecuteTask();
			}
		}

		ThreadHashLock.unlock();
	}
}

void SK::Threading::Sleep(SK::Time::Duration duration)
{
	std::this_thread::sleep_for(std::chrono::microseconds(duration.Microseconds()));
}

void SK::Threading::Yield()
{
	std::this_thread::yield();
}

ThreadId SK::Threading::GetThreadId()
{
	return ThreadId(std::this_thread::get_id());
}

void SK::Threading::Dispatch()
{
	ExecuteThreadProcs();
}

void SK::Threading::Invoke(const ThreadId& thread, std::function<void()> function)
{
	Invoke(thread, Time::Duration::Max(), function);
}

void SK::Threading::Invoke(const ThreadId& thread, Time::Duration timeout, std::function<void()> function)
{
	// Invoking on the same thread? Seems reduntant to me.
	if (thread == GetThreadId())
	{
		// Just call the function
		function();
	}
	else
	{
		std::shared_ptr<const ThreadProc> Proc = QueueThreadProc(thread, function);

		if (!Proc->Wait(timeout))
		{
			// We've time out. We can't return the result so we throw an exception.
			throw TimeoutException("Thread did not respond in a timely fashion");
		}
	}
}


size_t ThreadId::hash() const
{
	std::hash<std::thread::id> Hash;
	return Hash(m_InternalId);
}

Thread::Thread(Thread&& other) throw() : m_Thread(std::move(m_Thread))
{
	
}

Thread& SK::Threading::Thread::operator=(Thread&& other) throw()
{
	m_Thread = std::move(other.m_Thread);
	return *this;
}

void Thread::Join()
{
	m_Thread.join();
}

void Thread::Detach()
{
	m_Thread.detach();
}

ThreadId Thread::GetThreadId()
{
	return ThreadId(m_Thread.get_id());
}

bool Thread::IsValid()
{
	return m_Thread.joinable();
}

void* Thread::NativeHandle()
{
	return reinterpret_cast<void*>(m_Thread.native_handle());
}

//-- Private Methods --//
//=====================//