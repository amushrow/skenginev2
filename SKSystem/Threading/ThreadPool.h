#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <vector>
#include <queue>
#include <thread>
#include <future>
#include <mutex>
#include <condition_variable>
#include <SKSystem/Threading/Thread.h>
#include <SKSystem/Debug.h>

namespace SK
{
	namespace Threading
	{
		//---- Types ----//
		//===============//

		class ThreadPool
		{
		public:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			ThreadPool(unsigned int size);
			~ThreadPool();

			template<class Fn, class... Args>
			auto   AddTask(Fn&& function, Args&&... args)->std::future<typename std::result_of<Fn(Args...)>::type>;

			void BeginAddTasks();
			template<class Fn, class... Args>
			auto   AddTask_I(Fn&& function, Args&&... args)->std::future<typename std::result_of<Fn(Args...)>::type>;
			void EndAddTasks();

			void   Stop(bool finishTasks = true);
			size_t GetThreadCount() const { return m_Workers.size(); }
			void   RunTasksInCurrentContext(); // Process queued tasks on the local thread as well

			//-- Variables --//
			//===============//

		private:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			void RunTasks();

			//-- Variables --//
			//===============//

			std::vector<Thread>               m_Workers;
			std::queue<std::function<void()>> m_Tasks;
			std::mutex                        m_QueueLock;
			std::condition_variable           m_QueueConditional;
			bool                              m_Exit;
			bool                              m_CompleteTasks;
		};

		template<class T, int size>
		class FutureGroup
		{
		public:
			FutureGroup() : m_NumFutures(0) {}

			void Add(std::future<T>& f)
			{
				SK::Debug::Assert(m_NumFutures < size);
				m_Futures[m_NumFutures++] = std::move(f);
			}

			void Wait()
			{
				for (uint32_t i = 0; i < m_NumFutures; ++i)
					m_Futures[i].wait();
			}

			void Clear()
			{
				m_NumFutures = 0;
			}

		private:
			std::future<T> m_Futures[size];
			uint32_t       m_NumFutures;
		};

		//--- Methods ---//
		//===============//
	}
}

//--- Methods ---//
//===============//

template<class Fn, class... Args>
auto SK::Threading::ThreadPool::AddTask(Fn&& function, Args&&... args) -> std::future<typename std::result_of<Fn(Args...)>::type>
{
	typedef typename std::result_of<Fn(Args...)>::type ReturnType;
	auto task = std::make_shared<std::packaged_task<ReturnType()>>(std::bind(std::forward<Fn>(function), std::forward<Args>(args)...));

	std::future<ReturnType> Result = task->get_future();

	m_QueueLock.lock();
	m_Tasks.push([task](){ (*task)(); });
	m_QueueLock.unlock();

	m_QueueConditional.notify_one();

	return Result;
}

template<class Fn, class... Args>
auto SK::Threading::ThreadPool::AddTask_I(Fn&& function, Args&&... args) -> std::future<typename std::result_of<Fn(Args...)>::type>
{
	typedef typename std::result_of<Fn(Args...)>::type ReturnType;
	auto task = std::make_shared<std::packaged_task<ReturnType()>>(std::bind(std::forward<Fn>(function), std::forward<Args>(args)...));

	std::future<ReturnType> Result = task->get_future();
	m_Tasks.push([task]() { (*task)(); });

	return Result;
}

