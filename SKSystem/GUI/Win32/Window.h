#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <unordered_map>
#include <SKSystem/GUI/Window.h>
#include <SKSystem/Win32/Win32.h>

namespace SK
{
	namespace GUI
	{
		//---- Types ----//
		//===============//

		struct Window_EventData
		{
			WPARAM WParam;
			LPARAM LParam;
		};

		struct Window::PlatformData
		{
			HWND Wnd;
			HDC  DC;
			std::unordered_map<UINT, Window_EventData> Events;
		};

		//--- Methods ---//
		//===============//
	}
}