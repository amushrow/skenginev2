//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <SKSystem/Debug.h>
#include <SKSystem/Utils.h>
#include <SKSystem/IO/MemoryStream.h>
#include <SKGraphics/Image/BitmapFileFormat.h>
#include <SKSystem/GUI/Win32/Window.h>

using namespace SK;
using namespace SK::GUI;
using namespace SK::Math;

//-- Declarations --//
//==================//

#define WIDTHBYTES(bits) ((((bits) + 31)>>5)<<2)
namespace
{
	LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	HICON GetHIcon(const Graphics::Image& image);
}

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//


Window::~Window()
{
	delete m_Platform;
}

void Window::SetTitle(const String& title)
{
	SetWindowText(m_Platform->Wnd, Win32::GetWideString(title));
}

void Window::SetIcon(const SK::Graphics::MipmapImage& icons)
{
	// We need to set the window icon, and the larger taskbar icon
	// I'm going to assume all these icons are square
	int SmallIconSize = GetSystemMetrics(SM_CXSMICON);
	int IconSize = GetSystemMetrics(SM_CXICON);

	int SmallIndex = 0;
	int SmallDifference = 0x7FFFFFFF;
	int LargeIndex = 0;
	int LargeDifference = 0x7FFFFFFF;

	// Find closest matching icon sizes
	for (int i = 0; i < icons.NbLevels(); ++i)
	{
		const auto& Image = icons.Level(i);
		if (Image.Size().x >= SmallIconSize && (Image.Size().x - SmallIconSize) <  SmallDifference)
		{
			SmallIndex = i;
			SmallDifference = Image.Size().x - SmallIconSize;
		}
		if (Image.Size().x >= IconSize && (Image.Size().x - IconSize) <  LargeDifference)
		{
			LargeIndex = i;
			LargeDifference = Image.Size().x - IconSize;
		}
	}

	HICON BigIcon = GetHIcon(icons.Level(LargeIndex));
	HICON SmallIcon = GetHIcon(icons.Level(SmallIndex));

	HICON OldIcon = reinterpret_cast<HICON>(SetClassLongPtr(m_Platform->Wnd, GCLP_HICON, reinterpret_cast<LONG_PTR>(BigIcon)));
	if (OldIcon != nullptr)
	{
		DestroyIcon(OldIcon);
	}
	OldIcon = reinterpret_cast<HICON>(SetClassLongPtr(m_Platform->Wnd, GCLP_HICONSM, reinterpret_cast<LONG_PTR>(SmallIcon)));
	if (OldIcon != nullptr)
	{
		DestroyIcon(OldIcon);
	}
}

void Window::Show()
{
	if (!IsVisible())
	{
		ShowWindow(m_Platform->Wnd, SW_SHOWDEFAULT);
	}
	else
	{
		// Already visible, bring window to top
		SetWindowPos(m_Platform->Wnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
	}
}

void Window::Hide()
{
	ShowWindow(m_Platform->Wnd, SW_HIDE);
}

void Window::Minimise()
{
	ShowWindow(m_Platform->Wnd, SW_MINIMIZE);
}

void Window::Maximise()
{
	ShowWindow(m_Platform->Wnd, SW_MAXIMIZE);
}

void Window::Restore()
{
	ShowWindow(m_Platform->Wnd, SW_RESTORE);
}

void Window::Close()
{
	DestroyWindow(m_Platform->Wnd);
}

WindowEventFlags Window::Update()
{
	WindowEventFlags FrameEvents = WindowEvent::None;

	MSG msg;
	while (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	for (const auto& Event : m_Platform->Events)
	{
		switch (Event.first)
		{
		case WM_SIZE:
		{
			int2 NewSize(LOWORD(Event.second.LParam), HIWORD(Event.second.LParam));
			if (Event.second.WParam == SIZE_MINIMIZED)
			{
				FrameEvents |= WindowEvent::Minimise;
				OnResized(WindowEvent::Minimise, m_Size);
			}
			else if (Event.second.WParam == SIZE_MAXIMIZED)
			{
				FrameEvents |= WindowEvent::Maximise;
				m_Size = NewSize;
				OnResized(WindowEvent::Maximise, m_Size);
			}
			else
			{
				FrameEvents |= WindowEvent::Restore;
				m_Size = NewSize;
				OnResized(WindowEvent::Restore, m_Size);
			}
		}
			break;

		case WM_MOVE:
		{
			int2 NewPos(static_cast<int16_t>(LOWORD(Event.second.LParam)), static_cast<int16_t>(HIWORD(Event.second.LParam)));
			if (NewPos != m_Position)
			{
				m_Position = NewPos;
				FrameEvents |= WindowEvent::Move;
				OnMoved(m_Position);
			}
		}
			break;

		case WM_DESTROY:
			FrameEvents |= WindowEvent::Destroy;
			OnDestroyed();
			break;

		case WM_SETFOCUS:
			OnFocus(true);
			FrameEvents |= WindowEvent::Focus;
			break; 

		case WM_KILLFOCUS:
			OnFocus(false);
			FrameEvents |= WindowEvent::Focus;
			break;
		}
	}

	m_Platform->Events.clear();

	return FrameEvents;
}

bool Window::IsVisible() const
{
	if (IsWindowVisible(m_Platform->Wnd) && !IsIconic(m_Platform->Wnd))
	{
		return true;
	}

	return false;
}

bool Window::IsOpen() const
{
	return m_Platform->Wnd != nullptr;
}

bool Window::HasFocus() const
{
	return GetActiveWindow() == m_Platform->Wnd;
}

void Window::Flags(WindowStyleFlags flags)
{
	if (m_Flags != flags)
	{
		m_Flags = flags;

		DWORD Style = GetWindowLong(m_Platform->Wnd, GWL_STYLE) | WS_OVERLAPPEDWINDOW;
		if (flags.HasFlags(WindowStyle::FixedSize))
		{
			Style &= ~(WS_THICKFRAME | WS_MAXIMIZEBOX);
		}
		if (flags.HasFlags(WindowStyle::NoBorder))
		{
			Style &= ~WS_OVERLAPPEDWINDOW;
		}
		SetWindowLong(m_Platform->Wnd, GWL_STYLE, Style);

		if (flags.HasFlags(WindowStyle::AlwaysOnTop))
		{
			SetWindowPos(m_Platform->Wnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
		}
		else
		{
			SetWindowPos(m_Platform->Wnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
		}
		Size(m_Size);
	}
}

void Window::Size(const int2& size)
{
	m_Size = size;

	DWORD Style = GetWindowLong(m_Platform->Wnd, GWL_STYLE);
	DWORD ExStyle = GetWindowLong(m_Platform->Wnd, GWL_EXSTYLE);

	RECT WindowRect = { 0, 0, size.x, size.y };
	AdjustWindowRectEx(&WindowRect, Style, FALSE, ExStyle);

	int Width = WindowRect.right - WindowRect.left;
	int Height = WindowRect.bottom - WindowRect.top;
	SetWindowPos(m_Platform->Wnd, NULL, 0, 0, Width, Height, SWP_NOMOVE | SWP_NOZORDER);
}

void Window::Position(const int2& position)
{
	m_Position = position;
	SetWindowPos(m_Platform->Wnd, NULL, m_Position.x, m_Position.y, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
}

//-- Private Methods --//
//=====================//

void Window::PlatformInit(const String& title, const int2& size, WindowStyleFlags flags)
{
	m_Platform = new PlatformData();
	
	auto WideTitle = Win32::GetWideString(title);

	WNDCLASSEX wcex;
	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = GetModuleHandle(nullptr);
	wcex.hIcon = nullptr;
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH) (COLOR_WINDOW + 1);
	wcex.lpszMenuName = nullptr;
	wcex.lpszClassName = WideTitle;
	wcex.hIconSm = nullptr;

	RegisterClassEx(&wcex);

	m_Platform->Wnd = CreateWindow(WideTitle, WideTitle, WS_OVERLAPPEDWINDOW, 0, 0, size.x, size.y, NULL, NULL, GetModuleHandle(nullptr), NULL);
	Flags(flags);
	Size(size);

	SetProp(m_Platform->Wnd, L"SK::Window_PlatformData", m_Platform);
}

namespace
{
	LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
	{
		Window::PlatformData* WindowData = static_cast<Window::PlatformData*>(GetProp(hWnd, L"SK::Window_PlatformData"));

		if (WindowData)
		{
			WindowData->Events[message] = { wParam, lParam };
		}
		
		HICON OldIcon;
		switch (message)
		{
		case WM_SYSKEYDOWN:
		case WM_SYSKEYUP:
		case WM_SYSCHAR:
			// Ignore these
			break;

		case WM_DESTROY:
			RemoveProp(hWnd, L"SK::Window_PlatformData");
			OldIcon = reinterpret_cast<HICON>(SetClassLongPtr(hWnd, GCLP_HICON, 0));
			if (OldIcon != nullptr)
			{
				DestroyIcon(OldIcon);
			}
			OldIcon = reinterpret_cast<HICON>(SetClassLongPtr(hWnd, GCLP_HICONSM, 0));
			if (OldIcon != nullptr)
			{
				DestroyIcon(OldIcon);
			}
			if (WindowData)
			{
				WindowData->Wnd = nullptr;
			}
			PostQuitMessage(0);
			break;

		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		return 0;
	}

	HICON GetHIcon(const Graphics::Image& image)
	{
		Math::int2 Size = image.Size();
		uint32_t ANDLength = Size.y * WIDTHBYTES(Size.x);
		uint32_t ImageLength = Size.x * Size.y * 4;

		IO::MemoryStream IconData;
		IconData.Length(sizeof(Graphics::BitmapInfoHeader) + ANDLength + ImageLength);

		// Write header
		Graphics::BitmapInfoHeader Header;
		memset(&Header, 0, sizeof(Graphics::BitmapInfoHeader));
		Header.HeaderLength = sizeof(Graphics::BitmapInfoHeader);
		Header.Bpp = 32;
		Header.Compression = BI_RGB;
		Header.Height = Size.y * 2;
		Header.Width = Size.x;
		Header.Planes = 1;
		IconData.WriteData(reinterpret_cast<uint8_t*>(&Header), 0, sizeof(Graphics::BitmapInfoHeader));

		// Write image data
		if (image.Format() == Graphics::InternalFormat::RGBA || image.Format() == Graphics::InternalFormat::BGRA)
		{
			int Stride = (Size.x * 4);
			for (int Row = Size.y - 1; Row != -1; --Row)
			{
				for (int Col = 0; Col < Size.x; ++Col)
				{
					int Offset = (Row * Stride) + Col * 4;

					if (image.Format() == Graphics::InternalFormat::RGBA)
					{
						IconData.Write(image.Data()[Offset + 2]);
						IconData.Write(image.Data()[Offset + 1]);
						IconData.Write(image.Data()[Offset]);
						IconData.Write(image.Data()[Offset + 3]);
					}
					else
					{
						IconData.Write(image.Data()[Offset]);
						IconData.Write(image.Data()[Offset + 1]);
						IconData.Write(image.Data()[Offset + 2]);
						IconData.Write(image.Data()[Offset + 3]);
					}
				}
			}
		}
		else if (image.Format() == Graphics::InternalFormat::RGB || image.Format() == Graphics::InternalFormat::BGR)
		{
			int Stride = (Size.x * 3);
			for (int Row = Size.y - 1; Row != -1; --Row)
			{
				for (int Col = 0; Col < Size.x; ++Col)
				{
					int Offset = (Row * Stride) + Col * 3;

					if (image.Format() == Graphics::InternalFormat::RGBA)
					{
						IconData.Write(image.Data()[Offset + 2]);
						IconData.Write(image.Data()[Offset + 1]);
						IconData.Write(image.Data()[Offset]);
						IconData.Write(uint8_t(0xFF));
					}
					else
					{
						IconData.Write(image.Data()[Offset]);
						IconData.Write(image.Data()[Offset + 1]);
						IconData.Write(image.Data()[Offset + 2]);
						IconData.Write(uint8_t(0xFF));
					}
				}
			}
		}

		return CreateIconFromResourceEx(IconData.Data(), 0, TRUE, 0x00030000, Size.x, Size.y, LR_DEFAULTCOLOR);
	}
}