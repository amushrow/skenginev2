//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <SKSystem/GUI/Window.h>

using namespace SK;
using namespace SK::GUI;
using namespace SK::Math;

//-- Declarations --//
//==================//

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

Window::Window(const String& title, const int2& size, WindowStyleFlags flags)
: m_Flags(WindowStyle::Normal)
, m_Size(0)
, m_Position(0)
, FocusChanged(m_FocusChanged)
, Resized(m_Resized)
, Moved(m_Moved)
, Destroyed(m_Destroyed)
{
	PlatformInit(title, size, flags);
}

//-- Private Methods --//
//=====================//

void Window::OnFocus(bool focus)
{
	WindowFocusEventData Data = { focus };
	m_FocusChanged.OnEvent(this, Data);
}

void Window::OnResized(WindowEvent evt, Math::int2 size)
{
	WindowSizeEventData Data = { evt, size };
	m_Resized.OnEvent(this, Data);
}

void Window::OnMoved(Math::int2 position)
{
	WindowMoveEventData Data = { position };
	m_Moved.OnEvent(this, Data);
}

void Window::OnDestroyed()
{
	EmptyEventData Data;
	m_Destroyed.OnEvent(this, Data);
}