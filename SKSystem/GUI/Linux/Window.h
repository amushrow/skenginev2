#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <SKSystem/GUI/Window.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xatom.h>

namespace SK
{
	namespace GUI
	{
		//---- Types ----//
		//===============//

		enum WindowMessage
		{
			Delete = 0x00,
			NB_MESSAGES
		};

		struct Window::PlatformData
		{
			PlatformData(Display* inst, ::Window& wnd, int screen)
			: DisplayInstance(inst), Wnd(wnd), Screen(screen), Initialised(false), Mapped(false) {}

			Display* DisplayInstance;
			::Window   Wnd;
			int      Screen;
			bool     Initialised;
			bool     Mapped;

			Atom       Atoms[NB_MESSAGES];
			std::vector<Atom> State;
		};

		//--- Methods ---//
		//===============//
	}
}
