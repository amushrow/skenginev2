//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright © 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <SKSystem/Debug.h>
#include <SKSystem/Utils.h>
#include <SKSystem/IO/MemoryStream.h>
#include <SKGraphics/Image/BitmapFileFormat.h>
#include <SKSystem/GUI/Linux/Window.h>

using namespace SK;
using namespace SK::GUI;
using namespace SK::Math;

//-- Declarations --//
//==================//

// Stupid X11 making generic defines
#ifdef None
#undef None
#endif

namespace
{
	struct MwmHints
	{
		unsigned long flags;
		unsigned long functions;
		unsigned long decorations;
		long input_mode;
		unsigned long status;
	};

	enum
	{
		MWM_HINTS_FUNCTIONS = (1L << 0),
		MWM_HINTS_DECORATIONS = (1L << 1),

		MWM_FUNC_ALL = (1L << 0),
		MWM_FUNC_RESIZE = (1L << 1),
		MWM_FUNC_MOVE = (1L << 2),
		MWM_FUNC_MINIMIZE = (1L << 3),
		MWM_FUNC_MAXIMIZE = (1L << 4),
		MWM_FUNC_CLOSE = (1L << 5)
	};

	void SetWindowProperty(const char* prop, bool enabled, SK::GUI::Window::PlatformData& window);
	void SetWindowProperty(const char* prop, const char* prop2, bool enabled, SK::GUI::Window::PlatformData& window);
	bool HasWindowProperty(const char* prop, SK::GUI::Window::PlatformData& window);

	int WriteIconCardinal(const SK::Graphics::Image& image, SK::IO::Stream& ouput);
}

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

GUI::Window::~Window()
{
	delete m_Platform;
}

void GUI::Window::SetTitle(const String& title)
{
	XStoreName(m_Platform->DisplayInstance, m_Platform->Wnd, title);
}

void GUI::Window::SetIcon(const SK::Graphics::MipmapImage& icons)
{
	SK::IO::MemoryStream ImageData;
	int Length = 0;
	for (const auto& image : icons)
	{
		Length += WriteIconCardinal(image, ImageData);
	}

	Atom net_wm_icon = XInternAtom(m_Platform->DisplayInstance, "_NET_WM_ICON", False);
	Atom cardinal = XInternAtom(m_Platform->DisplayInstance, "CARDINAL", False);
	XChangeProperty(m_Platform->DisplayInstance, m_Platform->Wnd, net_wm_icon, cardinal, 32, PropModeReplace, ImageData.Data(), Length);
}

void GUI::Window::Show()
{
	if (m_Platform->Initialised)
	{
		if (!m_Platform->Mapped)
		{
			XMapRaised(m_Platform->DisplayInstance, m_Platform->Wnd);
			m_Platform->Mapped = true;
		}
		else
		{
			// Already mapped, bring window to top
			XClientMessageEvent ev;
			ev.type = ClientMessage;
			ev.window = m_Platform->Wnd;
			ev.message_type = XInternAtom(m_Platform->DisplayInstance, "_NET_ACTIVE_WINDOW", false);
			ev.format = 32;
			ev.data.l[0] = 1;
			ev.data.l[1] = CurrentTime;
			ev.data.l[2] = m_Platform->Wnd;
			XSendEvent(m_Platform->DisplayInstance, DefaultRootWindow(m_Platform->DisplayInstance), false,
				SubstructureRedirectMask | SubstructureNotifyMask,
				(XEvent *) &ev);
		}
	}
}

void GUI::Window::Hide()
{
	if (m_Platform->Initialised)
	{
		XWithdrawWindow(m_Platform->DisplayInstance, m_Platform->Wnd, m_Platform->Screen);
		m_Platform->Mapped = false;
	}
}

void GUI::Window::Minimise()
{
	if (m_Platform->Initialised)
	{
		XIconifyWindow(m_Platform->DisplayInstance, m_Platform->Wnd, m_Platform->Screen);
	}
}

void GUI::Window::Maximise()
{
	if (m_Platform->Initialised)
	{
		if (m_Platform->Mapped)
		{
			// Make sure window state is normal
			XMapWindow(m_Platform->DisplayInstance, m_Platform->Wnd);
		}
		SetWindowProperty("_NET_WM_STATE_MAXIMIZED_HORZ", "_NET_WM_STATE_MAXIMIZED_VERT", true, *m_Platform);
	}
}

void GUI::Window::Restore()
{
	if (m_Platform->Initialised)
	{
		if (m_Platform->Mapped)
		{
			XMapWindow(m_Platform->DisplayInstance, m_Platform->Wnd);
		}
		SetWindowProperty("_NET_WM_STATE_MAXIMIZED_HORZ", "_NET_WM_STATE_MAXIMIZED_VERT", false, *m_Platform);
	}
}

void GUI::Window::Close()
{
	if (m_Platform->Initialised)
	{
		XDestroyWindow(m_Platform->DisplayInstance, m_Platform->Wnd);
	}
}

WindowEventFlags GUI::Window::Update()
{
	WindowEventFlags FrameEvents = WindowEvent::None;

	if (m_Platform->Initialised)
	{
		XEvent evt;
		XSelectInput(m_Platform->DisplayInstance, m_Platform->Wnd, ClientMessage | FocusChangeMask | StructureNotifyMask | PropertyChangeMask);

		bool KillWindow = false;
		while (XPending(m_Platform->DisplayInstance) > 0)
		{
			XNextEvent(m_Platform->DisplayInstance, &evt);

			switch (evt.type)
			{
			case ClientMessage:
				if ((Atom) evt.xclient.data.l[0] == m_Platform->Atoms[Delete])
				{
					KillWindow = true;
				}
				break;

			case ConfigureNotify:
			{
									XWindowAttributes xwa;
									XGetWindowAttributes(m_Platform->DisplayInstance, m_Platform->Wnd, &xwa);
									Math::int2 NewSize(xwa.width, xwa.height);
									Math::int2 NewPos;
									::Window child;
									XTranslateCoordinates(m_Platform->DisplayInstance, xwa.root, m_Platform->Wnd,
										xwa.x, xwa.y, &NewPos.x, &NewPos.y, &child);
									NewPos = -NewPos;

									if (NewSize != m_Size)
									{
										m_Size = NewSize;
										if (HasWindowProperty("_NET_WM_STATE_MAXIMIZED_VERT", *m_Platform) &&
											HasWindowProperty("_NET_WM_STATE_MAXIMIZED_HORZ", *m_Platform))
										{
											FrameEvents |= WindowEvent::Maximise;
											SetWindowProperty("_NET_WM_STATE_MAXIMIZED_VERT", "_NET_WM_STATE_MAXIMIZED_HORZ", true, *m_Platform);
										}
										else
										{
											FrameEvents |= WindowEvent::Restore;
											SetWindowProperty("_NET_WM_STATE_MAXIMIZED_VERT", "_NET_WM_STATE_MAXIMIZED_HORZ", false, *m_Platform);
										}
									}
									if (NewPos != m_Position)
									{
										m_Position = NewPos;
										FrameEvents |= WindowEvent::Move;
									}
			}
				break;

			case PropertyNotify:
				if (evt.xproperty.state == PropertyNewValue && m_Platform->Mapped)
				{
					if (evt.xproperty.atom == XInternAtom(m_Platform->DisplayInstance, "_NET_WM_STATE", false))
					{
						if (IsVisible())
						{
							FrameEvents |= WindowEvent::Restore;
						}
						else
						{
							FrameEvents |= WindowEvent::Minimise;
						}
					}
				}
				break;

			case FocusIn:
			case FocusOut:
				if (evt.xfocus.mode == NotifyNormal)
				{
					FrameEvents |= WindowEvent::Focus;
				}
				break;
			}
		}

		if (KillWindow)
		{
			XDestroyWindow(m_Platform->DisplayInstance, m_Platform->Wnd);
			XCloseDisplay(m_Platform->DisplayInstance);
			m_Platform->Initialised = false;

			FrameEvents |= WindowEvent::Destroy;
		}
	}

	return FrameEvents;
}

bool GUI::Window::IsVisible() const
{
	if (m_Platform->Initialised)
	{
		Atom actual_type;
		int actual_format;
		unsigned long i, num_items, bytes_after;
		Atom* atoms = nullptr;

		XGetWindowProperty(m_Platform->DisplayInstance, m_Platform->Wnd,
			XInternAtom(m_Platform->DisplayInstance, "_NET_WM_STATE", false),
			0, 1024, false, XA_ATOM, &actual_type, &actual_format,
			&num_items, &bytes_after, (unsigned char**) &atoms);

		Atom hidden = XInternAtom(m_Platform->DisplayInstance, "_NET_WM_STATE_HIDDEN", false);
		for (int i = 0; i<num_items; ++i)
		{
			if (atoms[i] == hidden)
			{
				XFree(atoms);
				return false;
			}
		}
		XFree(atoms);

		return m_Platform->Mapped;
	}

	return false;
}

bool GUI::Window::IsOpen() const
{
	return m_Platform->Initialised;
}

bool GUI::Window::HasFocus() const
{
	::Window ActiveWnd;
	int FocusReturn;
	XGetInputFocus(m_Platform->DisplayInstance, &ActiveWnd, &FocusReturn);
	return ActiveWnd == m_Platform->Wnd;
}

void GUI::Window::Flags(WindowStyleFlags flags)
{
	if (m_Flags != flags)
	{
		m_Flags = flags;

		if (m_Platform->Initialised)
		{
			// Fixed size
			XSizeHints noResize;
			noResize.min_width = noResize.max_width = m_Size.x;
			noResize.min_height = noResize.max_height = m_Size.y;
			noResize.flags = 0;
			if (flags.HasFlags(WindowStyle::FixedSize))
			{
				noResize.flags = PMinSize | PMaxSize;
			}
			XSetWMNormalHints(m_Platform->DisplayInstance, m_Platform->Wnd, &noResize);

			// Always on top
			SetWindowProperty("_NET_WM_STATE_ABOVE", flags.HasFlags(WindowStyle::AlwaysOnTop), *m_Platform);
			if (IsVisible())
			{
				// Bring back to top
				Show();
			}

			// NoBorder
			Atom mwmHintsProperty = XInternAtom(m_Platform->DisplayInstance, "_MOTIF_WM_HINTS", 0);
			struct MwmHints hints;
			hints.decorations = 0;
			hints.flags = flags.HasFlags(WindowStyle::NoBorder) ? MWM_HINTS_DECORATIONS : 0;
			XChangeProperty(m_Platform->DisplayInstance, m_Platform->Wnd,
				mwmHintsProperty, mwmHintsProperty, 32,
				PropModeReplace, (unsigned char*) &hints, 5);
		}
	}
}

void GUI::Window::Size(const int2& size)
{
	m_Size = size;
	if (m_Platform->Initialised)
	{
		if (m_Flags.HasFlags(WindowStyle::FixedSize))
		{
			XSizeHints noResize;
			noResize.min_width = noResize.max_width = size.x;
			noResize.min_height = noResize.max_height = size.y;
			noResize.flags = PMinSize | PMaxSize;
			XSetWMNormalHints(m_Platform->DisplayInstance, m_Platform->Wnd, &noResize);
		}
		else
		{
			XResizeWindow(m_Platform->DisplayInstance, m_Platform->Wnd, size.x, size.y);
		}
	}
}

void GUI::Window::Position(const int2& position)
{
	m_Position = position;
	if (m_Platform->Initialised)
	{
		XMoveWindow(m_Platform->DisplayInstance, m_Platform->Wnd, position.x, position.y);
	}
}

//-- Private Methods --//
//=====================//

void GUI::Window::PlatformInit(const String& title, const int2& size, WindowStyleFlags flags)
{
	Display* Inst = XOpenDisplay(nullptr);
	int Screen = DefaultScreen(Inst);
	
	unsigned long ColourBlack = BlackPixel(Inst, Screen);
	::Window Wnd = XCreateSimpleWindow(Inst, 
										DefaultRootWindow(Inst),
										0, 0, size.x, size.y, 0, ColourBlack, 0xFFFFFFFF);
										
	m_Platform = new PlatformData(Inst, Wnd, Screen);
										
	XSelectInput(m_Platform->DisplayInstance, m_Platform->Wnd, StructureNotifyMask);
	XStoreName(m_Platform->DisplayInstance, m_Platform->Wnd, title);

	m_Platform->Atoms[Delete] = XInternAtom(m_Platform->DisplayInstance, "WM_DELETE_WINDOW", false);
	XSetWMProtocols(m_Platform->DisplayInstance, m_Platform->Wnd, &m_Platform->Atoms[Delete], 1);
	
	m_Platform->Initialised = true;
	
	Size(size);
	Flags(flags);
}

namespace
{
	void SetWindowProperty(const char* prop, bool enabled, SK::GUI::Window::PlatformData& window)
	{
		Atom Property = XInternAtom(window.DisplayInstance, prop, false);
		auto Index = std::find(window.State.begin(), window.State.end(), Property);
		if (Index == window.State.end() && enabled)
		{
			// Add property
			window.State.push_back(Property);
		}
		else if (Index != window.State.end() && !enabled)
		{
			// Remove property
			window.State.erase(Index);
		}
		
		if (window.Mapped)
		{
			XEvent xev;
			memset(&xev, 0, sizeof(xev));
			xev.type = ClientMessage;
			xev.xclient.window = window.Wnd;
			xev.xclient.message_type = XInternAtom(window.DisplayInstance, "_NET_WM_STATE", false);
			xev.xclient.format = 32;
			xev.xclient.data.l[0] = enabled ? 1 : 0;
			xev.xclient.data.l[1] = Property;
			XSendEvent(window.DisplayInstance, RootWindow(window.DisplayInstance, window.Screen), false, SubstructureRedirectMask|SubstructureNotifyMask, &xev);
		}
		else
		{
			XChangeProperty(window.DisplayInstance, window.Wnd,
			                XInternAtom(window.DisplayInstance, "_NET_WM_STATE", true),
						    XA_ATOM,  32,  PropModeReplace, (unsigned char*)&window.State[0], window.State.size());
		}
	}
	
	void SetWindowProperty(const char* prop, const char* prop2, bool enabled, SK::GUI::Window::PlatformData& window)
	{
		Atom Property1 = XInternAtom(window.DisplayInstance, prop, false);
		Atom Property2 = XInternAtom(window.DisplayInstance, prop2, false);
		auto Index = std::find(window.State.begin(), window.State.end(), Property1);
		if (Index == window.State.end() && enabled)
		{
			// Add property
			window.State.push_back(Property1);
		}
		else if (Index != window.State.end() && !enabled)
		{
			// Remove property
			window.State.erase(Index);
		}
		
		Index = std::find(window.State.begin(), window.State.end(), Property2);
		if (Index == window.State.end() && enabled)
		{
			// Add property
			window.State.push_back(Property2);
		}
		else if (Index != window.State.end() && !enabled)
		{
			// Remove property
			window.State.erase(Index);
		}
		
		if (window.Mapped)
		{
			XEvent xev;
			memset(&xev, 0, sizeof(xev));
			xev.type = ClientMessage;
			xev.xclient.window = window.Wnd;
			xev.xclient.message_type = XInternAtom(window.DisplayInstance, "_NET_WM_STATE", false);
			xev.xclient.format = 32;
			xev.xclient.data.l[0] = enabled ? 1 : 0;
			xev.xclient.data.l[1] = Property1;
			xev.xclient.data.l[2] = Property2;
			XSendEvent(window.DisplayInstance, RootWindow(window.DisplayInstance, window.Screen), false, SubstructureRedirectMask|SubstructureNotifyMask, &xev);
		}
		else
		{
			XChangeProperty(window.DisplayInstance, window.Wnd,
			                XInternAtom(window.DisplayInstance, "_NET_WM_STATE", true),
						    XA_ATOM,  32,  PropModeReplace, (unsigned char*)&window.State[0], window.State.size());
		}
	}
	
	bool HasWindowProperty(const char* prop, SK::GUI::Window::PlatformData& window)
	{
		Atom Property = XInternAtom(window.DisplayInstance, prop, false);
		
		Atom actual_type;
		int actual_format;
		unsigned long i, num_items, bytes_after;
		Atom* atoms = nullptr;
		
		XGetWindowProperty(window.DisplayInstance, window.Wnd, 
						   XInternAtom(window.DisplayInstance, "_NET_WM_STATE", false),
						   0, 1024, false, XA_ATOM, &actual_type, &actual_format, 
						   &num_items, &bytes_after, (unsigned char**)&atoms);
						   
		
		for(int i=0; i<num_items; ++i)
		{
			if(atoms[i]==Property)
			{
				XFree(atoms);
				return true;
			}
		}
		XFree(atoms);
		return false;
	}
	
	int WriteIconCardinal(const SK::Graphics::Image& image, SK::IO::Stream& ouput)
	{
		ouput.Write(static_cast<long>(image.Size().x), static_cast<long>(image.Size().y));
		if (image.Format() == Graphics::InternalFormat::RGBA || image.Format() == Graphics::InternalFormat::BGRA)
		{
			int Stride = (image.Size().x * 4);
			for (int Row = 0; Row < image.Size().y; ++Row)
			{
				for (int Col = 0; Col < image.Size().x; ++Col)
				{
					int Offset = (Row * Stride) + Col * 4;
					
					if (image.Format() == Graphics::InternalFormat::RGBA)
					{
						ouput.Write(image.Data()[Offset + 2]);
						ouput.Write(image.Data()[Offset + 1]);
						ouput.Write(image.Data()[Offset]);
						ouput.Write(image.Data()[Offset + 3]);
					}
					else
					{
						ouput.Write(image.Data()[Offset]);
						ouput.Write(image.Data()[Offset + 1]);
						ouput.Write(image.Data()[Offset + 2]);
						ouput.Write(image.Data()[Offset + 3]);
					}
					
					#if __x86_64__
					ouput.Write(int(0));
					#endif
				}
			}
		}
		else if (image.Format() == Graphics::InternalFormat::RGB || image.Format() == Graphics::InternalFormat::BGR)
		{
			int Stride = (image.Size().x * 3);
			for (int Row = 0; Row < image.Size().y; --Row)
			{
				for (int Col = 0; Col < image.Size().x; ++Col)
				{
					int Offset = (Row * Stride) + Col * 3;
				
					if (image.Format() == Graphics::InternalFormat::RGBA)
					{
						ouput.Write(image.Data()[Offset + 2]);
						ouput.Write(image.Data()[Offset + 1]);
						ouput.Write(image.Data()[Offset]);
						ouput.Write(uint8_t(0xFF));
					}
					else
					{
						ouput.Write(image.Data()[Offset]);
						ouput.Write(image.Data()[Offset + 1]);
						ouput.Write(image.Data()[Offset + 2]);
						ouput.Write(uint8_t(0xFF));
					}
					
					#if __x86_64__
					ouput.Write(int(0));
					#endif
				}
			}
		}

		return 2 + (image.Size().x * image.Size().y);
	}
}