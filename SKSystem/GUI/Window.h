#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <SKSystem/Math/Types.h>
#include <SKSystem/Text/String.h>
#include <SKSystem/Flags.h>
#include <SKSystem/Events.h>
#include <SKGraphics/Image/MipmapImage.h>

namespace SK
{
	namespace GUI
	{
		//---- Types ----//
		//===============//

		enum class WindowEvent
		{
			None = 0x00,
			Focus = 0x01,
			Minimise = 0x02,
			Maximise = 0x04,
			Restore = 0x08,
			Move = 0x10,
			Destroy = 0x20
		};

		struct WindowFocusEventData
		{
			bool Focus;
		};

		struct WindowSizeEventData
		{
			WindowEvent EventType;
			Math::int2  Size;
		};

		struct WindowMoveEventData
		{
			Math::int2 Position;
		};

		enum class WindowStyle
		{
			Normal = 0x00,
			FixedSize = 0x01,
			NoBorder = 0x02,
			AlwaysOnTop = 0x04
		};

		typedef Flags<WindowEvent> WindowEventFlags;
		typedef Flags<WindowStyle> WindowStyleFlags;

		class Window
		{
		public:
			//---- Types ----//
			//===============//

			struct PlatformData;

			//--- Methods ---//
			//===============//

			Window(const String& title, const Math::int2& size, WindowStyleFlags flags);
			~Window();

			void                SetTitle(const String& title);
			void                SetIcon(const SK::Graphics::MipmapImage& icons);
			void                Show();
			void                Hide();
			void                Minimise();
			void                Maximise();
			void                Restore();
			void                Close();
							    
			WindowEventFlags    Update();
							    
			bool                IsVisible() const;
			bool                IsOpen() const;
			bool                HasFocus() const;
							    
			const PlatformData* Data() const { return m_Platform; }
			PlatformData*       Data() { return m_Platform; }
			WindowStyleFlags    Flags() const { return m_Flags; }
			void                Flags(WindowStyleFlags flags);
			const Math::int2&   Size() const { return m_Size; }
			void                Size(const Math::int2& size);
			const Math::int2&   Position() const { return m_Position; }
			void                Position(const Math::int2& position);

			//-- Variables --//
			//===============//

			EventDelegate<WindowFocusEventData> FocusChanged;
			EventDelegate<WindowSizeEventData>  Resized;
			EventDelegate<WindowMoveEventData>  Moved;
			EventDelegate<EmptyEventData>       Destroyed;

		private:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			void PlatformInit(const String& title, const Math::int2& size, WindowStyleFlags flags);
			void OnFocus(bool focus);
			void OnResized(WindowEvent evt, Math::int2 size);
			void OnMoved(Math::int2 position);
			void OnDestroyed();

			//-- Variables --//
			//===============//

			WindowStyleFlags m_Flags;
			Math::int2       m_Size;
			Math::int2       m_Position;
			PlatformData*    m_Platform;

			Event<WindowFocusEventData> m_FocusChanged;
			Event<WindowSizeEventData>  m_Resized;
			Event<WindowMoveEventData>  m_Moved;
			Event<EmptyEventData>       m_Destroyed;
		};

		//--- Methods ---//
		//===============//
	}
}