//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include "SKSystem/Debug.h"

using namespace SK;

//-- Declarations --//
//==================//

template class SK::Debug::OutputStream<char>;
template class SK::Debug::OutputStream<wchar_t>;

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

void Debug::Halt()
{
	__builtin_trap();
}

template<class Elem>
typename Debug::OutputStream<Elem>::DebugBuffer::int_type  Debug::OutputStream<Elem>::DebugBuffer::overflow(int_type c)
{
	if (!Traits::eq_int_type(c, Traits::eof()))
	{
		m_Buffer += Traits::to_char_type(c);
		if (c == Traits::to_int_type('\n'))
			sync();
	}
	return Traits::not_eof(c);
}

namespace SK
{
	template<>
	int Debug::OutputStream<char>::DebugBuffer::sync()
	{
		if (!m_Buffer.empty())
		{
			std::cout << m_Buffer.c_str();
		}
		m_Buffer.clear();

		return 0;
	}

	template<>
	int Debug::OutputStream<wchar_t>::DebugBuffer::sync()
	{
		if (!m_Buffer.empty())
		{
			std::wcout << m_Buffer.c_str();
		}
		m_Buffer.clear();

		return 0;
	}
}

//-- Private Methods --//
//=====================//