//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright © 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <stdlib.h>
#include <cstring>
#include <SKSystem/Utils.h>
#include "Guid.h"

using namespace SK;

//-- Declarations --//
//==================//

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

Guid::Guid()
{

}

Guid::Guid(uint32_t data1, int16_t data2, uint16_t data3, const uint8_t data4[8])
{
	Parts.Data1 = data1;
	Parts.Data2 = data2;
	Parts.Data3 = data3;
	memcpy(Parts.Data4, data4, 8);
}

Guid::Guid(const Guid& rhs)
{
	memcpy(Data, rhs.Data, 16);
}

Guid::Guid(const String& string)
{
	String GuidString(string);
	GuidString.Replace("0x", "");

	char HexVal[3];
	HexVal[2] = 0;
	int GuidIndex = 0;
	int ValIndex = 0;
	for (CodePoint c : GuidString)
	{
		if (c == '{' || c == '}' || c == '-' || c == '(' || c == ')')
		{
			if (ValIndex != 0)
			{
				throw SK::ArgumentException("Invalid Guid string");
			}
			continue;
		}

		HexVal[ValIndex] = c;
		ValIndex = 1 - ValIndex;
		if (ValIndex == 0)
		{
			Data[GuidIndex++] = static_cast<uint8_t>(strtol(HexVal, nullptr, 16));
		}
	}

	if (GuidIndex != 16)
	{
		throw SK::ArgumentException("Invalid Guid string");
	}
}

bool Guid::operator==(const Guid& rhs) const
{
	return memcmp(Data, rhs.Data, 16) == 0;
}

bool Guid::operator!=(const Guid& rhs) const
{
	return memcmp(Data, rhs.Data, 16) != 0;
}

//-- Private Methods --//
//=====================//