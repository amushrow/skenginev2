#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <SKSystem/IO/Stream.h>

namespace SK
{
	namespace IO
	{
		//---- Types ----//
		//===============//

		class EmbeddedStream : public Stream
		{
		public:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			EmbeddedStream(Stream* source, uint64_t length);
			EmbeddedStream(Stream* source, uint64_t start, uint64_t length);
			virtual ~EmbeddedStream();

			//--Stream Interface
			void     WriteData(const uint8_t* buffer, uint64_t offset, uint64_t length) override;
			uint64_t ReadData(uint8_t* buffer, uint64_t offset, uint64_t length) override;
			uint64_t CopyTo(Stream& rhs);
			uint64_t Length() override;
			void     Length(uint64_t pos) override;
			uint64_t Position() override;
			void     Position(uint64_t pos) override;
			void     Close() override;
			bool     IsOpen() override;

			bool     CanSeek() override  { return m_Source->CanSeek(); }
			bool     CanRead() override  { return m_Source->CanRead(); }
			bool     CanWrite() override { return m_Source->CanWrite(); }
			//--

			//-- Variables --//
			//===============//

		private:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			//-- Variables --//
			//===============//

			Stream*   m_Source;
			uint64_t  m_SourcePos;
			uint64_t  m_Length;
			uint64_t  m_Pos;
		};

		//--- Methods ---//
		//===============//
	}
}