#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <SKSystem/Text/String.h>

namespace SK
{
	namespace IO
	{
		//---- Types ----//
		//===============//

		enum class SpecialFolder
		{
			None,
			AppData,
			CommonAppData,
			Desktop,
			Documents,
			Install,
			Temporary
		};

		class Path
		{
		public:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			Path(const char* absolute);
			Path(const String& absolute);
			Path(SpecialFolder root);
			Path(SpecialFolder root, const String& relative);

			operator String() { return m_SystemPath; }
			operator const String&() const { return m_SystemPath; }

			bool          operator==(const Path& other) const { return m_SystemPath == other.m_SystemPath; }
			bool          operator!=(const Path& other) const { return m_SystemPath != other.m_SystemPath; }
			const String& GetSystemPath() const               { return m_SystemPath; }

			static Path   Combine(const Path& root, const String& relative);
			static Path   Combine(const Path& root, const std::vector<String>& relativeParts);
			static bool   IsRelative(const String& path);

			//-- Variables --//
			//===============//

		private:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			//-- Variables --//
			//===============//

			SpecialFolder m_RootFolder;
			String        m_SystemPath;
		};

		//--- Methods ---//
		//===============//
	}
}