#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright © 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//

//-- Declarations --//
//==================//

namespace SK
{
	namespace IO
	{
		//---- Types ----//
		//===============//

		class Stream;

		enum class SerializeMode
		{
			Read,
			Write
		};

		template<SerializeMode Mode>
		class Serializer
		{
		public:

			//--- Methods ---//
			//===============//
			
			Serializer(Stream& stream) {}

			template <typename... Arguments>
			bool operator()(Arguments&&... arguments)
			{
				//static_assert(false, "Serializer implementation not found.");
				return false;
			}
		};

		template<typename T>
		struct Serialize
		{
			template<SerializeMode Mode>
			bool operator()(Serializer<Mode>&, T&);
		};

		//--- Methods ---//
		//===============//
	}
}