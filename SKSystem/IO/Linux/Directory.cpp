//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <sstream>
#include <iomanip>
#include <dirent.h>
#include <libgen.h>
#include <unistd.h>
#include <sys/stat.h>
#include <SKSystem/Utils.h>
#include <SKSystem/Text/Encoding.h>
#include <SKSystem/IO/FileSystem.h>

using namespace SK;
using namespace SK::IO;
using namespace SK::Encoding;

//-- Declarations --//
//==================//

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

Directory::Directory(const Path& path)
: m_Path(path)
{
	if (!Exists(path))
	{
		throw FileNotFoundException(String::Join({ "'", path, "' could not be found." }));
	}

	auto DirectoryCopy = UTF8::GetBytes(path, true);
	auto NameCopy = UTF8::GetBytes(path, true);
	m_Directory = dirname(reinterpret_cast<char*>(DirectoryCopy.Get()));
	m_Name = basename(reinterpret_cast<char*>(NameCopy.Get()));
}

DirectoryList Directory::GetDirectories()
{
	DirectoryList Result;

	DIR* TopDirectory = opendir(m_Path);
	if (!TopDirectory)
	{
		throw IOException("SK::IO::Directory::GetDirectories - Unable to find directories");
	}

	struct dirent64* SubDirectory;
	while ((SubDirectory = readdir64(TopDirectory)) != nullptr)
	{
		if (SK::CheckFlags(SubDirectory->d_type, DT_DIR))
		{
			Result.push_back(std::make_shared<Directory>( String::Join({ m_Path, "/", SubDirectory->d_name}) ));
		}
	}
	
	closedir(TopDirectory);

	return Result;
}

FileList Directory::GetFiles()
{
	FileList Result;

	DIR* TopDirectory = opendir(m_Path);
	if (!TopDirectory)
	{
		throw IOException("SK::IO::Directory::GetDirectories - Unable to find directories");
	}

	struct dirent64* SubFile;
	while ((SubFile = readdir64(TopDirectory)) != nullptr)
	{
		if (!SK::CheckFlags(SubFile->d_type, DT_DIR))
		{
			Result.push_back(std::make_shared<File>( String::Join({ m_Path, "/", SubFile->d_name}) ));
		}
	}

	closedir(TopDirectory);

	return Result;
}

void Directory::Rename(const String& name)
{
	if (rename(m_Path, String::Join({ m_Directory, "\\", name })))
	{
		throw IOException("SK::IO::Directory::Rename - Unable to rename directory");
	}
}

bool Directory::Exists(const Path& path)
{
	struct stat64 statbuf;
	if (stat64(path.GetSystemPath(), &statbuf) != -1)
	{
		if (S_ISDIR(statbuf.st_mode))
		{
			// This is a directory
			return true;
		}
	}

	return false;
}

void Directory::Copy(const Path& source, const Path& destination, bool overwrite)
{
	Directory Source(source);
	String DestinationPath = String::Join({ destination, "\\", Source.Name() });
	Create(DestinationPath);
	for (const auto& SourceFile : Source.GetFiles())
	{
		SourceFile->Copy(String::Join({ DestinationPath, "\\", SourceFile->Name() }), overwrite);
	}

	for (const auto& SourceDir : Source.GetDirectories())
	{
		Copy(SourceDir->FullName(), DestinationPath, overwrite);
	}
}

void Directory::Move(const Path& source, const Path& destination, bool overwrite)
{
	Copy(source, destination, overwrite);
	Delete(source);
}

void Directory::Delete(const Path& path)
{
	Directory Source(path);
	for (const auto& SourceFile : Source.GetFiles())
	{
		File::Delete(SourceFile->FullName());
	}

	for (const auto& SourceDir : Source.GetDirectories())
	{
		Delete(SourceDir->FullName());
	}

	if (rmdir(path.GetSystemPath()))
	{
		if (errno != ENOENT)
		{
			std::stringstream ErrorString;
			ErrorString << "Unable to delete directory. Error: 0x" << std::hex << std::setfill('0') << std::setw(8) << errno;
			throw IOException(ErrorString.str());
		}
	}
}

void Directory::Create(const Path& path)
{
	mkdir(path.GetSystemPath(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
}

//-- Private Methods --//
//=====================//
