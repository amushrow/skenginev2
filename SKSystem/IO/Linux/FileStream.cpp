//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <cstdio>
#include <limits>
#include <unistd.h>
#include <SKSystem/Utils.h>
#include <SKSystem/Debug.h>
#include <SKSystem/IO/FileStream.h>

using namespace SK::IO;

//-- Declarations --//
//==================//

struct SK::IO::FileStream::FileStreamData
{
	FILE*         File;
	FileModeFlags Mode;
};

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

FileStream::FileStream(const Path& fileName, FileModeFlags mode)
: m_Data(new FileStreamData())
{
	Debug::Assert(!mode.HasFlags(FileMode::Append, FileMode::Truncate), "SK::IO::FileStream - Cannot Append and Truncate at the same time");

	String StrMode;

	if (mode.HasFlags(FileMode::Append))
	{
		if (mode.HasFlags(FileMode::Read))
		{
			StrMode = "a+b";
		}
		else
		{
			StrMode = "ab";
		}
	}
	else if (mode.HasFlags(FileMode::Truncate))
	{
		if (mode.HasFlags(FileMode::Read))
		{
			StrMode = "w+b";
		}
		else
		{
			StrMode = "wb";
		}
	}
	else if (mode.HasFlags(FileMode::Write))
	{
		StrMode = "r+b";
	}
	else
	{
		StrMode = "rb";
	}
	
	m_Data->File = fopen64(fileName.GetSystemPath(), StrMode);
	m_Data->Mode = mode;
}

FileStream::~FileStream()
{
	Close();
}

void FileStream::Close()
{
	if (m_Data->File)
	{
		fclose(m_Data->File);
		m_Data->File = nullptr;
	}
}

bool FileStream::IsOpen()
{
	return m_Data->File != nullptr;
}

void FileStream::WriteData(const uint8_t* buffer, uint64_t offset, uint64_t length)
{
	Debug::Assert(IsOpen(), "SK::IO::FileStream::WriteData - The stream is not open");
	Debug::Assert(CanWrite(), "SK::IO::FileStream::WriteData - The stream is read only");

	while (length > std::numeric_limits<std::size_t>::max())
	{
		fwrite(buffer + offset, 1, std::numeric_limits<std::size_t>::max(), m_Data->File);
		length -= std::numeric_limits<std::size_t>::max();
		offset += std::numeric_limits<std::size_t>::max();
	}
	fwrite(buffer + offset, 1, static_cast<std::size_t>(length), m_Data->File);
}

uint64_t FileStream::ReadData(uint8_t* buffer, uint64_t offset, uint64_t length)
{
	Debug::Assert(IsOpen(), "SK::IO::FileStream::ReadData - The stream is not open");
	Debug::Assert(CanRead(), "SK::IO::FileStream::WriteData - The stream is write only");

	uint64_t TotalBytesRead = 0;
	while (length > std::numeric_limits<std::size_t>::max())
	{
		TotalBytesRead += fread(buffer + offset, 1, std::numeric_limits<std::size_t>::max(), m_Data->File);
		length -= std::numeric_limits<std::size_t>::max();
		offset += std::numeric_limits<std::size_t>::max();
	}
	TotalBytesRead += fread(buffer + offset, 1, static_cast<std::size_t>(length), m_Data->File);
	return TotalBytesRead;
}

uint64_t FileStream::CopyTo(Stream& rhs)
{
	Debug::Assert(this != &rhs, "SK::IO::FileStream::CopyTo - Cannot copy a stream to itself");
	Debug::Assert(IsOpen(), "SK::IO::FileStream::CopyTo - The stream is not open");
	Debug::Assert(CanRead(), "SK::IO::FileStream::CopyTo - The stream is write only");

	uint8_t Buffer[4096];
	uint64_t OriginalPosition = Position();
	Position(0);

	uint64_t BytesWritten = 0;
	while (BytesWritten < Length())
	{
		uint64_t ChunkSize = ReadData(Buffer, 0, 4096);
		rhs.WriteData(Buffer, 0, ChunkSize);
		BytesWritten += ChunkSize;
	}

	Position(OriginalPosition);
	return BytesWritten;
}

uint64_t FileStream::Length()
{
	Debug::Assert(IsOpen(), "SK::IO::FileStream::Length - The stream is not open");
	
	off64_t OriginalPosition = ftello64(m_Data->File);
	fseeko64(m_Data->File, 0, SEEK_END);

	off64_t Length = ftello64(m_Data->File);
	fseeko64(m_Data->File, OriginalPosition, SEEK_SET);

	return static_cast<uint64_t>(Length);
}

void FileStream::Length(uint64_t length)
{
	Debug::Assert(IsOpen(), "SK::IO::FileStream::Length - The stream is not open");
	Debug::Assert(CanWrite(), "SK::IO::FileStream::Length - The stream is read only");

	uint64_t OriginalPosition = Position();
	fflush(m_Data->File);
	ftruncate64(fileno(m_Data->File), static_cast<off64_t>(length));

	if (length > OriginalPosition)
	{
		Position(OriginalPosition);
	}
	else
	{
		Position(length);
	}
}

uint64_t FileStream::Position()
{
	Debug::Assert(IsOpen(), "SK::IO::FileStream::Position - The stream is not open");
	return static_cast<uint64_t>(ftello64(m_Data->File));
}

void FileStream::Position(uint64_t position)
{
	Debug::Assert(IsOpen(), "SK::IO::FileStream::Position - The stream is not open");
	fseeko64(m_Data->File, static_cast<off64_t>(position), SEEK_SET);
}

bool FileStream::CanRead()
{
	return IsOpen() && m_Data->Mode.HasFlags(FileMode::Read);
}

bool FileStream::CanWrite()
{
	return IsOpen()&& m_Data->Mode.HasFlags(FileMode::Write);
}

//-- Private Methods --//
//=====================//