//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <cstring>
#include <limits>
#include <SKSystem/Utils.h>
#include <SKSystem/Text/Encoding.h>
#include "Stream.h"

using namespace SK;
using namespace SK::IO;
using namespace SK::Encoding;

//-- Declarations --//
//==================//

namespace
{
	//-- The following from Beej's Guide to Network Programming
	//-- http://beej.us/guide/bgnet/output/html/singlepage/bgnet.html#serialization
	#define pack754_32(f) (pack754((f), 32, 8))
	#define pack754_64(f) (pack754((f), 64, 11))
	#define unpack754_32(i) (unpack754((i), 32, 8))
	#define unpack754_64(i) (unpack754((i), 64, 11))

	uint64_t pack754(long double f, unsigned bits, unsigned expbits);
	long double unpack754(uint64_t i, unsigned bits, unsigned expbits);
	//--
}

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//


void Stream::Write(bool value)
{
	WriteData(reinterpret_cast<const uint8_t*>(&value), 0, 1);
}

void Stream::Write(float value)
{
	Write(uint32_t(pack754_32(value)));
}

void Stream::Write(double value)
{
	Write(pack754_64(value));
}

void Stream::Write(const String& value)
{
	uint64_t size = value.Size();
	Write(size);
	WriteData(value.Buffer(), 0, size);
}

void Stream::Write(const char* value)
{
	uint64_t length = strlen(value);
	Write(length);
	WriteData(reinterpret_cast<const uint8_t*>(value), 0, length);
}

bool Stream::Read(bool& value)
{
	return ReadData(reinterpret_cast<uint8_t*>(&value), 0, 1) == 1;
}

bool Stream::Read(float& value)
{
	uint32_t PackedFloat = 0;
	bool Success = Read(PackedFloat);
	value = static_cast<float>(unpack754_32(PackedFloat));
	return Success;
}

bool Stream::Read(double& value)
{
	uint64_t PackedDouble = 0;
	bool Success = Read(PackedDouble);
	value = unpack754_64(PackedDouble);
	return Success;
}

bool Stream::Read(String& value)
{
	uint64_t Size = 0;
	bool Success = Read(Size);

	if (Size > std::numeric_limits<std::size_t>::max())
	{
		throw NotEnoughMemoryException("SK::IO::Stream::Read(string) - Data is too large to fit in memory");
	}

	if (Success)
	{
		uint8_t* Buffer = new uint8_t[static_cast<std::size_t>(Size)];
		Success = ReadData(Buffer, 0, Size) == Size;
		value = std::move(UTF8::FromBytes(Buffer, static_cast<std::size_t>(Size)));
		delete[] Buffer;
	}

	return Success;
}

//-- Private Methods --//
//=====================//

namespace
{
	//-- The following from Beej's Guide to Network Programming
	//-- http://beej.us/guide/bgnet/output/html/singlepage/bgnet.html#serialization
	uint64_t pack754(long double f, unsigned bits, unsigned expbits)
	{
		long double fnorm;
		int shift;
		long long sign, exp, significand;
		unsigned significandbits = bits - expbits - 1; // -1 for sign bit

		if (f == 0.0) return 0; // get this special case out of the way

		// check sign and begin normalization
		if (f < 0) { sign = 1; fnorm = -f; }
		else { sign = 0; fnorm = f; }

		// get the normalized form of f and track the exponent
		shift = 0;
		while (fnorm >= 2.0) { fnorm /= 2.0; shift++; }
		while (fnorm < 1.0) { fnorm *= 2.0; shift--; }
		fnorm = fnorm - 1.0;

		// calculate the binary form (non-float) of the significand data
		significand = static_cast<long long>(fnorm * ((1LL << significandbits) + 0.5f));

		// get the biased exponent
		exp = shift + ((1 << (expbits - 1)) - 1); // shift + bias

		// return the final answer
		return (sign << (bits - 1)) | (exp << (bits - expbits - 1)) | significand;
	}

	long double unpack754(uint64_t i, unsigned bits, unsigned expbits)
	{
		long double result;
		long long shift;
		unsigned bias;
		unsigned significandbits = bits - expbits - 1; // -1 for sign bit

		if (i == 0) return 0.0;

		// pull the significand
		result = static_cast<long double>(i&((1LL << significandbits) - 1)); // mask
		result /= (1LL << significandbits); // convert back to float
		result += 1.0f; // add the one back on

		// deal with the exponent
		bias = (1 << (expbits - 1)) - 1;
		shift = ((i >> significandbits)&((1LL << expbits) - 1)) - bias;
		while (shift > 0) { result *= 2.0; shift--; }
		while (shift < 0) { result /= 2.0; shift++; }

		// sign it
		result *= (i >> (bits - 1)) & 1 ? -1.0 : 1.0;

		return result;
	}
	//--
}