#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <SKSystem/IO/Stream.h>

namespace SK
{
	namespace IO
	{
		//---- Types ----//
		//===============//

		class MemoryStream : public Stream
		{
		public:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			MemoryStream();
			MemoryStream(uint8_t* data, uint64_t length, bool readOnly = true, bool owner = false);
			MemoryStream(const uint8_t* data, uint64_t length);
			virtual ~MemoryStream();

			//--Stream Interface
			void           WriteData(const uint8_t* buffer, uint64_t offset, uint64_t length) override;
			uint64_t       ReadData(uint8_t* buffer, uint64_t offset, uint64_t length) override;
			uint64_t       CopyTo(Stream& rhs);
			uint64_t       Length() override;
			void           Length(uint64_t pos) override;
			uint64_t       Position() override;
			void           Position(uint64_t pos) override;
			void           Close() override;
			bool           IsOpen() override;

			bool           CanSeek() override  { return true; }
			bool           CanRead() override  { return true; }
			bool           CanWrite() override { return true; }
			//--

			const uint8_t* Data() const { return m_Data; }
			uint8_t*       Data() { return m_Data; }

			//-- Variables --//
			//===============//

		private:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			//-- Variables --//
			//===============//

			uint8_t*    m_Data;
			uint64_t    m_Length;
			uint64_t    m_Pos;
			std::size_t m_BufferSize;
			bool        m_Owner;
			bool        m_ReadOnly;
		};
	
		//--- Methods ---//
		//===============//
	}
}