//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <sstream>
#include <iomanip>
#include <SKSystem/Win32/Win32.h>
#include <SKSystem/Utils.h>
#include <SKSystem/Text/Encoding.h>
#include <SKSystem/IO/FileSystem.h>

using namespace SK;
using namespace SK::IO;
using namespace SK::Encoding;

//-- Declarations --//
//==================//

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

Directory::Directory(const Path& path)
: m_Path(path)
{

	const String& SystemPath = path.GetSystemPath();
	if (!Exists(path))
	{
		throw FileNotFoundException(String::Join({ "'", SystemPath, "' could not be found." }));
	}

	std::size_t MaxPathLength = SystemPath.Length();
	wchar_t* Drive = new wchar_t[MaxPathLength];
	wchar_t* Dir = new wchar_t[MaxPathLength];
	wchar_t* Filename = new wchar_t[MaxPathLength];
	_wsplitpath_s(Win32::GetWideString(SystemPath), Drive, MaxPathLength, Dir, MaxPathLength, Filename, MaxPathLength, nullptr, 0);
	
	wcscat_s(Drive, MaxPathLength, Dir);
	m_Directory = UTF16::FromBytes(reinterpret_cast<uint8_t*>(Drive));
	m_Name = UTF16::FromBytes(reinterpret_cast<uint8_t*>(Filename));

	delete[] Drive;
	delete[] Dir;
	delete[] Filename;
}

DirectoryList Directory::GetDirectories()
{
	DirectoryList Result;

	WIN32_FIND_DATA FileData;
	HANDLE NextFile = FindFirstFile(Win32::GetWideString(String::Join({ m_Path, "\\*" })), &FileData);

	if (NextFile == INVALID_HANDLE_VALUE)
	{
		throw IOException("SK::IO::Directory::GetDirectories - Unable to find directories");
	}

	do
	{
		if (CheckFlags(FileData.dwFileAttributes, FILE_ATTRIBUTE_DIRECTORY))
		{
			if (wcscmp(FileData.cFileName, L".") != 0 && wcscmp(FileData.cFileName, L"..") != 0)
			{
				Result.push_back(std::make_shared<Directory>(String::Join({ m_Path, "\\", UTF16::FromBytes(reinterpret_cast<uint8_t*>(FileData.cFileName)) })));
			}
		}
	} while (FindNextFile(NextFile, &FileData) != 0);

	FindClose(NextFile);

	return Result;
}

FileList Directory::GetFiles()
{
	FileList Result;

	WIN32_FIND_DATA FileData;
	HANDLE NextFile = FindFirstFile(Win32::GetWideString(String::Join({ m_Path, "\\*" })), &FileData);

	if (NextFile == INVALID_HANDLE_VALUE)
	{
		throw IOException("SK::IO::Directory::GetFiles - Unable to find files");
	}

	do
	{
		if (!CheckFlags(FileData.dwFileAttributes, FILE_ATTRIBUTE_DIRECTORY))
		{
			Result.push_back(std::make_shared<File>(String::Join({ m_Path, "\\", UTF16::FromBytes(reinterpret_cast<uint8_t*>(FileData.cFileName)) })));
		}
	} while (FindNextFile(NextFile, &FileData) != 0);

	FindClose(NextFile);

	return Result;
}

void Directory::Rename(const String& name)
{
	MoveFile(Win32::GetWideString(m_Path), Win32::GetWideString(String::Join({ m_Directory, "\\", name })));
}

bool Directory::Exists(const Path& path)
{
	DWORD Attr = GetFileAttributes(Win32::GetWideString(path));
	if (Attr == INVALID_FILE_ATTRIBUTES && GetLastError() == ERROR_FILE_NOT_FOUND)
	{
		// File not found
		return false;
	}
	else if (Attr != INVALID_FILE_ATTRIBUTES && CheckFlags(Attr, FILE_ATTRIBUTE_DIRECTORY))
	{
		// Path is a directory
		return true;
	}

	return false;
}

void Directory::Copy(const Path& source, const Path& destination, bool overwrite)
{
	Directory Source(source);
	String DestinationPath = String::Join({ destination, "\\", Source.Name() });
	Create(DestinationPath);
	for (const auto& SourceFile : Source.GetFiles())
	{
		SourceFile->Copy(String::Join({ DestinationPath, "\\", SourceFile->Name() }), overwrite);
	}
	
	for (const auto& SourceDir : Source.GetDirectories())
	{
		Copy(SourceDir->FullName(), DestinationPath, overwrite);
	}
}

void Directory::Move(const Path& source, const Path& destination, bool overwrite)
{
	Copy(source, destination, overwrite);
	Delete(source);
}

void Directory::Delete(const Path& path)
{
	Directory Source(path);
	for (const auto& SourceFile : Source.GetFiles())
	{
		File::Delete(SourceFile->FullName());
	}

	for (const auto& SourceDir : Source.GetDirectories())
	{
		Delete(SourceDir->FullName());
	}

	if (!RemoveDirectory(Win32::GetWideString(path)))
	{
		DWORD Error = GetLastError();
		if (Error != ERROR_FILE_NOT_FOUND)
		{
			std::stringstream ErrorString;
			ErrorString << "Unable to delete directory. Error: 0x" << std::hex << std::setfill('0') << std::setw(8) << Error;
			throw IOException(ErrorString.str());
		}
	}
}

void Directory::Create(const Path& path)
{
	CreateDirectory(Win32::GetWideString(path), nullptr);
}

//-- Private Methods --//
//=====================//