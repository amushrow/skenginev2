//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <SKSystem/Events.h>

//-- Declarations --//
//==================//

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//
namespace
{
	unsigned int CurrentEventKey = 0;
}

//-- Public Methods --//
//====================//

SK::EventKey SK::GetEventKey()
{
	return EventKey(CurrentEventKey++);
}

//-- Private Methods --//
//=====================//
