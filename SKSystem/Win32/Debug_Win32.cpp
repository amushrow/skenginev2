//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <SKSystem/Win32/Win32.h>
#include <SKSystem/Debug.h>

using namespace SK;

//-- Declarations --//
//==================//

//-- Public Variables --//
//======================//

template class SK::Debug::OutputStream<char>;
template class SK::Debug::OutputStream<wchar_t>;

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

void Debug::Halt()
{
	DebugBreak();
}

template<class Elem>
typename Debug::OutputStream<Elem>::DebugBuffer::int_type  Debug::OutputStream<Elem>::DebugBuffer::overflow(int_type c)
{
	if (!Traits::eq_int_type(c, Traits::eof()))
	{
		m_Buffer += Traits::to_char_type(c);
	}
	return Traits::not_eof(c);
}

template<>
int Debug::OutputStream<char>::DebugBuffer::sync()
{
	if (!m_Buffer.empty())
	{
		OutputDebugStringW(Win32::GetWideString(m_Buffer));
		std::cout << m_Buffer.c_str();
	}
	m_Buffer.clear();      
	
	return 0;
}

template<>
int Debug::OutputStream<wchar_t>::DebugBuffer::sync()
{
	if (!m_Buffer.empty())
	{
		OutputDebugStringW(m_Buffer.c_str());
		std::cout << m_Buffer.c_str();
	}
	m_Buffer.clear();      
	
	return 0;
}

//-- Private Methods --//
//=====================//