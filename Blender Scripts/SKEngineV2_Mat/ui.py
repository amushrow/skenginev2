﻿##=============================================================##
##                                                             ##
##                         SKEngineV2                          ##
##   -------------------------------------------------------   ##
##  Copyright © 2013 - 2014 Anthony Mushrow                    ##
##  All rights reserved                                        ##
##                                                             ##
##  This code is licensed under The Code Project Open License  ##
##  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     ##
##  for more information.                                      ##
##                                                             ##
##=============================================================##

import bpy

class SKEngineMaterialPanel(bpy.types.Panel):
    bl_label = "SKEngine Material"
    bl_idname = "OBJECT_PT_SKMaterial"

    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "material"

    def draw(self, context):
        layout = self.layout

        box = layout.box()
        row = box.row()
        row.prop(context.material.SKMaterialProps, "sortOrder")
        row = box.row()
        row.prop(context.material.SKMaterialProps, "vertexFormat")
        row = box.row()
        row.prop(context.material.SKMaterialProps, "cullable")
        row = box.row()
        row.prop(context.material.SKMaterialProps, "vertexShader")
        row = box.row()
        row.prop(context.material.SKMaterialProps, "pixelShader")

