﻿##=============================================================##
##                                                             ##
##                         SKEngineV2                          ##
##   -------------------------------------------------------   ##
##  Copyright © 2013 - 2014 Anthony Mushrow                    ##
##  All rights reserved                                        ##
##                                                             ##
##  This code is licensed under The Code Project Open License  ##
##  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     ##
##  for more information.                                      ##
##                                                             ##
##=============================================================##

bl_info = {
    "name":         "SKEngine Material",
    "author":       "Anthony Mushrow",
    "blender":      (2,6,7),
    "version":      (0,0,1),
    "description":  "Expose SKEngine material properties",
    "category":     "Material"
}
    

import bpy
import bpy.props

from SKEngineV2_Mat import properties, ui

def register():
    properties.register()
    bpy.utils.register_module(__name__)


def unregister():
    properties.unregister()
    bpy.utils.unregister_module(__name__)

if __name__ == "__main__":
    register()