#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <SKSystem/Text/String.h>
#include <SKSystem/Guid.h>
#include <SKInput/DeviceKeys.h>
#include <SKSystem/Events.h>

namespace SK
{
	namespace Input
	{
		//---- Types ----//
		//===============//

		enum class Type
		{
			Keyboard,
			Mouse,
			Joypad,
			Wheel,
			Joystick
		};

		class KeyEventArgs
		{
		public:
			KeyEventArgs(Key key, float value)
				: m_Key(key), m_Value(value)
			{

			}

			Key   Key() const { return m_Key;  }
			float Value() const { return m_Value; }

		private:
			SK::Input::Key m_Key;
			float          m_Value;
		};

		struct AxisMagnitude
		{
			AxisMagnitude(ForceFeedbackAxis axis, float mag)
			: Axis(axis), Magnitude(mag) {}

			ForceFeedbackAxis Axis;
			float             Magnitude;
		};

		struct DeviceInfo
		{
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			bool operator==(const DeviceInfo& rhs) const
			{
				return VendorId == rhs.VendorId && ProductId == rhs.ProductId && Interface == rhs.Interface;
			}

			bool operator!=(const DeviceInfo& rhs) const
			{
				return VendorId != rhs.VendorId || ProductId != rhs.ProductId || Interface != rhs.Interface;
			}

			//-- Variables --//
			//===============//

			uint16_t VendorId;
			uint16_t ProductId;
			uint16_t Interface;
		};


		class Device
		{
		public:
			//---- Types ----//
			//===============//

			friend class DeviceManager;

			//--- Methods ---//
			//===============//

			Device(const String& name, const SK::Guid& guid, const DeviceInfo& devInfo);
			virtual ~Device() {}

			Device(const Device& rhs) = delete;
			Device& operator=(const Device& rhs) = delete;
			Device(const Device&& rhs) = delete;
			Device& operator=(const Device&& rhs) = delete;

			bool                     operator==(const Device& other) const { return this == &other; }
			bool                     operator!=(const Device& other) const { return this != &other; }

			unsigned int             Id() const         { return m_Id; }
			const String&            Name() const       { return m_Name; }
			const SK::Guid&          Guid() const       { return m_Guid; }
			const Input::DeviceInfo& DeviceInfo() const { return m_DevInfo; }
			virtual String           GetKeyName(Key key) const;

			virtual void             ClearInput() = 0;
			virtual Input::Type      Type() const = 0;
			virtual float            GetValue(Key key) const = 0;

			virtual void             SetForceFeedback(const std::vector<AxisMagnitude>& axes) {}
			void                     SetForceFeedback(ForceFeedbackAxis axis, float magnitude);
			virtual void             ClearForceFeedback() {}
			virtual FFAxisFlags      AvailableFFAxis() const { return FFAxisFlags(); }

			//-- Variables --//
			//===============//

			// Triggered when a button is released
			EventDelegate<KeyEventArgs> ButtonUp;
			// Triggered when a button is first pressed
			EventDelegate<KeyEventArgs> ButtonDown;
			// Triggered when the state of an Axis has changed
			EventDelegate<KeyEventArgs> AxisChanged;

		protected:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//
			
			void OnButtonUp(Key key);
			void OnButtonDown(Key key);
			void OnAxisChanged(Key key, float value);

			//-- Variables --//
			//===============//

		private:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			virtual void             Update() = 0;
			void                     SetId(unsigned int id) { m_Id = id; }

			//-- Variables --//
			//===============//

			unsigned int      m_Id;
			String            m_Name;
			SK::Guid          m_Guid;
			Input::DeviceInfo m_DevInfo;

			Event<KeyEventArgs> m_ButtonUp;
			Event<KeyEventArgs> m_ButtonDown;
			Event<KeyEventArgs> m_AxisChanged;
		};

		//--- Methods ---//
		//===============//
	}
}