#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <SKSystem/Singleton.h>
#include <SKInput/Device.h>

namespace SK
{
	namespace Input
	{
		//---- Types ----//
		//===============//

		enum class SpecialDevice
		{
			SystemKeyboard,
			SystemMouse
		};

		class DeviceIterator
		{
		public:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			DeviceIterator(const std::vector<Device*>& devices, bool end = false);
			DeviceIterator(const DeviceIterator& rhs);

			bool operator==(const DeviceIterator& rhs);
			bool operator!=(const DeviceIterator& rhs);

			DeviceIterator operator++(int);
			DeviceIterator operator--(int);
			DeviceIterator& operator++();
			DeviceIterator& operator--();
			const Device& operator*() const;
			Device& operator*();

			//-- Variables --//
			//===============//

		private:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			//-- Variables --//
			//===============//

			const std::vector<Device*>&          m_Devices;
			std::vector<Device*>::const_iterator m_It;
		};

		class DeviceList
		{
		public:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			DeviceList(const std::vector<Device*>& devices) : m_Devices(devices) {}
			DeviceIterator begin() { return DeviceIterator(m_Devices); }
			DeviceIterator end() { return DeviceIterator(m_Devices, true); }
			const DeviceIterator begin() const { return DeviceIterator(m_Devices); }
			const DeviceIterator end() const { return DeviceIterator(m_Devices, true); }

			const Device&        operator[](unsigned int index) const { return *m_Devices[index]; }
			Device&              operator[](unsigned int index)       { return *m_Devices[index]; }

			unsigned int NumDevices() const { return static_cast<unsigned int>(m_Devices.size()); }

			//-- Variables --//
			//===============//

		private:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			//-- Variables --//
			//===============//

			const std::vector<Device*>& m_Devices;
		};

		class DeviceManager : public DeviceList, public Singleton<DeviceManager>
		{
		public:
			//---- Types ----//
			//===============//

			friend class Singleton<DeviceManager>;
			struct PlatformData;

			//--- Methods ---//
			//===============//

			~DeviceManager();

			void          Update();
			void          FindDevices();

			const Device& operator[](SpecialDevice device) const;
			Device&       operator[](SpecialDevice device);
			const Device& operator[](unsigned int id) const { return *m_Devices[id]; }
			Device&       operator[](unsigned int id)       { return *m_Devices[id]; }

			void          DisableSystemShortcuts(bool val);
			bool          BackgroundInput() const   { return m_BackgroundInput; }
			void          BackgroundInput(bool val) { m_BackgroundInput = val; }

			DeviceList    Joypads()   const { return DeviceList(m_Joypads); }
			DeviceList    Joysticks() const { return DeviceList(m_Joysticks); }
			DeviceList    Keyboards() const { return DeviceList(m_Keyboards); }
			DeviceList    Mice()      const { return DeviceList(m_Mice); }
			DeviceList    Wheels()    const { return DeviceList(m_Wheels); }

			//-- Variables --//
			//===============//

			// Triggered when a button is released
			EventDelegate<KeyEventArgs> ButtonUp;
			// Triggered when a button is first pressed
			EventDelegate<KeyEventArgs> ButtonDown;
			// Triggered when the state of an Axis changed
			EventDelegate<KeyEventArgs> AxisChanged;

		private:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			DeviceManager();
			void          PlatformInit();
			void          AddDevice(Device* newDevice);
			void          ButtonUpHandler(void* sender, KeyEventArgs& e);
			void          ButtonDownHandler(void* sender, KeyEventArgs& e);
			void          AxisChangedHandler(void* sender, KeyEventArgs& e);

			//-- Variables --//
			//===============//

			PlatformData*        m_PlatformData;
			std::vector<Device*> m_Devices;
			bool                 m_InputFocus;
			bool                 m_BackgroundInput;
			EventKey             m_EventKey;

			std::vector<Device*> m_Mice;
			std::vector<Device*> m_Keyboards;
			std::vector<Device*> m_Joysticks;
			std::vector<Device*> m_Joypads;
			std::vector<Device*> m_Wheels;

			Event<KeyEventArgs>  m_ButtonUp;
			Event<KeyEventArgs>  m_ButtonDown;
			Event<KeyEventArgs>  m_AxisChanged;
		};

		//--- Methods ---//
		//===============//
	}
}