#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#define DIRECTINPUT_VERSION 0x0800
#include <SKSystem/Win32/Win32.h>
#include <SKInput/Device.h>
#include <dinput.h>

namespace SK
{
	namespace Input
	{
		//---- Types ----//
		//===============//

		class DirectInputDevice : public Device
		{
		public:
			//---- Types ----//
			//===============//

			friend BOOL CALLBACK DIEnumAxisCallback(const DIDEVICEOBJECTINSTANCE*, void*);

			//--- Methods ---//
			//===============//

			DirectInputDevice(LPDIRECTINPUT8 directInput, const DIDEVICEINSTANCE& device, HWND inputWindow);
			~DirectInputDevice();

			void        ClearInput() override;
			Input::Type Type() const override { return m_Type; }
			float       GetValue(Key key) const override;
			String      GetKeyName(Key key) const override;

			void        SetForceFeedback(const std::vector<AxisMagnitude>& axes) override;
			void        SetForceFeedback(ForceFeedbackAxis axis, float magnitude);
			void        ClearForceFeedback() override;
			FFAxisFlags AvailableFFAxis() const override { return m_AvailableFFAxis; }

			//-- Variables --//
			//===============//

		private:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			void        Update() override;
			void        SetButton(int index, bool val);
			void        SetAxis(int index, float val);

			static BOOL CALLBACK DIEnumObjects(const DIDEVICEOBJECTINSTANCE* objectInstance, void* user);

			//-- Variables --//
			//===============//
			
			LPDIRECTINPUTDEVICE8 m_Device;
			LPDIRECTINPUTEFFECT  m_Effect;
			bool                 m_ForceFeedback;
			bool                 m_DeviceLost;
			Input::Type          m_Type;
			bool                 m_Buttons[48];
			float                m_Axis[8];

			String               m_KeyNames[40]; //32 Buttons, 8 Axis
			FFAxisFlags          m_AvailableFFAxis;
		};

		//--- Methods ---//
		//===============//
	}
}
