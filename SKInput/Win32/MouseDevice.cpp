//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <SKInput/Win32/RawInputDevices.h>
#include <SKSystem/Text/Encoding.h>
#include <SKSystem/Utils.h>

using namespace SK;
using namespace SK::Input;

//-- Declarations --//
//==================//

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

MouseDevice::MouseDevice(HANDLE deviceHandle)
: RawInputDevice(GetDeviceName(GetDevicePath(deviceHandle), true), GetGuid(deviceHandle), GetDevInfo(deviceHandle))
, m_Handle(deviceHandle)
, m_DeltaX(0)
, m_DeltaY(0)
, m_PosX(0)
, m_PosY(0)
{
	memset(m_Buttons, 0, sizeof(m_Buttons));
}

MouseDevice::~MouseDevice() {}

void MouseDevice::ClearInput()
{
	m_WheelDelta = 0;
	m_DeltaX = 0;
	m_DeltaY = 0;

	for (int i = 0; i < 5; ++i)
	{
		if (ClearButtonState(m_Buttons[i]))
		{
			OnButtonUp(static_cast<Key>(i + static_cast<int>(Key::MouseLeftButton)));
		}
	}
}

float MouseDevice::GetValue(Key key) const
{
	switch (key)
	{
	case Key::MouseXAxis: return static_cast<float>(m_DeltaX);
	case Key::MouseYAxis: return static_cast<float>(m_DeltaY);
	case Key::MouseXPos:  return static_cast<float>(m_PosX);
	case Key::MouseYPos:  return static_cast<float>(m_PosY);
	case Key::MouseWheel: return static_cast<float>(m_WheelDelta);
	case Key::MouseLeftButton:
	case Key::MouseRightButton:
	case Key::MouseMiddleButton:
	case Key::MouseButton1:
	case Key::MouseButton2:
		{
			int Index = static_cast<int>(key) - static_cast<int>(Key::MouseLeftButton);
			return m_Buttons[Index] ? 1.0f : 0.0f;
		}
	}

	return 0.0f;
}

void MouseDevice::InputEvent(const RAWINPUT& eventData)
{
	for (int i = 0; i < 5; ++i)
	{
		if (SK::CheckFlags(eventData.data.mouse.usButtonFlags, 1 << (i * 2)))
		{
			if (SetButtonStateDown(m_Buttons[i]))
			{
				OnButtonDown(static_cast<Key>(static_cast<int>(Key::MouseLeftButton) + i));
			}
		}
		else if (SK::CheckFlags(eventData.data.mouse.usButtonFlags, 1 << ((i * 2) + 1)))
		{
			if (SetButtonStateUp(m_Buttons[i]))
			{
				OnButtonUp(static_cast<Key>(static_cast<int>(Key::MouseLeftButton) + i));
			}
		}
	}

	if (SK::CheckFlags(eventData.data.mouse.usButtonFlags, RI_MOUSE_WHEEL))
	{
		m_WheelDelta += static_cast<short>(eventData.data.mouse.usButtonData);
		OnAxisChanged(Key::MouseWheel, float(m_WheelDelta));
	}

	if (SK::CheckFlags(eventData.data.mouse.usFlags, MOUSE_MOVE_RELATIVE))
	{
		m_DeltaX += eventData.data.mouse.lLastX;
		m_DeltaY += eventData.data.mouse.lLastY;
		m_PosX += eventData.data.mouse.lLastX;
		m_PosY += eventData.data.mouse.lLastY;

		if (eventData.data.mouse.lLastX)
		{
			OnAxisChanged(Key::MouseXAxis, float(eventData.data.mouse.lLastX));
			OnAxisChanged(Key::MouseXPos, float(m_PosX));
		}
		else if (eventData.data.mouse.lLastY)
		{
			OnAxisChanged(Key::MouseYAxis, float(eventData.data.mouse.lLastY));
			OnAxisChanged(Key::MouseYPos, float(m_PosY));
		}
	}
	else if (SK::CheckFlags(eventData.data.mouse.usFlags, MOUSE_MOVE_ABSOLUTE))
	{
		m_PosX = eventData.data.mouse.lLastX;
		m_PosY = eventData.data.mouse.lLastY;
		
		if (eventData.data.mouse.lLastX)
		{
			OnAxisChanged(Key::MouseXPos, float(m_PosX));
		}
		else if (eventData.data.mouse.lLastY)
		{
			OnAxisChanged(Key::MouseYPos, float(m_PosY));
		}
	}
}

void MouseDevice::DeviceRemoved(HANDLE handle)
{
	ClearInput();
}

//-- Private Methods --//
//=====================//

void MouseDevice::Update()
{
	m_WheelDelta = 0;
	m_DeltaX = 0;
	m_DeltaY = 0;

	for (int i = 0; i < 5; ++i)
	{
		if (UpdateButtonState(m_Buttons[i]))
		{
			OnButtonUp(static_cast<Key>(i + static_cast<int>(Key::MouseLeftButton)));
		}
	}
}