//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <SKInput/Win32/DirectInputDevice.h>
#include <SKSystem/Text/Encoding.h>
#include <SKSystem/Utils.h>
#include <algorithm>

using namespace SK::Input;

//-- Declarations --//
//==================//

namespace
{
	DWORD FFAxisToDIJOFS(ForceFeedbackAxis axis);
	float GetAxisValue(LONG x);
	bool DPadUp(DWORD val);
	bool DPadDown(DWORD val);
	bool DPadLeft(DWORD val);
	bool DPadRight(DWORD val);
}

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

DirectInputDevice::DirectInputDevice(LPDIRECTINPUT8 directInput, const DIDEVICEINSTANCE& device, HWND inputWindow)
: Device(SK::Encoding::UTF16::FromBytes(reinterpret_cast<const uint8_t*>(device.tszProductName))
  , SK::Guid(device.guidInstance.Data1, device.guidInstance.Data2, device.guidInstance.Data3, device.guidInstance.Data4)
  , { device.guidProduct.Data1 & 0xFFFF, device.guidProduct.Data1 >> 16, 0 })
, m_Device(nullptr)
, m_Effect(nullptr)
, m_ForceFeedback(false)
, m_DeviceLost(true)
, m_Type(Type::Joypad)
{
	switch (device.dwDevType & 0xFF)
	{
	case DI8DEVTYPE_DRIVING: m_Type = Type::Wheel; break;
	case DI8DEVTYPE_FLIGHT:
	case DI8DEVTYPE_JOYSTICK: m_Type = Type::Joystick; break;
	default: m_Type = Type::Joypad; break;
	}

	if (DI_OK == directInput->CreateDevice(device.guidInstance, &m_Device, nullptr))
	{
		m_Device->SetDataFormat(&c_dfDIJoystick);
		m_Device->SetCooperativeLevel(inputWindow, DISCL_EXCLUSIVE | DISCL_BACKGROUND);
		m_Device->EnumObjects(DIEnumObjects, this, DIDFT_BUTTON | DIDFT_ABSAXIS);

		if (m_Device->Acquire() == DI_OK)
		{
			m_DeviceLost = false;
		}

		DIDEVCAPS caps;
		caps.dwSize = sizeof(caps);
		m_Device->GetCapabilities(&caps);

		m_ForceFeedback = CheckFlags(caps.dwFlags, DIDC_FORCEFEEDBACK);
		
		if (m_ForceFeedback)
		{
			DWORD NbAxes = 0;
			DWORD Axes[6];
			LONG Direction[6] = { 0 };
			DICONSTANTFORCE cf = { 0 };

			if (m_AvailableFFAxis.HasFlags(ForceFeedbackAxis::X))
			{
				Axes[NbAxes++] = DIJOFS_X;
			}
			if (m_AvailableFFAxis.HasFlags(ForceFeedbackAxis::Y))
			{
				Axes[NbAxes++] = DIJOFS_Y;
			}
			if (m_AvailableFFAxis.HasFlags(ForceFeedbackAxis::Z))
			{
				Axes[NbAxes++] = DIJOFS_Z;
			}
			if (m_AvailableFFAxis.HasFlags(ForceFeedbackAxis::Rx))
			{
				Axes[NbAxes++] = DIJOFS_RX;
			}
			if (m_AvailableFFAxis.HasFlags(ForceFeedbackAxis::Ry))
			{
				Axes[NbAxes++] = DIJOFS_RY;
			}
			if (m_AvailableFFAxis.HasFlags(ForceFeedbackAxis::Rz))
			{
				Axes[NbAxes++] = DIJOFS_RZ;
			}

			DIEFFECT eff;
			memset(&eff, 0, sizeof(eff));
			eff.dwSize = sizeof(DIEFFECT);
			eff.dwFlags = DIEFF_CARTESIAN | DIEFF_OBJECTOFFSETS;
			eff.dwDuration = INFINITE;
			eff.dwSamplePeriod = 0;
			eff.dwGain = DI_FFNOMINALMAX;
			eff.dwTriggerButton = DIEB_NOTRIGGER;
			eff.dwTriggerRepeatInterval = 0;
			eff.cAxes = NbAxes;
			eff.rgdwAxes = Axes;
			eff.rglDirection = Direction;
			eff.lpEnvelope = 0;
			eff.cbTypeSpecificParams = sizeof(DICONSTANTFORCE);
			eff.lpvTypeSpecificParams = &cf;
			eff.dwStartDelay = 0;
			if (m_Device->CreateEffect(GUID_ConstantForce, &eff, &m_Effect, NULL) == DI_OK)
			{
				m_Effect->SetParameters(&eff, DIEP_DIRECTION | DIEP_TYPESPECIFICPARAMS | DIEP_START);
			}
			else
			{
				m_Effect = nullptr;
			}
		}
	}
}

DirectInputDevice::~DirectInputDevice()
{
	if (m_Effect)
	{
		m_Effect->Release();
	}
	m_Device->Release();
}

void DirectInputDevice::ClearInput()
{
	for (int i = 0; i < 48; ++i)
	{
		SetButton(i, false);
	}

	for (int i = 0; i < 8; ++i)
	{
		SetAxis(i, 0.0f);
	}
}

float DirectInputDevice::GetValue(Key key) const
{
	if (key >= Key::Button1 && key <= Key::DPad4_Right)
	{
		int Index = static_cast<int>(key) - static_cast<int>(Key::Button1);
		return m_Buttons[Index] ? 1.0f : 0.0f;
	}
	else if (key >= Key::Axis_X && key <= Key::Slider2)
	{
		int Index = static_cast<int>(key) - static_cast<int>(Key::Axis_X);
		return m_Axis[Index];
	}

	return 0.0f;
}

SK::String DirectInputDevice::GetKeyName(Key key) const
{
	if (key >= Key::Button1 && key <= Key::Button32)
	{
		int Index = static_cast<int>(key) - static_cast<int>(Key::Button1);
		if (!m_KeyNames[Index].IsEmpty())
		{
			return m_KeyNames[Index];
		}
	}

	if (key >= Key::Axis_X && key <= Key::Slider2)
	{
		int Index = static_cast<int>(key) - static_cast<int>(Key::Axis_X) + 32;
		if (!m_KeyNames[Index].IsEmpty())
		{
			return m_KeyNames[Index];
		}
	}

	return Device::GetKeyName(key);
}

void DirectInputDevice::SetForceFeedback(const std::vector<AxisMagnitude>& axes)
{
	if (m_Effect)
	{
		DWORD NbAxes = 0;
		DWORD Axises[6] = { 0 };
		LONG Directions[6] = { 0 };

		LONG EffectMagnitude = 0;

		for (std::size_t i = 0; i < axes.size(); ++i)
		{
			if (m_AvailableFFAxis.HasFlags(axes[i].Axis))
			{
				LONG Dir = static_cast<LONG>(axes[i].Magnitude * DI_FFNOMINALMAX);
				Axises[NbAxes] = FFAxisToDIJOFS(axes[i].Axis);
				Directions[NbAxes] = Dir;
				++NbAxes;

				EffectMagnitude = std::max(EffectMagnitude, std::abs(Dir));
			}
		}

		DICONSTANTFORCE cf = { EffectMagnitude };
		DIEFFECT eff;
		memset(&eff, 0, sizeof(eff));
		eff.dwSize = sizeof(DIEFFECT);
		eff.dwFlags = DIEFF_OBJECTOFFSETS | DIEFF_CARTESIAN;
		eff.dwDuration = INFINITE;
		eff.cAxes = NbAxes;
		eff.rgdwAxes = Axises;
		eff.rglDirection = Directions;
		eff.cbTypeSpecificParams = sizeof(DICONSTANTFORCE);
		eff.lpvTypeSpecificParams = &cf;

		m_Effect->SetParameters(&eff, DIEP_DIRECTION | DIEP_TYPESPECIFICPARAMS | DIEP_START);
	}
}

void DirectInputDevice::ClearForceFeedback()
{
	if (m_Effect)
	{
		m_Effect->Stop();
	}
}

//-- Private Methods --//
//=====================//

void DirectInputDevice::Update()
{
	if (m_DeviceLost)
	{
		if (m_Device->Acquire() == DI_OK)
		{
			m_DeviceLost = false;
		}
	}

	if (!m_DeviceLost)
	{
		DIJOYSTATE State;
		HRESULT Result = m_Device->GetDeviceState(sizeof(State), &State);
		if (Result == DIERR_NOTACQUIRED || Result == DIERR_INPUTLOST)
		{
			memset(&State, 0, sizeof(State));
			m_Device->Unacquire();
			m_DeviceLost = true;
		}

		for (int i = 0; i < 32; ++i)
		{
			SetButton(i, (State.rgbButtons[i] & 0x80) == 0x80);
		}

		for (int i = 0; i < 4; ++i)
		{
			int Index = 32 + (i * 4);
			SetButton(Index, DPadUp(State.rgdwPOV[i]));
			SetButton(Index + 1, DPadDown(State.rgdwPOV[i]));
			SetButton(Index + 2, DPadLeft(State.rgdwPOV[i]));
			SetButton(Index + 3, DPadRight(State.rgdwPOV[i]));
		}

		SetAxis(0, GetAxisValue(State.lX));
		SetAxis(1, GetAxisValue(State.lY));
		SetAxis(2, GetAxisValue(State.lZ));
		SetAxis(3, GetAxisValue(State.lRx));
		SetAxis(4, GetAxisValue(State.lRy));
		SetAxis(5, GetAxisValue(State.lRz));
		SetAxis(6, GetAxisValue(State.rglSlider[0]));
		SetAxis(7, GetAxisValue(State.rglSlider[1]));
	}
}

void DirectInputDevice::SetButton(int index, bool val)
{
	if (val)
	{
		if (!m_Buttons[index])
		{
			m_Buttons[index] = true;
			OnButtonDown(static_cast<Key>(static_cast<int>(Key::Button1) + index));
		}
	}
	else
	{
		if (m_Buttons[index])
		{
			m_Buttons[index] = false;
			OnButtonUp(static_cast<Key>(static_cast<int>(Key::Button1) + index));
		}
	}
}

void DirectInputDevice::SetAxis(int index, float val)
{
	if (m_Axis[index] != val)
	{
		m_Axis[index] = val;
		OnAxisChanged(static_cast<Key>(static_cast<int>(Key::Axis_X) + index), val);
	}
}

BOOL CALLBACK DirectInputDevice::DIEnumObjects(const DIDEVICEOBJECTINSTANCE* objectInstance, void* user)
{
	DirectInputDevice& self = *static_cast<DirectInputDevice*>(user);

	if (objectInstance->guidType == GUID_Button)
	{
		int ButtonId = (objectInstance->dwType & DIDFT_ANYINSTANCE) >> 8;
		self.m_KeyNames[ButtonId] = SK::Encoding::UTF16::FromBytes(reinterpret_cast<const uint8_t*>(objectInstance->tszName));
	}
	else
	{
		DIPROPRANGE PropRange;
		PropRange.diph.dwSize = sizeof(DIPROPRANGE);
		PropRange.diph.dwHeaderSize = sizeof(DIPROPHEADER);
		PropRange.diph.dwHow = DIPH_BYID;
		PropRange.diph.dwObj = objectInstance->dwType;
		PropRange.lMin = -32768;
		PropRange.lMax = 32767;
		self.m_Device->SetProperty(DIPROP_RANGE, &PropRange.diph);

		int KeyNameIndex = -1;
		ForceFeedbackAxis FFAxis = ForceFeedbackAxis::X;
		if (objectInstance->guidType == GUID_XAxis)
		{
			KeyNameIndex = 32;
			FFAxis = ForceFeedbackAxis::X;
		}
		else if (objectInstance->guidType == GUID_YAxis)
		{
			KeyNameIndex = 33;
			FFAxis = ForceFeedbackAxis::Y;
		}
		else if (objectInstance->guidType == GUID_ZAxis)
		{
			KeyNameIndex = 34;
			FFAxis = ForceFeedbackAxis::Z;
		}
		else if (objectInstance->guidType == GUID_RxAxis)
		{
			KeyNameIndex = 35;
			FFAxis = ForceFeedbackAxis::Rx;
		}
		else if (objectInstance->guidType == GUID_RyAxis)
		{
			KeyNameIndex = 36;
			FFAxis = ForceFeedbackAxis::Ry;
		}
		else if (objectInstance->guidType == GUID_RzAxis)
		{
			KeyNameIndex = 37;
			FFAxis = ForceFeedbackAxis::Rz;
		}
		else if (objectInstance->guidType == GUID_Slider)
		{
			int SliderId = ((objectInstance->dwType & DIDFT_ANYINSTANCE) >> 8);
			if (SliderId < 2)
			{
				KeyNameIndex = SliderId + 38;
			}
		}

		if (KeyNameIndex != -1)
		{
			self.m_KeyNames[KeyNameIndex] = SK::Encoding::UTF16::FromBytes(reinterpret_cast<const uint8_t*>(objectInstance->tszName));
		}

		if (objectInstance->dwFlags & DIDOI_FFACTUATOR)
		{
			self.m_AvailableFFAxis |= FFAxis;
		}
	}

	return DIENUM_CONTINUE;
}

namespace
{
	DWORD FFAxisToDIJOFS(ForceFeedbackAxis axis)
	{
		switch (axis)
		{
		case ForceFeedbackAxis::X: return DIJOFS_X;
		case ForceFeedbackAxis::Y: return DIJOFS_Y;
		case ForceFeedbackAxis::Z: return DIJOFS_Z;
		case ForceFeedbackAxis::Rx: return DIJOFS_RX;
		case ForceFeedbackAxis::Ry: return DIJOFS_RY;
		case ForceFeedbackAxis::Rz: return DIJOFS_RZ;
		}

		return 0;
	}

	float GetAxisValue(LONG x)
	{
		return x > 0 ? float(x) / 32767.0f : float(x) / 32768.0f;
	}

	bool DPadUp(DWORD val)
	{
		if (val > 36000)
		{
			return false;
		}

		if (val > 27000 || val < 9000)
		{
			return true;
		}

		return false;
	}

	bool DPadDown(DWORD val)
	{
		return (val < 27000 && val > 9000);
	}

	bool DPadLeft(DWORD val)
	{
		return (val < 36000 && val > 18000);
	}

	bool DPadRight(DWORD val)
	{
		return val > 0 && val < 18000;
	}
}