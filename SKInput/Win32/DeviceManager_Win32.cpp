//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#define DIRECTINPUT_VERSION 0x0800
#include <SKSystem/Win32/Win32.h>
#include <Xinput.h>
#include <wbemidl.h>
#include <SKSystem/Threading/Thread.h>
#include <SKSystem/Queues.h>
#include <SKSystem/Debug.h>
#include <SKInput/Win32/RawInputDevices.h>
#include <SKInput/Win32/DirectInputDevice.h>
#include <SKInput/Win32/XInputDevice.h>
#include <SKInput/DeviceManager.h>

#pragma comment(lib, "dinput8.lib")
#pragma comment(lib, "dxguid.lib")

#if (WINVER >= _WIN32_WINNT_WIN8)
#pragma comment(lib, "xinput.lib")
#else
#pragma comment(lib, "xinput9_1_0.lib")
#endif

using namespace SK::Input;

//-- Declarations --//
//==================//

struct DeviceManager::PlatformData
{
	DeviceManager::PlatformData()
	: DirectInput(nullptr)
	, SpecialDevice_Keyboard(nullptr)
	, SpecialDevice_Mouse(nullptr)
	, RawInputWindow(nullptr)
	, RawInputQueue(512)
	{}

	LPDIRECTINPUT8           DirectInput;
	SystemKeyboard*          SpecialDevice_Keyboard;
	SystemMouse*             SpecialDevice_Mouse;

	std::promise<HWND>       PromisedWindow;
	std::mutex               RawInputThreadLock;
	HWND                     RawInputWindow;
	SK::RingBuffer<RAWINPUT> RawInputQueue;
	SK::Threading::Thread    RawInputThread;
};

namespace
{
	const int DEVICE_REMOVED = 0x10;
	const int DEVICE_ADDED = 0x11;

	LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	BOOL CALLBACK JoyEnumCallback(const DIDEVICEINSTANCE* inst, void* data);
	bool IsXInputDevice(const GUID& productGuid);
	void RawInputThread(DeviceManager::PlatformData* platformData);
}

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

DeviceManager::~DeviceManager()
{
	// Break out of the 'GetMessage' loop
	DWORD Win32ThreadId = GetThreadId(m_PlatformData->RawInputThread.NativeHandle());
	PostThreadMessage(Win32ThreadId, WM_QUIT, 0, 0);

	for (auto Device : m_Devices)
	{
		delete Device;
	}

	m_PlatformData->DirectInput->Release();
	m_PlatformData->RawInputThread.Join();
	delete m_PlatformData;
}

void DeviceManager::Update()
{
	DWORD ActiveProcess = 0;
	HWND Foreground = GetForegroundWindow();
	GetWindowThreadProcessId(Foreground, &ActiveProcess);
	bool AllowInput = m_BackgroundInput || ActiveProcess == GetCurrentProcessId();

	for (auto Device : m_Devices)
	{
		if (AllowInput)
		{
			Device->Update();
		}
		else
		{
			Device->ClearInput();
		}
	}

	RAWINPUT Input;
	while (m_PlatformData->RawInputQueue.Dequeue(Input))
	{
		if (AllowInput && (Input.header.wParam == RIM_INPUT || Input.header.wParam == RIM_INPUTSINK))
		{
			for (auto& Device : m_Devices)
			{
				RawInputDevice* RawDevice = dynamic_cast<RawInputDevice*>(Device);
				if (RawDevice && RawDevice->RawInutDevice() == Input.header.hDevice)
				{
					RawDevice->InputEvent(Input);
				}
			}

			if (Input.header.dwType == RIM_TYPEKEYBOARD)
			{
				m_PlatformData->SpecialDevice_Keyboard->InputEvent(Input);
			}
			else if (Input.header.dwType == RIM_TYPEMOUSE)
			{
				m_PlatformData->SpecialDevice_Mouse->InputEvent(Input);
			}
		}
		else if (Input.header.wParam == DEVICE_REMOVED)
		{
			for (auto& Device : m_Devices)
			{
				RawInputDevice* RawDevice = dynamic_cast<RawInputDevice*>(Device);
				if (RawDevice && RawDevice->RawInutDevice() == Input.header.hDevice)
				{
					RawDevice->DeviceRemoved(Input.header.hDevice);
				}
			}

			m_PlatformData->SpecialDevice_Mouse->DeviceRemoved(Input.header.hDevice);
			m_PlatformData->SpecialDevice_Keyboard->DeviceRemoved(Input.header.hDevice);
		}
	}
}

void DeviceManager::FindDevices()
{
	//-- Raw Input devices (Keyboard and Mouse)
	UINT NumFoundDevices = 0;
	GetRawInputDeviceList(nullptr, &NumFoundDevices, sizeof(RAWINPUTDEVICELIST));

	RAWINPUTDEVICELIST* FoundDevices = new RAWINPUTDEVICELIST[NumFoundDevices];
	GetRawInputDeviceList(FoundDevices, &NumFoundDevices, sizeof(RAWINPUTDEVICELIST));

	for (UINT i = 0; i < NumFoundDevices; ++i)
	{
		if (RIM_TYPEMOUSE == FoundDevices[i].dwType)
		{
			AddDevice(new MouseDevice(FoundDevices[i].hDevice));
		}
		else if (RIM_TYPEKEYBOARD == FoundDevices[i].dwType)
		{
			AddDevice(new KeyboardDevice(FoundDevices[i].hDevice));
		}
	}
	delete[] FoundDevices;
	//--

	//-- XInput Devices
#if (WINVER >= 0x0602)
	XInputEnable(true);
#endif
	for (unsigned int i = 0; i < XUSER_MAX_COUNT; ++i)
	{
		XINPUT_CAPABILITIES Caps;

		if (XInputGetCapabilities(i, 0, &Caps) == ERROR_SUCCESS)
		{
			AddDevice(new XInputDevice(i, Caps));
		}
	}
	//--

	//-- DirectInput devices (everything else)
	std::vector<DIDEVICEINSTANCE> DIDevices;
	m_PlatformData->DirectInput->EnumDevices(DI8DEVCLASS_GAMECTRL, JoyEnumCallback, &DIDevices, DIEDFL_ATTACHEDONLY);
	for (auto& NewDevice : DIDevices)
	{
		AddDevice(new DirectInputDevice(m_PlatformData->DirectInput, NewDevice, m_PlatformData->RawInputWindow));
	}
	//--
}

const Device& DeviceManager::operator[](SpecialDevice device) const
{
	if (device == SpecialDevice::SystemKeyboard)
	{
		return *m_PlatformData->SpecialDevice_Keyboard;
	}
	else
	{
		return *m_PlatformData->SpecialDevice_Mouse;
	}
}

Device& DeviceManager::operator[](SpecialDevice device)
{
	if (device == SpecialDevice::SystemKeyboard)
	{
		return *m_PlatformData->SpecialDevice_Keyboard;
	}
	else
	{
		return *m_PlatformData->SpecialDevice_Mouse;
	}
}

void DeviceManager::DisableSystemShortcuts(bool val)
{
	if (val)
	{
		RAWINPUTDEVICE RawKeyboard;
		RawKeyboard.usUsagePage = 0x01; // Generic desktop page
		RawKeyboard.usUsage = 0x06;     // Keyboard
		RawKeyboard.hwndTarget = m_PlatformData->RawInputWindow;
		RawKeyboard.dwFlags = RIDEV_INPUTSINK | RIDEV_DEVNOTIFY | RIDEV_NOHOTKEYS;
		RegisterRawInputDevices(&RawKeyboard, 1, sizeof(RAWINPUTDEVICE));
	}
	else
	{
		RAWINPUTDEVICE RawKeyboard;
		RawKeyboard.usUsagePage = 0x01; // Generic desktop page
		RawKeyboard.usUsage = 0x06;     // Keyboard
		RawKeyboard.hwndTarget = m_PlatformData->RawInputWindow;
		RawKeyboard.dwFlags = RIDEV_INPUTSINK | RIDEV_DEVNOTIFY;
		RegisterRawInputDevices(&RawKeyboard, 1, sizeof(RAWINPUTDEVICE));
	}
}

//-- Private Methods --//
//=====================//

void DeviceManager::PlatformInit()
{
	m_PlatformData = new PlatformData();

	DirectInput8Create(GetModuleHandle(nullptr), DIRECTINPUT_VERSION, IID_IDirectInput8, reinterpret_cast<void**>(&m_PlatformData->DirectInput), nullptr);
	m_PlatformData->RawInputThread = SK::Threading::Thread(RawInputThread, m_PlatformData);
	
	auto FutureWindow = m_PlatformData->PromisedWindow.get_future();
	FutureWindow.wait();
	m_PlatformData->RawInputWindow = FutureWindow.get();

	//-- Basic System devices (ie. all devices combined)
	m_PlatformData->SpecialDevice_Keyboard = new SystemKeyboard();
	m_PlatformData->SpecialDevice_Mouse = new SystemMouse();
	AddDevice(m_PlatformData->SpecialDevice_Keyboard);
	AddDevice(m_PlatformData->SpecialDevice_Mouse);
	//--
}

namespace
{
	BOOL CALLBACK JoyEnumCallback(const DIDEVICEINSTANCE* inst, void* data)
	{
		std::vector<DIDEVICEINSTANCE>& Devices = *reinterpret_cast<std::vector<DIDEVICEINSTANCE>*>(data);
		if (IsXInputDevice(inst->guidProduct))
		{
			return DIENUM_CONTINUE;
		}
		Devices.push_back(*inst);
		return DIENUM_CONTINUE;
	}

	bool IsXInputDevice(const GUID& productGuid)
	{
		bool bIsXinputDevice = false;
		
		// CoInit if needed
		bool CleanupCOM = CoInitialize(nullptr) >= 0;

		// Create WMI
		IWbemLocator* pIWbemLocator = nullptr;
		HRESULT hr = CoCreateInstance(__uuidof(WbemLocator), nullptr, CLSCTX_INPROC_SERVER,
		                              __uuidof(IWbemLocator), reinterpret_cast<void**>(&pIWbemLocator));

		if (SUCCEEDED(hr) && pIWbemLocator != nullptr)
		{
			BSTR bstrNamespace = SysAllocString(L"\\\\.\\root\\cimv2");
			BSTR bstrClassName = SysAllocString(L"Win32_PNPEntity");
			BSTR bstrDeviceID = SysAllocString(L"DeviceID");

			if (bstrNamespace && bstrDeviceID &&  bstrDeviceID)
			{
				// Connect to WMI
				IWbemServices* pIWbemServices = nullptr;
				hr = pIWbemLocator->ConnectServer(bstrNamespace, nullptr, nullptr, 0,
					                              0, nullptr, nullptr, &pIWbemServices);

				if (SUCCEEDED(hr) && pIWbemServices != nullptr)
				{
					// Switch security level to IMPERSONATE. 
					CoSetProxyBlanket(pIWbemServices, RPC_C_AUTHN_WINNT, RPC_C_AUTHZ_NONE, nullptr,
						              RPC_C_AUTHN_LEVEL_CALL, RPC_C_IMP_LEVEL_IMPERSONATE, nullptr, EOAC_NONE);

					IEnumWbemClassObject* pEnumDevices = nullptr;
					hr = pIWbemServices->CreateInstanceEnum(bstrClassName, 0, NULL, &pEnumDevices);
					if (SUCCEEDED(hr) && pEnumDevices != nullptr)
					{
						while (!bIsXinputDevice)
						{
							DWORD Returned = 0;
							IWbemClassObject* pDevices[20] = { 0 };

							// Get 20 at a time
							if (FAILED(pEnumDevices->Next(10000, 20, pDevices, &Returned)) || Returned == 0)
								break;

							for (UINT Device = 0; Device < Returned; ++Device)
							{
								// For each device, get its device ID
								VARIANT var;
								hr = pDevices[Device]->Get(bstrDeviceID, 0L, &var, NULL, NULL);
								if (SUCCEEDED(hr) && var.vt == VT_BSTR && var.bstrVal != NULL)
								{
									// Check if the device ID contains "IG_".  If it does, then it's an XInput device
									// This information can not be found from DirectInput 
									if (wcsstr(var.bstrVal, L"IG_"))
									{
										// If it does, then get the VID/PID from var.bstrVal
										DWORD dwPid = 0, dwVid = 0;
										WCHAR* strVid = wcsstr(var.bstrVal, L"VID_");
										if (strVid && swscanf_s(strVid, L"VID_%4X", &dwVid) != 1)
											dwVid = 0;
										WCHAR* strPid = wcsstr(var.bstrVal, L"PID_");
										if (strPid && swscanf_s(strPid, L"PID_%4X", &dwPid) != 1)
											dwPid = 0;

										// Compare the VID/PID to the DInput device
										DWORD dwVidPid = MAKELONG(dwVid, dwPid);
										if (dwVidPid == productGuid.Data1)
										{
											bIsXinputDevice = true;
											break;
										}
									}
								}

								if (SUCCEEDED(hr))
								{
									VariantClear(&var);
								}
							}

							for (UINT Device = 0; Device < Returned; ++Device)
							{
								if (pDevices[Device])
								{
									pDevices[Device]->Release();
									pDevices[Device] = nullptr;
								}
							}
						}

						pEnumDevices->Release();
					}

					pIWbemServices->Release();
				}
			}

			if (bstrNamespace)
				SysFreeString(bstrNamespace);
			if (bstrDeviceID)
				SysFreeString(bstrDeviceID);
			if (bstrClassName)
				SysFreeString(bstrClassName);

			pIWbemLocator->Release();
		}

		if (CleanupCOM)
			CoUninitialize();

		return bIsXinputDevice;
	}

	LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
	{
		uint8_t InputBuffer[128];
		UINT InputBufferSize = sizeof(InputBuffer);

		void* Prop = GetProp(hWnd, L"SK::DeviceManager::InputQueue");
		if (!Prop)
		{
			return DefWindowProc(hWnd, message, wParam, lParam);
		}

		SK::RingBuffer<RAWINPUT>& InputQueue = *static_cast<SK::RingBuffer<RAWINPUT>*>(Prop);
		DWORD Error = 0;
		switch (message)
		{
		case WM_INPUT_DEVICE_CHANGE:
			{
				RID_DEVICE_INFO Info;
				UINT Size = sizeof(Info);
				GetRawInputDeviceInfo(reinterpret_cast<HANDLE>(lParam), RIDI_DEVICEINFO, &Info, &Size);
				
				RAWINPUT DeviceChanged;
				DeviceChanged.header.dwType  = Info.dwType;
				DeviceChanged.header.hDevice = reinterpret_cast<HANDLE>(lParam);
				DeviceChanged.header.wParam = wParam == GIDC_ARRIVAL ? DEVICE_ADDED : DEVICE_REMOVED;

				bool Queued = InputQueue.Enqueue(DeviceChanged);
				SK::Debug::Assert(Queued, "SK::Input::DeviceManager - Input Queue is too small");
			}
			break;

		case WM_INPUT:
			if (GetRawInputData(reinterpret_cast<HRAWINPUT>(lParam), RID_INPUT, InputBuffer, &InputBufferSize, sizeof(RAWINPUTHEADER)) > 0)
			{
				RAWINPUT* Input = reinterpret_cast<RAWINPUT*>(InputBuffer);
				bool Queued = InputQueue.Enqueue(*Input);
				//SK::Debug::Assert(Queued, "SK::Input::DeviceManager - Input Queue is too small");
			}
			else
			{
				SK::Debug::Assert(false, "SK::Input::DeviceManager - Failed to get raw input data");
			}
			break;

		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		return 0;
	}

	void RawInputThread(DeviceManager::PlatformData* platformData)
	{
		WNDCLASS WC = { 0 };
		WC.hInstance = GetModuleHandle(nullptr);
		WC.lpszClassName = L"SKInputWindow";
		WC.lpfnWndProc = WndProc;
		RegisterClass(&WC);

		HWND InputWindow = CreateWindow(WC.lpszClassName, nullptr, 0, 0, 0, 0, 0, HWND_MESSAGE, nullptr, WC.hInstance, nullptr);
		SetProp(InputWindow, L"SK::DeviceManager::InputQueue", &platformData->RawInputQueue);

		RAWINPUTDEVICE RawDevices[2];
		RawDevices[0].usUsagePage = 0x01; // Generic desktop page
		RawDevices[0].usUsage = 0x06;     // Keyboard
		RawDevices[0].hwndTarget = InputWindow;
		RawDevices[0].dwFlags = RIDEV_INPUTSINK | RIDEV_DEVNOTIFY;
		RawDevices[1].usUsagePage = 0x01; // Generic desktop page
		RawDevices[1].usUsage = 0x02;     // Mouse
		RawDevices[1].hwndTarget = InputWindow;
		RawDevices[1].dwFlags = RIDEV_INPUTSINK | RIDEV_DEVNOTIFY;
		RegisterRawInputDevices(RawDevices, 2, sizeof(RAWINPUTDEVICE));

		// Set the promised window
		platformData->PromisedWindow.set_value(InputWindow);

		MSG msg;
		while (GetMessage(&msg, nullptr, 0, 0))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		RemoveProp(InputWindow, L"SK::DeviceManager::InputQueue");
		DestroyWindow(InputWindow);
	}
}