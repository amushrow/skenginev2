#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <vector>
#include <map>
#include <SKInput/DeviceManager.h>
#include <SKSystem/IO/Stream.h>
#include <SKSystem/Utils.h>

namespace SK
{
	namespace Input
	{
		//---- Types ----//
		//===============//

		class DeviceKeyPair
		{
		public:
			DeviceKeyPair(const Device* device, Key key) :
				Device(device), Key(key) {}

			const Device* Device;
			Key           Key;
		};

		struct SavedDeviceKeyPair
		{
			SK::Guid   DeviceGuid;
			DeviceInfo DeviceInfo;
			Key        Key;
		};

		class KeyBinding
		{
		public:

			//--- Methods ---//
			//===============//

			KeyBinding() {}

			KeyBinding(unsigned int device, Key deviceKey);
			KeyBinding(SpecialDevice device, Key deviceKey);
			KeyBinding(std::vector<DeviceKeyPair> bindings);

			void  DeadZone(float dead);
			void  AddBinding(unsigned int device, Key deviceKey);
			void  AddBinding(SpecialDevice device, Key deviceKey);
			void  AddBinding(const KeyBinding& other);
			void  RemoveBinding(unsigned int device, Key deviceKey);
			void  RemoveBinding(SpecialDevice device, Key deviceKey);
			void  ClearBindings();

			void  Update();

			bool  Pressed();
			bool  JustPressed();
			bool  JustReleased();
			float Value();

		private:

			//-- Variables --//
			//===============//

			std::vector<DeviceKeyPair> m_Bindings;
			float                      m_DeadZone;
			float                      m_Value;
			float                      m_PrevValue;
		};

		template<typename GameKey>
		class KeyMap
		{
		public:

			//--- Constants ---//
			//=================//

			static const uint16_t MAGICNUMBER = 0x7390;
			static const uint16_t VERSION = 1;

			//--- Methods ---//
			//===============//

			void Update()
			{
				for (auto& Binding : m_Keys)
				{
					Binding.second.Update();
				}
			}

			void Add(GameKey key, const KeyBinding& binding)
			{
				if (m_Keys.find(key) == m_Keys.end())
				{
					m_Keys.insert(std::make_pair(key, binding));
				}
				else
				{
					m_Keys[key].AddBinding(binding);
				}
			}

			KeyBinding& Binding(GameKey key)
			{
				if (m_Keys.find(key) == m_Keys.end())
				{
					KeyBinding Blank;
					m_Keys.insert(std::make_pair(key, Blank));
				}
				return m_Keys.find(key)->second;
			}

			void Clear()
			{
				m_Keys.clear();
			}

			bool  Pressed(GameKey key)
			{
				auto Binding = m_Keys.find(key);
				if (Binding != m_Keys.end())
				{
					return Binding->second.Pressed();
				}
				return false;
			}

			bool  JustPressed(GameKey key)
			{
				auto Binding = m_Keys.find(key);
				if (Binding != m_Keys.end())
				{
					return Binding->second.JustPressed();
				}
				return false;
			}

			bool  JustReleased(GameKey key)
			{
				auto Binding = m_Keys.find(key);
				if (Binding != m_Keys.end())
				{
					return Binding->second.JustReleased();
				}
				return false;
			}

			float Value(GameKey key)
			{
				auto Binding = m_Keys.find(key);
				if (Binding != m_Keys.end())
				{
					return Binding->second.Value();
				}

				return 0.0f;
			}

			void Write(SK::IO::Stream& out)
			{
				out.Write(MAGICNUMBER);
				out.Write(VERSION);
				out.Write(static_cast<uint32_t>(m_Keys.size() + m_UnavailableKeys.size()));

				// Mapped keys
				for (auto& Bindings : m_Keys)
				{
					out.Write(static_cast<uint32_t>(Bindings.first));
					out.Write(static_cast<uint32_t>(Bindings.second.m_Bindings.size()));

					for (auto& KeyBinding: Bindings.second.m_Bindings)
					{
						out.WriteData(KeyBinding.Device->Guid().Data, 0, sizeof(SK::Guid));
						out.Write(KeyBinding.Device->DeviceInfo().ProductId);
						out.Write(KeyBinding.Device->DeviceInfo().VendorId);
						out.Write(KeyBinding.Device->DeviceInfo().Interface);
						out.Write(static_cast<uint32_t>(KeyBinding.Key));
					}
				}

				// Mapped keys for disconnected devices
				for (auto& Bindings : m_UnavailableKeys)
				{
					out.Write(static_cast<uint32_t>(Bindings.first));
					out.Write(static_cast<uint32_t>(Bindings.second.size()));

					for (auto& KeyBinding : Bindings.second)
					{
						out.WriteData(KeyBinding.DeviceGuid.Data, 0, sizeof(SK::Guid));
						out.Write(KeyBinding.DeviceInfo.ProductId);
						out.Write(KeyBinding.DeviceInfo.VendorId);
						out.Write(KeyBinding.DeviceInfo.Interface);
						out.Write(static_cast<uint32_t>(KeyBinding.Key));
					}
				}
			}

			void Read(SK::IO::Stream& in)
			{
				uint32_t Magic, Version;
				in.Read(Magic, Version);

				if (Magic != MAGICNUMBER || Version != VERSION)
				{
					throw SK::ArgumentException("Not a valid keymap");
				}

				uint32_t NbKeys = in.Read<uint32_t>();
				
				DeviceManager& DevMan = DeviceManager::Instance();
				for (uint32_t i = 0; i < NbKeys; ++i)
				{
					GameKey NewKey = static_cast<GameKey>(in.Read<uint32_t>());
					uint32_t NbBindings = in.Read<uint32_t>();
					
					for (uint32_t j = 0; j < NbKeys; ++j)
					{
						SavedDeviceKeyPair Pair;

						in.ReadData(Pair.DeviceGuid.Data, 0, sizeof(SK::Guid));
						in.Read(Pair.DeviceInfo.ProductId);
						in.Read(Pair.DeviceInfo.VendorId);
						in.Read(Pair.DeviceInfo.Interface);
						Pair.Key = static_cast<Key>(in.Read<uint32_t>());

						// Find device
						bool FoundDevice = false;
						for (auto& Device : DevMan)
						{
							if (Device.Guid() == Pair.DeviceGuid && Device.DeviceInfo() == Pair.DeviceInfo)
							{
								Add(NewKey, KeyBinding(Device.Id(), Pair.Key));
								FoundDevice = true;
								break;
							}
						}

						if (!FoundDevice)
						{
							m_UnavailableKeys[NewKey].push_back(Pair);
						}
					}
				}
			}

		private:

			//-- Variables --//
			//===============//

			std::map<GameKey, KeyBinding> m_Keys;
			std::map<GameKey, std::vector<SavedDeviceKeyPair>> m_UnavailableKeys;
		};
	}
}