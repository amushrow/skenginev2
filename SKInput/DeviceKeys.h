#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <SKSystem/Text/String.h>
#include <SKSystem/Flags.h>

namespace SK
{
	namespace Input
	{
		//---- Types ----//
		//===============//

		enum class Key
		{
			Back = 0x08,
			Tab = 0x09,

			Clear = 0x0C,
			Return = 0x0D,

			Pause = 0x13,
			CapsLock = 0x14,

			Kana = 0x15,
			Junja = 0x17,
			Final = 0x18,
			Kanji = 0x19,

			Escape = 0x1B,

			Convert = 0x1C,
			Nonconvert = 0x1D,
			Accept = 0x1E,
			ModeChange = 0x1F,

			Space = 0x20,
			Prior = 0x21,
			Next = 0x22,
			End = 0x23,
			Home = 0x24,
			Left = 0x25,
			Up = 0x26,
			Right = 0x27,
			Down = 0x28,
			Select = 0x29,
			Print = 0x2A,
			Execute = 0x2B,
			PrintScreen = 0x2C,
			Insert = 0x2D,
			Delete = 0x2E,
			Help = 0x2F,

			Num0 = 0x30,
			Num1 = 0x31,
			Num2 = 0x32,
			Num3 = 0x33,
			Num4 = 0x34,
			Num5 = 0x35,
			Num6 = 0x36,
			Num7 = 0x37,
			Num8 = 0x38,
			Num9 = 0x39,

			A = 0x41,
			B = 0x42,
			C = 0x43,
			D = 0x44,
			E = 0x45,
			F = 0x46,
			G = 0x47,
			H = 0x48,
			I = 0x49,
			J = 0x4A,
			K = 0x4B,
			L = 0x4C,
			M = 0x4D,
			N = 0x4E,
			O = 0x4F,
			P = 0x50,
			Q = 0x51,
			R = 0x52,
			S = 0x53,
			T = 0x54,
			U = 0x55,
			V = 0x56,
			W = 0x57,
			X = 0x58,
			Y = 0x59,
			Z = 0x5A,

			LeftWindows = 0x5B,
			RightWindows = 0x5C,
			Application = 0x5D,

			Sleep = 0x5F,

			Numpad0 = 0x60,
			Numpad1 = 0x61,
			Numpad2 = 0x62,
			Numpad3 = 0x63,
			Numpad4 = 0x64,
			Numpad5 = 0x65,
			Numpad6 = 0x66,
			Numpad7 = 0x67,
			Numpad8 = 0x68,
			Numpad9 = 0x69,
			Multiply = 0x6A,
			Add = 0x6B,
			Separator = 0x6C,
			Subtract = 0x6D,
			Decimal = 0x6E,
			Divide = 0x6F,
			F1 = 0x70,
			F2 = 0x71,
			F3 = 0x72,
			F4 = 0x73,
			F5 = 0x74,
			F6 = 0x75,
			F7 = 0x76,
			F8 = 0x77,
			F9 = 0x78,
			F10 = 0x79,
			F11 = 0x7A,
			F12 = 0x7B,
			F13 = 0x7C,
			F14 = 0x7D,
			F15 = 0x7E,
			F16 = 0x7F,
			F17 = 0x80,
			F18 = 0x81,
			F19 = 0x82,
			F20 = 0x83,
			F21 = 0x84,
			F22 = 0x85,
			F23 = 0x86,
			F24 = 0x87,

			NumpadEnter = 0x8F,
			NumLock = 0x90,
			ScrollLock = 0x91,

			FJ_Jisho = 0x92,
			FJ_Masshou = 0x93,
			FJ_Touroku = 0x94,
			FJ_Loya = 0x95,
			FJ_Roya = 0x96,

			LeftShift = 0xA0,
			RightShift = 0xA1,
			LeftCtrl = 0xA2,
			RightCtrl = 0xA3,
			Alt = 0xA4,
			AltGr = 0xA5,

			Browser_Back = 0xA6,
			Browser_Forward = 0xA7,
			Browser_Refresh = 0xA8,
			Browser_Stop = 0xA9,
			Browser_Search = 0xAA,
			Browser_Favorites = 0xAB,
			Browser_Home = 0xAC,

			Volume_Mute = 0xAD,
			Volume_Down = 0xAE,
			Volume_Up = 0xAF,
			Media_Next_Track = 0xB0,
			Media_Prev_Track = 0xB1,
			Media_Stop = 0xB2,
			Media_Play_Pause = 0xB3,
			Launch_Mail = 0xB4,
			Launch_Media_Select = 0xB5,
			Launch_App1 = 0xB6,
			Launch_App2 = 0xB7,

			OEM_1 = 0xBA,
			Plus = 0xBB,
			Comma = 0xBC,
			Minus = 0xBD,
			Period = 0xBE,
			OEM_2 = 0xBF,
			OEM_3 = 0xC0,

			OEM_4 = 0xDB,
			OEM_5 = 0xDC,
			OEM_6 = 0xDD,
			OEM_7 = 0xDE,
			OEM_8 = 0xDF,

			AX = 0xE1,
			OEM_102 = 0xE2,
			Ico_Help = 0xE3,
			Ico_00 = 0xE4,

			ProcessKey = 0xE5,

			Ico_Clear = 0xE6,

			Packet = 0xE7,

			Reset = 0xE9,
			Jump = 0xEA,
			OEM_Pa1 = 0xEB,
			Pa2 = 0xEC,
			Pa3 = 0xED,
			WsCtrl = 0xEE,
			CuSel = 0xEF,
			OEM_Attn = 0xF0,
			Finish = 0xF1,
			Copy = 0xF2,
			Auto = 0xF3,
			Enlw = 0xF4,
			BackTab = 0xF5,

			Attn = 0xF6,
			CrSel = 0xF7,
			ExSel = 0xF8,
			ErEOF = 0xF9,
			Play = 0xFA,
			Zoom = 0xFB,
			NONAME = 0xFC,
			PA1 = 0xFD,
			OEM_Clear = 0xFE,

			MouseLeftButton = 0x0100,
			MouseRightButton,
			MouseMiddleButton,
			MouseButton1,
			MouseButton2,
			MouseButton3,
			MouseButton4,
			MouseButton5,
			MouseWheel,
			MouseXAxis,
			MouseYAxis,
			MouseXPos,
			MouseYPos,

			Button1,
			Button2,
			Button3,
			Button4,
			Button5,
			Button6,
			Button7,
			Button8,
			Button9,
			Button10,
			Button11,
			Button12,
			Button13,
			Button14,
			Button15,
			Button16,
			Button17,
			Button18,
			Button19,
			Button20,
			Button21,
			Button22,
			Button23,
			Button24,
			Button25,
			Button26,
			Button27,
			Button28,
			Button29,
			Button30,
			Button31,
			Button32,
			
			DPad_Up,
			DPad_Down,
			DPad_Left,
			DPad_Right,
			DPad2_Up,
			DPad2_Down,
			DPad2_Left,
			DPad2_Right,
			DPad3_Up,
			DPad3_Down,
			DPad3_Left,
			DPad3_Right,
			DPad4_Up,
			DPad4_Down,
			DPad4_Left,
			DPad4_Right,

			Axis_X,
			Axis_Y,
			Axis_Z,
			Axis_Rx,
			Axis_Ry,
			Axis_Rz,
			Slider1,
			Slider2
		};

		enum class ForceFeedbackAxis
		{
			X    = 0x01,
			Y    = 0x02,
			Z    = 0x04,
			Rx   = 0x08,
			Ry   = 0x10,
			Rz   = 0x20
		};
		typedef Flags<ForceFeedbackAxis> FFAxisFlags;

		//--- Methods ---//
		//===============//

		String GetKeyName(Key key);
	}
}