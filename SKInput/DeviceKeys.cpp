//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <SKInput/DeviceKeys.h>

//-- Declarations --//
//==================//

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

namespace
{
	SK::String KeyNames[328] =
	{
		"Invalid",          "Invalid",          "Invalid",        "Invalid",           "Invalid",       "Invalid",      "Invalid",      "Invalid",         //0x07
		"Back",             "Tab",              "Invalid",        "Invalid",           "Clear",         "Return",       "Invalid",      "Invalid",         //0x0F
		"Invalid",          "Invalid",          "Invalid",        "Pause",             "Caps Lock",     "Kana",         "Invalid",      "Junja",           //0x17
		"Final",            "Kanji",            "Invalid",        "Escape",            "Convert",       "Nonconvert",   "Accept",       "Mode Change",     //0x1F
		"Space",            "Prior",            "Next",           "End",               "Home",          "Left",         "Up",           "Right",           //0x27
		"Down",             "Select",           "Print",          "Execute",           "Print Screen",  "Insert",       "Delete",       "Help",            //0x2F
		"0",                "1",                "2",              "3",                 "4",             "5",            "6",            "7",               //0x37
		"8",                "9",                "Invalid",        "Invalid",           "Invalid",       "Invalid",      "Invalid",      "Invalid",         //0x3F
		"Invalid",          "A",                "B",              "C",                 "D",             "E",            "F",            "G",               //0x47
		"H",                "I",                "J",              "K",                 "L",             "M",            "N",            "O",               //0x4F
		"P",                "Q",                "R",              "S",                 "T",             "U",            "V",            "W",               //0x57
		"X",                "Y",                "Z",              "Left Windows",      "Right Windows", "Application",  "Invalid",      "Sleep",           //0x5F
		"Numpad0",          "Numpad1",          "Numpad2",        "Numpad3",           "Numpad4",       "Numpad5",      "Numpad6",      "Numpad7",         //0x67
		"Numpad8",          "Numpad9",          "Multiply",       "Add",               "Separator",     "Subtract",     "Decimal",      "Divide",          //0x6F
		"F1",               "F2",               "F3",             "F4",                "F5",            "F6",           "F7",           "F8",              //0x77
		"F9",               "F10",              "F11",            "F12",               "F13",           "F14",          "F15",          "F16",             //0x7F
		"F17",              "F18",              "F19",            "F20",               "F21",           "F22",          "F23",          "F24",             //0x87
		"Invalid",          "Invalid",          "Invalid",        "Invalid",           "Invalid",       "Invalid",      "Invalid",      "Numpad Enter",    //0x8F
		"Num Lock",         "Scroll Lock",      "Jisho",          "Masshou",           "Touroku",       "Loya",         "Roya",         "Invalid",         //0x97
		"Invalid",          "Invalid",          "Invalid",        "Invalid",           "Invalid",       "Invalid",      "Invalid",      "Invalid",         //0x9F
		"Left Shift",       "Right Shift",      "Left Ctrl",      "Right Ctrl",        "Left Alt",      "Right Alt",    "Browser Back", "Browser Forward", //0xA7
		"Browser Refresh",  "Browser Stop",     "Browser Search", "Browser Favorites", "Browser Home",  "Volume Mute",  "Volume Down",  "Volume Up",       //0xAF
		"Media Next Track", "Media Prev Track", "Media Stop",     "Media Play/Pause",  "Launch Mail",   "Launch Media", "Launch App1",  "Launch App2",     //0xB7
		"Invalid",          "Invalid",          "OEM 1",          "Plus",              "Comma",         "Minus",        "Period",       "OEM 2",           //0xBF
		"OEM 3",            "Invalid",          "Invalid",        "Invalid",           "Invalid",       "Invalid",      "Invalid",      "Invalid",         //0xC7
		"Invalid",          "Invalid",          "Invalid",        "Invalid",           "Invalid",       "Invalid",      "Invalid",      "Invalid",         //0xCF
		"Invalid",          "Invalid",          "Invalid",        "Invalid",           "Invalid",       "Invalid",      "Invalid",      "Invalid",         //0xD7
		"Invalid",          "Invalid",          "Invalid",        "OEM 4",             "OEM 5",         "OEM 6",        "OEM 7",        "OEM 8",           //0xDF
		"Invalid",          "AX",               "OEM 102",        "Ico Help",          "Ico 00",        "Process",      "Ico Clear",    "Packet",          //0xE7
		"Invalid",          "Reset",            "Jump",           "OEM PA1",           "PA2",           "PA3",          "WsCtrl",       "CuSel",           //0xEF
		"OEM Attn",         "Finish",           "Copy",           "Auto",              "Enlw",          "Backtab",      "Attn",         "CrSel",           //0xF7
		"ExSel",            "ErEOF",            "Play",           "Zoom",              "NONAME",        "PA1",          "OEM Clear",    "Invalid",         //0xFF

		"Mouse Left",       "Mouse Right",     "Mouse Middle",    "XButton 1",         "XButton 2",     "XButton 3",    "XButton 4",    "XButton 5",       //0x107
		"Mouse Wheel",      "Mouse Delta X",   "Mouse Delta Y",   "Mouse Pos X",       "Mouse Pos Y",   "Button 1",     "Button 2",     "Button 3",        //0x10F
		"Button 4",         "Button 5",        "Button 6",        "Button 7",          "Button 8",      "Button 9",     "Button 10",    "Button 11",       //0x117
		"Button 12",        "Button 13",       "Button 14",       "Button 15",         "Button 16",     "Button 17",    "Button 18",    "Button 19",       //0x11F
		"Button 20",        "Button 21",       "Button 22",       "Button 23",         "Button 24",     "Button 25",    "Button 26",    "Button 27",       //0x127
		"Button 28",        "Button 29",       "Button 30",       "Button 31",         "Button 32",     "D-Pad Up",     "D-Pad Down",   "D-Pad Left",      //0x12F
		"D-Pad Right",      "D-Pad2 Up",       "D-Pad2 Down",     "D-Pad2 Left",       "D-Pad2 Right",  "D-Pad3 Up",    "D-Pad3 Down",  "D-Pad3 Left",     //0x137
		"D-Pad3 Right",     "D-Pad4 Up",       "D-Pad4 Down",     "D-Pad4 Left",       "D-Pad4 Right",  "X Axis Pos",   "Y Axis Pos",   "Z Axis Pos",      //0x13F    
		"X Axis Rot",       "Y Axis Rot",      "Z Axis Rot",      "Slider 1",          "Slider 2",      "Invalid",      "Invalid",      "Invalid"          //0x147
	};
}

//-- Public Methods --//
//====================//

SK::String SK::Input::GetKeyName(SK::Input::Key key)
{
	return KeyNames[static_cast<int>(key)];
}

//-- Private Methods --//
//=====================//
